#include <iostream>
using namespace std;

typedef struct nodo
{
    int dato;
    struct nodo *link;

}nodo;
typedef nodo *lista;
void agregarfinal(lista *l,int e)
{
    lista p,q;
    p=new(nodo);
    p->dato=e;
    p->link=NULL;
    if((*l)==NULL)
        (*l)=p;
    else
        {
        if((*l)->link==NULL)
            (*l)->link=p;
        else
        {
            q=(*l);
            while(q->link!=NULL)
            {
                q=q->link;

            }
            q->link=p;
        }
    }
}
void agregarseg(lista *l, int e)
{
  lista p;
  if((*l)!=NULL)
  {
    p=new(nodo);
    p->dato=e;
    p->link=(*l)->link;
    (*l)->link=p;
  }
}

void imprimir(lista l)
{
    lista p;
    p=l;
    while(p!=NULL)
    {
        cout<<p->dato<<" ->";
        p=p->link;
    }
}

int main()
{
    lista l=NULL;
    int e;
    agregarfinal(&l,1);
    agregarfinal(&l,8);
    agregarfinal(&l,8);
    agregarseg(&l,3);

    imprimir(l);
    return 0;
}
