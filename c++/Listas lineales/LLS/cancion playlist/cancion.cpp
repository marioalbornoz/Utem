#include <iostream>
#include <string>

using namespace std;

typedef struct nodo
{
    string nombre_cancion;
    string autor;
    int duracion;
    int valor_disco;
    struct nodo*link;
};
typedef nodo *lista;

void crear(lista *l,string name,string compositor,int a,int b)
{


    lista p,q;
    p=new(nodo);
    p->nombre_cancion=name;
    p->autor=compositor;
    p->duracion=a;
    p->valor_disco=b;
    p->link=NULL;

    if((*l)==NULL) //PRIMER CASO
        (*l)=p;
    else
        {
        if((*l)->link==NULL) //SEGUNDO CASO
            (*l)->link=p;
        else
        {
            q=(*l);
            while(q->link!=NULL) //TERCER CASO
            {
                q=q->link;
            }
            q->link=p; //CONECTA EL FINAL DEL NODO CON EL NODO P
        }
    }
}

void imprimir(lista l)
{
    lista p;
    p=l;

    while(p!=NULL)
    {
        cout<<"nombre:"<<p->nombre_cancion<<endl;
        cout<<"autor: "<<p->autor<<endl;
        cout<<"duracion: "<<p->duracion<<endl;
        cout<<"valor: "<<p->valor_disco<<endl;
        cout<<"-----------------------------------"<<endl;
        p=p->link;
    }
}

int main()
{
    lista l=NULL;
    crear(&l,"Despacito","Dady Yankie",560,1500);
    crear(&l,"4 baby","radiohead",330,1500);
    crear(&l,"Quiero beber cerveza","Linkin Park",330,1500);
    crear(&l,"karma police","radiohead",330,599);
    crear(&l,"where is my mind?","Pixies",330,789);
    crear(&l,"Fake Plastic Trees","radiohead",330,769);
    crear(&l,"Californication","Red Hot Chili Peppers",330,699);
    crear(&l,"Dont go Away","Oasis",330,650);
    crear(&l,"Is this love","Whitesnake",330,790);
    crear(&l,"Patience","Guns N Roses",330,600);
    imprimir(l);
    return 0;
}
