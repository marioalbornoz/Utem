#include <iostream>
using namespace std;

struct Nodo {
    int info;
    struct Nodo *link;
};
typedef Nodo *Lista;

Lista llenarListaRecursivamente(Lista &L, int iCantidad) {
    if(iCantidad == 0)
        return L;

    Lista nuevoNodo = new (Nodo);
    nuevoNodo->info = iCantidad;
    nuevoNodo->link = llenarListaRecursivamente(L, iCantidad - 1);
    return nuevoNodo;
}

void mostrarListaRecursivamente(Lista &L) {
    if(L == NULL)
        return;

    cout << "[" << L->info << "],";
    mostrarListaRecursivamente(L->link);
}

void mostrarListaRecursivamenteInversa(Lista &L) {
    if(L == NULL)
        return;
    mostrarListaRecursivamenteInversa(L->link);
    cout << "[" << L->info << "],";
}


int sumarListaRecursivamente (Lista &L){
    if(L == NULL)
        return 0;
    else
        return L->info + sumarListaRecursivamente(L->link);
}

#include <climits>
int mayorElemento (Lista &L) {
    if (L == NULL)
        return INT_MIN;
    else {
        int iMayor = mayorElemento(L->link);
        if(L->info > iMayor)
            return L->info;
        else
            return iMayor;
    }
}

int menorElemento (Lista &L) {
    if (L == NULL)
        return INT_MAX;
    else {
        int iMenor = menorElemento(L->link);
        if(L->info < iMenor)
            return L->info;
        else
            return iMenor;
    }
}

int cantidadElementosEnListaRecursiva (Lista &L) {
    if (L ==NULL)
        return 0;

    return 1+cantidadElementosEnListaRecursiva(L->link);
}

int promedioListaRecursivamente (Lista &L){
    if(L == NULL)
        return 0;
    else
        return (L->info + sumarListaRecursivamente(L->link))
                    /cantidadElementosEnListaRecursiva(L);
}


void eliminarNodoX(Lista &L, int valorX) {
    if(L==NULL) {
        return;
    } else { /**Primer Elemento de la Lista**/
        if(L->info == valorX) {
            Lista nodoEliminar = L;
            L= L->link;
            delete(nodoEliminar);
        } else { /*Elementos siguientes*/
            Lista actual    = L;
            Lista siguiente = L->link;
            /**Segundo Elemento */
            while (siguiente != NULL) {
                if(siguiente->info == valorX){
                    //siguiente->info = 111;
                    Lista nodoEliminar = siguiente;
                    actual->link = siguiente->link;
                    delete (nodoEliminar);
                }else {
                    actual = actual->link;
                }
                siguiente   = actual->link;
            }
        }
    }
}



void eliminarPrimerNodo(Lista &L) {
    /**Primer Elemento de la Lista**/
    if(L!= NULL) {
        Lista nodoEliminar = L;
        L= L->link;
        delete(nodoEliminar);
    }
}



Lista eliminarLaPosicionXRecursivamente(Lista &L, int iPosicionActual, int iPosicionEliminar) {
    if(L==NULL)
        return L;
    if(iPosicionActual == iPosicionEliminar) {
        Lista nodoEliminar = L;
        Lista siguiente = L->link;
        delete (nodoEliminar);
        return siguiente;
    } else {
        return L;
    }
}

int main() {
    cout <<endl << "Listas Recursivas" << endl;
    Lista L = NULL;
    L = llenarListaRecursivamente(L, 15);
    cout <<endl<< "Mostrar Lista";
    mostrarListaRecursivamente(L);
    cout <<endl<< "Mostrar Lista Inversa";
    mostrarListaRecursivamenteInversa(L);
    cout <<endl<< "Suma Recursiva:" << sumarListaRecursivamente(L);
    cout <<endl<< "Mayor Elemento Recursivo:" << mayorElemento(L);
    cout <<endl<< "Menor Elemento Recursivo:" << menorElemento(L);
    cout <<endl<< "Cantidad de Elementos:" << cantidadElementosEnListaRecursiva(L);
    cout <<endl<< "Promedio de Elementos:" << promedioListaRecursivamente(L);
    cout <<endl<< "Eliminar (9) ";
    //L = eliminarLaPosicionXRecursivamente(L, 0, 4);
    eliminarNodoX(L, 9);
    mostrarListaRecursivamente(L);
    cout <<endl<< "Eliminar (15) ";
    eliminarNodoX(L, 15);
    mostrarListaRecursivamente(L);
    cout <<endl<< "Eliminar (6) ";
    eliminarNodoX(L, 6);
    mostrarListaRecursivamente(L);
    cout <<endl<< "Eliminar (1) ";
    eliminarNodoX(L, 1);
    mostrarListaRecursivamente(L);
    cout <<endl<< "Eliminar (2) ";
    eliminarNodoX(L, 2);
    mostrarListaRecursivamente(L);
    cout <<endl<< "Eliminar Primer Nodo de la lista ";
    eliminarPrimerNodo(L);
    mostrarListaRecursivamente(L);
    return 0;
}
