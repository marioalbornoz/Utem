 #include <iostream>
#include <climits> /**INT_MAX INT MIN*/
//#define NULL 0

using namespace std;

struct Nodo {
    int info;
    struct Nodo *link;
};

typedef Nodo *Lista;


/***PROTOTIPOS***/
void generaLista(Lista &L);
void imprimeLista(Lista &L);
int contarElementos(Lista &L);
int sumarElementos(Lista &L);
int mayorElemento(Lista &L);
int contarSI(Lista &L, int k);
bool bComienzaConDigito(int valor, int K);
void agregarKNodosAlFinal(Lista &L, int iCantidadNodos, int info);
void agregarNodoAlFinal (Lista &L, Lista &nuevoNodo);
Lista eliminarElPrimerNodo(Lista &L);
Lista eliminarElUltimoNodo(Lista &L);
bool estaOrdenado(Lista &L);
void insertarNodoConInformacion(Lista &L, int valor);
void eliminarNodoConInformacionIgualAK(Lista &L, int valorK);
void ordenarListaPorBurbuja(Lista &L);
int buscarElMenorElemento(Lista &L);
int buscarElMayorElemento(Lista &L);

int main() {
    Lista L;
    L = NULL;
    cout << "Listas" << endl;

    generaLista(L);
    imprimeLista(L);
    cout << "Listas" << endl;
    imprimeLista(L);
    cout << endl <<"Existen " << contarElementos(L) << " elementos";
    cout << endl <<"La Suma de los elementos es " << sumarElementos(L);
    cout << endl <<"El Mayor elemento de la lista es " << mayorElemento(L);
    cout << endl <<"Cantidad de Elementos con K = 5 es " << contarSI(L, 5);
    cout << endl <<"Agregamos elementos al final de la lista" << endl;
    agregarKNodosAlFinal(L, 4, 25);
    imprimeLista(L);
    cout << endl <<"Eliminamos el Primer Nodo de la Lista" << endl;
    L = eliminarElPrimerNodo(L);
    L = eliminarElPrimerNodo(L);
    L = eliminarElPrimerNodo(L);
    imprimeLista(L);
    cout << endl <<"Eliminamos el Ultimo Nodo de la Lista" << endl;
    /*L = eliminarElUltimoNodo(L);
    L = eliminarElUltimoNodo(L);
    L = eliminarElUltimoNodo(L);
    L = eliminarElUltimoNodo(L);
    L = eliminarElUltimoNodo(L);*/
    Lista LL = eliminarElUltimoNodo(L);
    imprimeLista(L);
    cout << endl << "Esta Ordenado: " << estaOrdenado(L);
    cout << endl << "Insertamos Elemento 3 ";
    insertarNodoConInformacion(L, 3);
    imprimeLista(L);
    cout << endl << "Insertamos Elemento 0 ";
    insertarNodoConInformacion(L, 0);
    imprimeLista(L);
    cout << endl << "Insertamos Elemento 1000 ";
    insertarNodoConInformacion(L, 1000);
    imprimeLista(L);

    cout << endl << "Eliminamos el elemento con valor (3) ";
    eliminarNodoConInformacionIgualAK(L, 3);
    imprimeLista(L);
    cout << endl << "Eliminamos el elemento con valor (3) ";
    eliminarNodoConInformacionIgualAK(L, 3);
    imprimeLista(L);
    cout << endl << "Eliminamos el elemento con valor (8) ";
    eliminarNodoConInformacionIgualAK(L, 8);
    imprimeLista(L);
    cout << endl << "Eliminamos el elemento con valor (7) ";
    eliminarNodoConInformacionIgualAK(L, 7);
    imprimeLista(L);
    cout << endl << "Eliminamos el elemento con valor (25) ";
    eliminarNodoConInformacionIgualAK(L, 25);
    imprimeLista(L);
    cout << endl << "Eliminamos el elemento con valor (100 No existe) ";
    eliminarNodoConInformacionIgualAK(L, 100);
    imprimeLista(L);
    cout << endl << "Ordenar Lista";
    ordenarListaPorBurbuja(L);
    imprimeLista(L);
    cout << endl << "El Mayor Elemento de la lista es :"<< buscarElMenorElemento(L);
    cout << endl << "El Menor Elemento de la lista es :"<< buscarElMayorElemento(L);
    cout << endl << endl;
    return 0;
}


void generaLista(Lista &L) {
    int i, iCantidadNodos = 11;
    Lista aux;

    for (i = 1; i <= iCantidadNodos ; i ++) {
        aux = new (Nodo);
        aux->info = i;
        aux->link = L; //Asignamos la lista existente
        L = aux;
    }
}


void imprimeLista(Lista &L) {
    Lista aux = L;
    while(aux != NULL) {
        cout << endl <<"[" << aux->info << "]";
        aux = aux->link;
    }
}

int contarElementos(Lista &L) {
    Lista aux = L;
    int iCantidad = 0;
    while(aux != NULL) {
        iCantidad++;
        aux = aux->link;
    }
    return iCantidad;
}


int sumarElementos(Lista &L) {
    Lista aux = L;
    int iSuma = 0;
    while (aux != NULL) {
        iSuma = iSuma + aux->info;
        aux = aux->link;
    }
    return iSuma;
}


int mayorElemento(Lista &L) {
    Lista aux = L;
    int iMayorElemento = 0;
    while (aux != NULL) {
        if(aux->info > iMayorElemento )
        iMayorElemento =  aux->info;
        aux = aux->link;
    }
    return iMayorElemento;
}



int contarSI(Lista &L, int k) {
    Lista aux = L;
    int iCantidad = 0;
    while (aux != NULL) {
        if(bComienzaConDigito(aux->info, k))
            iCantidad++;
        aux = aux->link;
    }
    return iCantidad;
}

bool bComienzaConDigito(int valor, int k) {
    while ( valor > 10) {
        valor = valor / 10;
    }
    return (valor == k );
}

/* e= informacion */
void agregarKNodosAlFinal(Lista &L, int iCantidadNodos, int info) {
    Lista aux = L, nuevoNodo;

    for (int i = 0; i < iCantidadNodos; i++ ) {
        nuevoNodo = new(Nodo);
        nuevoNodo->info = info;
        nuevoNodo->link = NULL;

        agregarNodoAlFinal(aux, nuevoNodo);
    }
}



void agregarNodoAlFinal (Lista &L, Lista &nuevoNodo) {
    Lista aux = L;
    if(aux == NULL)
        aux = nuevoNodo;
    else {
        while (aux->link != NULL) {
            /***recorro a->link, para saber si es el ultimo elemento de la lista **/
            /**Si lo hago con aux != NULL, s�lo llegar� al �ltimo nodo**/
            aux = aux->link;
        }
        aux->link = nuevoNodo;
    }
}

#include <limits>
Lista eliminarElPrimerNodo(Lista &L){
    Lista listaActual = L;

    if (listaActual != NULL) {
        /**guardamos el nodo que deseamos eliminar**/
        Lista nodoAEliminar = listaActual;
        /**seleccionamos el segundo nodo de la lista**/
        listaActual = listaActual->link;
        delete(nodoAEliminar);
    }
    /**Retornamos la lista sin el primer elemento**/
    return listaActual;
}


Lista eliminarElUltimoNodo(Lista &L) {
    Lista listaActual = L;
    Lista ultimoNodo = L;
    if(aux->link == NULL) {
        aux=NULL;
        delete(aux);
    } else
        while(aux->link != NULL) {
            nodoAnterior = aux;
            aux = aux->link;
        }
    nodoAnterior->link=NULL;
    delete(aux);

    L = listaActual;
    //return listaActual;
}


bool estaOrdenado(Lista &L) {
    Lista aux = L;
    bool bEstaOrdenado = true;
    int iMenorValor = aux->info;

    while (aux != NULL) {
        if(aux->info < iMenorValor)
            bEstaOrdenado = false;
        aux = aux->link;
    }
    return bEstaOrdenado;
}


void insertarNodoConInformacion(Lista &L, int valor) {
    Lista aux       = L;
    Lista siguiente = L;
    /**Preparamos el nodo **/
    Lista nuevoNodo = new (Nodo);
    nuevoNodo->info = valor;
    nuevoNodo->link = NULL;

    if(aux == NULL) {
        L = nuevoNodo;
    } else {
        /**revisamos el primer caso**/
       if(aux->info <= valor) {
            nuevoNodo->link = aux;
            L = nuevoNodo;
        } else {
            while (aux->link != NULL) {
                siguiente = aux->link;
                if(siguiente !=  NULL && siguiente->info <= valor) {
                    nuevoNodo->link= siguiente;
                    aux->link = nuevoNodo;
                    break;
                }
                /**continuamos iterando **/
                aux = aux->link;
            }
            /*Si no lo encontramos anteriormente, lo agregamos a la lista*/
            aux->link = nuevoNodo;
        }
    }
}



void insertarNodoConInformacionV(Lista &L, int valor) {
    Lista aux       = L;
    Lista siguiente = L;
    /**Preparamos el nodo **/
    Lista nuevoNodo = new (Nodo);
    nuevoNodo->info = valor;
    nuevoNodo->link = NULL;

    if(aux == NULL) {
        L = nuevoNodo;
    } else {
        while (aux->link != NULL) {
            siguiente = aux->link;
            if(siguiente !=  NULL && siguiente->info <= valor) {
                nuevoNodo->link= siguiente;
                aux->link = nuevoNodo;
                break;
            }
            /**continuamos iterando **/
            aux = aux->link;
        }
        /*Si no lo encontramos anteriormente, lo agregamos a la lista*/
        aux->link = nuevoNodo;
    }
}


void eliminarNodoConInformacionIgualAK(Lista &L, int valorK) {
    Lista aux       = L;
    Lista actual    = L;
    Lista siguiente = L;
    /**revisamos el primer elemento **/
    if(aux != NULL && aux->info == valorK ) {
        siguiente = aux;
        L = aux->link;
        delete(aux);
    } else {
        /**Revisamos cuando no es el primer caso**/
        while (aux != NULL) {
            siguiente = aux->link;
            if(siguiente != NULL && siguiente->info == valorK) {
                Lista nodoEliminar = siguiente;
                aux->link = siguiente->link;
                delete (siguiente);
                break;
            }
            aux = aux->link;
        }
    }
}


/**(Bubble Sort)**/
void ordenarListaPorBurbuja(Lista &L) {
    Lista actual    = L;
    Lista siguiente = L;

    while (actual != NULL) {
        siguiente =  actual->link;
        while (siguiente != NULL) {
            if(actual->info > siguiente->info) {
                int temp        = siguiente->info;
                siguiente->info = actual->info;
                actual->info    = temp;
            }
            /**siguiente elemento **/
            siguiente = siguiente->link;
        }
        actual = actual->link;
    }
}

int buscarElMayorElemento(Lista &L) {
    Lista aux = L;
    int iMayorElemento = INT_MIN;

    while (aux != NULL) {
        if(aux->info > iMayorElemento)
            iMayorElemento = aux->info;

        aux = aux->link;
    }
    return iMayorElemento;
}


int buscarElMenorElemento(Lista &L) {
    Lista aux = L;
    int iMenorElemento = INT_MAX;

    while (aux != NULL) {
        if(aux->info < iMenorElemento)
            iMenorElemento = aux->info;

        aux = aux->link;
    }
    return iMenorElemento;
}
