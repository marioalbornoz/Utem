#include<iostream>
using namespace std;


struct nodo{
  int dato;
  struct nodo *sig;
  struct nodo *ant;
};

typedef nodo *lista;

void agregarNodo(lista &l, int valor)
{
  lista p;
  p=new(nodo);
  p->dato=valor;
  p->ant=NULL;
  p->sig=l;
  if(l!=NULL) l->ant=p;
  l=p;
}


void agregarNodoCasoGeneral(lista &l,int valor)
{
  lista p;
  p=new(nodo);
  p->dato=valor;
  if(l==NULL)
  {
    l=p;
    p->sig=NULL;
    p->ant=NULL;

  }
  else
  {
    p->sig = l->sig;
    l->sig = p;
    p->ant=l;
    (l->sig)->ant=p;

  }
}

void mostrarRecursivo(lista l)
{
  if(l!=NULL)
  {
    cout<<"["<< l->dato <<"],";
    mostrarRecursivo(l->sig);
  }
}

int main()
{
  lista l;
  l=NULL;
  agregarNodo(l,2);
  //mostrarRecursivo(l);
  agregarNodo(l,1);
  //mostrarRecursivo(l);
  agregarNodoCasoGeneral(l,3);
  agregarNodoCasoGeneral(l,10);
  agregarNodoCasoGeneral(l,11);
  agregarNodoCasoGeneral(l,122);
  agregarNodoCasoGeneral(l,0);
  mostrarRecursivo(l);
  return 0;
}
