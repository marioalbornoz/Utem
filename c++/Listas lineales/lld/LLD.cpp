#include <iostream>

using namespace std;

struct TipoNodo{
    int info;
    struct TipoNodo *ant;
    struct TipoNodo *sig;
};

typedef TipoNodo *Lista;

void agregarNodo(Lista &L, int valor) {
    Lista aux;
    aux = new(TipoNodo);
    aux->info = valor;
    /**El nodo anterior es null**/
    aux->ant = NULL;
    /**El nodo siguiente (quedar� como primer nodo)**/
    aux->sig = L;
    /**Si la lista no esta vac�a,
     * lo enlazamos a su nodo anterior**/
    if(L != NULL)
        L->ant = aux;
    /**Marco como Nodo actual al nodo ingresado***/
    L = aux;
}

void agregarNodoCasoGeneral(Lista &L, int valor) {
    Lista nuevoNodo;
    nuevoNodo = new(TipoNodo);
    nuevoNodo->info = valor;
    /**Caso 1: Lista Vacia **/
    if(L == NULL) {
        L = nuevoNodo;
        nuevoNodo->sig = NULL;
        nuevoNodo->ant = NULL;
    } else {
        /**Caso 2: Lista con Datos **/
        /*2*/nuevoNodo->sig = L->sig;
        /*3*/L->sig         = nuevoNodo;
        /*4*/nuevoNodo->ant = L;
        /*5*/if(nuevoNodo->sig != NULL)
                (nuevoNodo->sig)->ant = nuevoNodo;
    }
}

void irAlPrimerNodo(Lista &L){
    while(L->ant != NULL)
        L = L->ant;
}

void irAlUltimoNodo(Lista &L){
    while(L->sig != NULL)
        L = L->sig;
}

int contarLaCantidadDeNodos(Lista L){
    int iCantidadNodos = 0;
    irAlPrimerNodo(L);

    while(L != NULL) {
        iCantidadNodos++;
        L = L->sig;
    }
    return iCantidadNodos;
}

int irAlNodoCentral(Lista &L) {
    int iPosicionActual = 0;
    int iPosicionCentral = 0;
    iPosicionCentral = contarLaCantidadDeNodos(L) / 2;

    irAlPrimerNodo(L);

    while(L != NULL) {
        iPosicionActual++;
        L = L->sig;
        if(iPosicionActual >= iPosicionCentral)
            break;
    }
}




void mostrarIterativo(Lista L){
    while(L != NULL){
        cout << "Valor : " << L->info << endl;
        L = L->sig;
    }
}

void mostrarRecursivo(Lista L){
    if(L!=NULL){
        cout << "Valor : " << L->info << endl;
        mostrarRecursivo(L->sig);
    }
}

int mostrarNodoActual(Lista L){
    if  (L != NULL)
        return L->info;
}



void eliminarPosicionActual(Lista &L) {
    cout << "eliminando ... " << endl;
    if(L != NULL) {
        /**Si es el �ltimo nodo de la lista **/
        if(L->sig==NULL) {
            delete(L);
            L = NULL;
        } else {
            /**si tiene mas de un elemento**/
            L = L->sig;
            Lista nodoEliminar = L->ant;
            delete(nodoEliminar);
            L->ant = NULL;
        }
    }
}


int main() {
    cout << "Uso de LLD" << endl;
    Lista L = NULL;
    Lista primerElemento = L;

    agregarNodoCasoGeneral(L, 25);
    agregarNodoCasoGeneral(L, 26);
    agregarNodoCasoGeneral(L, 27);
    agregarNodoCasoGeneral(L, 1);
    agregarNodoCasoGeneral(L, 2);
    agregarNodoCasoGeneral(L, 3);
    agregarNodoCasoGeneral(L, 4);
    agregarNodoCasoGeneral(L, 5);
    agregarNodoCasoGeneral(L, 6);
    agregarNodoCasoGeneral(L, 100);
    agregarNodoCasoGeneral(L, 101);
    mostrarRecursivo(L);
    cout << endl << "Ir al primer nodo"<< endl;
    irAlPrimerNodo(L);
    mostrarRecursivo(L);
    cout << endl<< "Ir al ultimo nodo"<< endl;
    irAlUltimoNodo(L);
    mostrarRecursivo(L);
    cout << endl<< "Existen "<< contarLaCantidadDeNodos(L) << "Nodos";

    cout << endl<< "Ir Al Nodo Central " << endl;
    irAlNodoCentral(L);
    mostrarIterativo(L);
    cout << endl<< "El nodo central es :" << mostrarNodoActual(L);
    cout << endl<< "Eliminamos el nodo actual (El central)" << endl;
    eliminarPosicionActual(L);
    mostrarRecursivo(L);
    irAlPrimerNodo(L);
    mostrarIterativo(L);

    return 0;
}
