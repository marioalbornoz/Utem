#include <iostream>
using namespace std;

struct nodo{
  int dato;
  struct nodo *sig;
  struct nodo *ant;
  };
  typedef nodo *lista;

  void agregarNodo( lista &l,int valor)
  {
	lista p;
	p=new(nodo);
	p->sig=l;
	p->ant=NULL;
	p->dato=valor;
	if(l!=NULL) l->ant=p;
	l=p;
	}

	void mostrar(lista L)
	{
	  if(L!=NULL){
	  cout<<"[" <<L->dato<<"],";
	  mostrar(L->sig);}
	  }
	void agregarNodoGeneral(lista &l, int valor){
	lista p;
	p=new(nodo);
	p->dato=valor;
	if(l==NULL)
	{
		l=p;
		p->sig=NULL;
		p->ant=NULL;
		}
	else
	{
		p->sig 	= l->sig;
		l->sig=p;
		p->ant=l;
		(l->sig)->ant=p;
	}

}

void irAlPrimerNodo(lista &l)
{
  while(l->ant!=NULL) l=l->ant;
}

int mostrarNodoActual(lista L)
{
    if (L != NULL)
        return L->dato;
}

void eliminarPosicionActual(lista &l)
{
  cout<<"\nEliminando..."<<endl;
  if(l!=NULL) //QUE LA LISTA NO ESTË VACIA
  {
    if(l->sig==NULL) //SI ESTAA AL FINAL
    {
      delete(l);
      l=NULL;
    }
    else
    {
      l=l->sig;   //AVANZAMOS ! POSICION
      lista p = l->ant; //
      delete(p);
      l->ant=NULL;
    }
  }
}

int main() {
  lista L;L=NULL;
	//agregarNodo(L,10);
  agregarNodoGeneral(L,9);
  agregarNodoGeneral(L,55);
  mostrar(L);
  irAlPrimerNodo(L);
  cout<<"el nodo actual es: "<<mostrarNodoActual(L)<<endl;
  eliminarPosicionActual(L);
  cout<<"_________________________________"<<endl;
  mostrar(L);



  //mostrar(L);
}
