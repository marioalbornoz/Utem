#include <iostream>
#include <queue>
using namespace std;

typedef pair<int, string> StringPAR; // Prioridad, orden

int main() {
    cout <<"HEAP_MAX"<<endl;
    priority_queue<StringPAR> heap_max;
    StringPAR s;

    s.first= 1;
    s.second="Primero";
    heap_max.push(s);

    s.first= 4;
    s.second="Cuarto";
    heap_max.push(s);

    s.first= 2;
    s.second="Segundo";
    heap_max.push(s);

    s.first= 10;
    s.second="Decimo";
    heap_max.push(s);

    s.first= 7;
    s.second="Septimo";
    heap_max.push(s);

    while(!heap_max.empty()) {
        StringPAR auxPar;
        auxPar = heap_max.top();
        heap_max.pop();
        cout <<endl <<" "<< auxPar.first;
        cout << " = "<<auxPar.second;
    }
    return 0;
}
