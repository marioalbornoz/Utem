#include<iostream>
#include<queue>
using namespace std;
struct comparadorINT{
  bool operator()(int i, int j){
    return i>j;
  }
};

int main(){
  cout<<endl<<"Priority Queue (MIN)"<<endl;
  priority_queue<int,vector<int>,comparadorINT>h;
  h.push(24);
  h.push(32);
  h.push(1);
  h.push(22);
  h.push(11);
  while(!h.empty()){
    cout<<" "<<h.top();
    h.pop();
  }
  return 0;
}
