#include <iostream>
#include "ABB.h"
using namespace std;

void Crear(ABB &T)
{
     int i, k;
     cout << "Ingrese un entero (Info) : ";
     cin >> i;
     while(i != 0){
        cout << "Ingrese una clave (Key) : ";
        cin >> k;
        T.Insertar(k,i);
        cout << "Ingrese un entero (Info) : ";
        cin >> i;
    }
}
void Swap(ABB &T1, ABB &T2)
{  int k1, k2, i1, i2;
   if(!T1.Vacio() && !T2.Vacio())
   {  k1 = T1.GetKey();
      i1 = T1.GetInfo();
      k2 = T2.GetKey();
      i2 = T2.GetInfo();
      T1.Eliminar(k1);
      T2.Eliminar(k2);
      Swap(T1,T2);
      T1.Insertar(k2,i2);
      T2.Insertar(k1,i1);
    }
}

    int main()
    {
        cout << "ABB" << endl;
        ABB T;
        T.Insertar(12,12);
        T.Insertar(21,11);
        T.Insertar(3,13);
        T.Eliminar(3);
        T.Insertar(3,17);
        T.Ver();

/*    cout << "Buscar 1: " << T.Buscar(1) << endl;
    cout << "Existe 5: " << T.Existe(5) << endl;
    cout << "Vacio   : " << T.Vacio() << endl;
    cout << "Clave Raiz : " << T.GetKey() << endl;
    cout << "Info  Raiz : " << T.GetInfo() << endl;

    cout << "Eliminando el 3" << endl;
    T.Eliminar(3);
    T.Ver();
    cout << endl << endl;
    ABB R, R2;
    Crear(R);
    R.Ver();
    Crear(R2);
    R2.Ver();

    cout << "Swap" << endl;
    Swap(R2,R);
    cout << "R2" << endl;
    R2.Ver();
    cout << "R" << endl;
    R.Ver();
*/
    return 0;
}
