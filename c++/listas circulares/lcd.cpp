#include <iostream>
using namespace std;

/*Lista Circular Doble **/
struct TipoNodo{
    int info;
    struct TipoNodo *ant;
    struct TipoNodo *sig;
};

typedef TipoNodo *Lista;

void agregarNodoLCD(Lista &L, int valor) {
    Lista p;
    p = new(TipoNodo);
    p->info = valor;
    p->ant = p;
    p->sig = p;

    if(L == NULL)
        L = p;
    else{
        p->ant = L;
        p->sig = L->sig;
        p->sig->ant = p;
        L->sig = p;
    }
}

void Listar(Lista L){
    Lista p;
    cout << endl << "Lista LCD " << endl;
    if(L!=NULL){
        p = L;
        cout << "Valor : " << p->info << endl;
        p = p->sig;
        while(p != L){
            cout << "Valor : " << p->info << endl;
            p = p->sig;
        }
    }
}

void ListarR(Lista C){
    Lista p;
    cout << endl << "Lista LCD " << endl;
    if(C!=NULL){
        p = C;
        cout << "Valor : " << p->info << endl;
        p = p->ant;
        while(p != C){
            cout << "Valor : " << p->info << endl;
            p = p->ant;
        }
    }
}


int main() {
    cout << "Uso de LCD" << endl;
    Lista L;
    L = NULL;
    Listar(L);

    agregarNodoLCD(L,1);
    Listar(L);

    agregarNodoLCD(L,2);
    Listar(L);

    agregarNodoLCD(L,3);
    Listar(L);

    agregarNodoLCD(L,4);
    Listar(L);

    cout <<endl<< "Sentido inverso" <<endl;
    ListarR(L);


    return 0;
}
