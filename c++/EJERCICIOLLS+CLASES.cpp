#include<iostream>
#include<string>

using namespace std;

//declarativa de funciones

/*int verificarRut(lista,int);
void agregarAlumno(lista,string,int,int,int);
int contarAlumnos(lista,int);
void mostrarListasDeAlumnosporCarrera(lista,int);
void mostrarAlumno(lista);
*/
struct Alumno{
  int rut;
  string nombre;
  int edad;
  int tipodeCarrera;
  struct Alumno *siguiente;
};

typedef Alumno *lista;

int  verificarRut(lista l,int r)
{
  lista p;
  p=l;
  while (p!=NULL) {
    if(p->rut==r)return 1;
    p=p->siguiente;
  }
  return 0;
}



void agregarAlumno(lista &A, string name, int run, int old ,int carrera )
{
  int x=verificarRut(A,run);
  if(x==0){
  lista p,q;
  p=new(Alumno);
  p->rut=run;
  p->nombre=name;
  p->edad=old;
  p->tipodeCarrera=carrera;
    if(A==NULL)
    {
      A=p;
      p->siguiente=NULL;
    }
    else
    {
        q=A;
        while(q->siguiente!=NULL) q=q->siguiente;
        q->siguiente=p;


  }

}
else cout<<"\nRut ingresado ya esta en la lista: "<<endl;
}


int contarAlumnos(lista l, int carrera)
{
  int cont=0, informaticos=0, geomensura=0,alimentos=0, mecanicos=0;

  lista p;
  p=l;

  while(p!=NULL)
  {
      if(p->tipodeCarrera==carrera )cont++;

    p=p->siguiente;
  }

    return cont;
  }

 void mostrarListasDeAlumnosporCarrera(lista l,int carrera)
  {
    int x;
    x=contarAlumnos(l,carrera);
    if(carrera==1) cout<<"Cantidad de alumnos en Informatica: "<<x<<endl;
    if(carrera==2) cout<<"Cantidad de alumnos en Geomensura: "<<x<<endl;
    if(carrera==3) cout<<"Cantidad de alumnos de Alimentos: "<<x<<endl;
    if(carrera==4) cout<<"Cantidad de alumnos de Mecanica: "<<x<<endl;


  }


void mostrarAlumno(lista l)
{
  if(l!=NULL)
  {
    cout<<"\nEl nombre del alumno es: "<<l->nombre<<endl;
    cout<<"Rut: "<<l->rut<<endl;
    cout<<"Edad: "<<l->edad<<endl;
    cout<<"Carrera: "<<l->tipodeCarrera<<endl;
    mostrarAlumno(l->siguiente);
  }
}

/*void crearListaAlumnosSegunCarrera(lista l, int carrera,lista &p)
{
  if(l!=NULL)
  {
    while(l!=NULL)
    {
      if(l->tipodeCarrera==carrera)
          agregarAlumno(p,l->rut,l->nombre,l->edad,l->tipodeCarrera);
      l=l->siguiente;
    }
  }

*/





int main() {
  lista Persona;
  Persona=NULL;
  agregarAlumno(Persona,"Mario Albornoz",19002691,22,1);
  agregarAlumno(Persona,"Daniela Zuniga",28232442,18,2);
  agregarAlumno(Persona,"Gabriel Gonzalez",18923232,22,1);
  agregarAlumno(Persona,"Mario Albornoz",18002691,22,1);
  agregarAlumno(Persona,"Daniela Zuniga",20231442,18,2);
  agregarAlumno(Persona,"Daniela Zuniga",20631442,18,2);
  mostrarListasDeAlumnosporCarrera(Persona,2); //funcion declarada para mostrar cantidad de alumnos por carrera segun num.


  mostrarAlumno(Persona);
  return 0;
}
