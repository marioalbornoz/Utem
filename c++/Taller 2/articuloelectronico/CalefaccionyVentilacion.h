#ifndef CALEFACCIONYVENTILACION_H
#define CALEFACCIONYVENTILACION_H
#include "ArticulosElectronicos.h"
class CalefaccionyVentilacion: public ArticulosElectronicos
 // Estufas, Calientacamas, purificadores
{
    public:

        CalefaccionyVentilacion();
        CalefaccionyVentilacion(int,string,string,string,string,int,int,int,int,int);

        void setMaterial(string);
        string getMaterial();

        void setSuperficie(string);
        string getSuperficie();

        void setPotencia(int);
        int getPotencia();

        void setTipoTemperatura(string);
        string getTipoTemperatura();

        void setLargo(int);
        int getLargo();

        void setAncho(int);
        int getAncho();

        void setProfundidad(int);
        int getProfundidad();

    private:

        string Material;
        string Superficie; //cama, piso, muralla, pedestal,sobremesa
        string TipoTemperatura; //frio caliente
        int Potencia;
        int Largo;
        int Ancho;
        int Profundidad;
};


#endif // CALEFACCIONYVENTILACION_H
