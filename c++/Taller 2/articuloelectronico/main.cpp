#include <iostream>
#include<stdlib.h>
#include<iostream>
#include<fstream>
#include "stack"

#include "ArticulosElectronicos.h"
#include "ComputacionyTelefonos.h"
#include "TelefonosyTablets.h"
#include "LineaBlanca.h"
#include "CalefaccionyVentilacion.h"
#include "Audio.h"
#include "TV.h"

typedef stack <Computacion> TipoComputacion;
TipoComputacion listaComputador;
Computacion Computador;

typedef stack <TelefonosyTablets> TiposTelefonoT;
TiposTelefonoT listaTelefonos;

typedef stack <CalefaccionyVentilacion> TipoStackCalefaccion;
TipoStackCalefaccion listaCalefaccion;

typedef stack <LineaBlanca> TipoStackLineaBlanca;
TipoStackLineaBlanca listaServiciosLB;

typedef stack <Audio> TipoStackAudio;
TipoStackAudio listaDispositivosAudio;

typedef stack <TV>TipoStackTV;
TipoStackTV listaTV;


using namespace std;
char Menu()
{
    char  opcion=' ';
    do{
    cout<<"\t Bienvenido al Menu para Articulos Electronicos "<<endl;
    cout<<"\t\t Porfavor Elija una opcion:"<<endl;

    cout<<"1.- Agregar un Articulo a la Pila "<<endl;
    cout<<"2.- Mostrar los Articulos de la Pila "<<endl;
    cout<<"3.- Buscar Articulo en la Pila "<<endl;
    cout<<"4.- Salir "<<endl;

    cout<<endl<<"\tOpcion: ";
    cin>>opcion;
    if((opcion<'1')||(opcion>'4'))
    {
        cout<<"Opcion invalida, intente nuevamente"<<endl;

            getch();
            system("cls");
    }
    }while((opcion<'1')||(opcion>'5'));

    return opcion;
}

void agregarArticulo(int articulo) {

    switch (articulo) {
    case 1: {//COMPUTACION
        int camara, codigo, ram, mi;
        float pulgadas;
        string soperativo, nombre, marca, procesador, wifi;

        Computacion computadorPrueba;

        cout<<"\n Ingrese el nombre o modelo del pc(notebook,pc,ultrabook,etc): ";
        cin>>nombre;
        computadorPrueba.setNombre(nombre);

        cout<<"\nIngrese la marca del objeto: "<<endl;
        cin>>marca;
        computadorPrueba.setMarca(marca);

        cout<<"\nCodigo del producto: "<<endl;
        cin>>codigo;
        computadorPrueba.setCodigo(codigo);

        cout<<"\nProcesador: "<<endl;
        cin>>procesador;
        computadorPrueba.setProcesador(procesador);


        cout<<"\nSistema Operativo: "<<endl;
        cin>>soperativo;
        computadorPrueba.setSO(soperativo);


        cout<<"Conectividad del articulo: "<<endl;
        cin>>wifi;
        computadorPrueba.setConectividad(wifi);


        cout<<"Camara del dispositivo: "<<endl;
        cin>>camara;
        computadorPrueba.setCamara(camara);


        cout<<"Memoria ram: "<<endl;
        cin>>ram;
        computadorPrueba.setMemoriaRam(ram);

        cout<<"Memoria Interna: "<<endl;
        cin>>mi;
        computadorPrueba.setMemoriaInterna(mi);

        cout<<"Tamano de pantalla(pulgadas en sistema decimal con punto): "<<endl;
        cin>>pulgadas;
        computadorPrueba.setPulgadas(pulgadas);

        listaComputador.push(computadorPrueba);
        cout<<"El ingreso fue correcto, Usted volvera al Menu."<<endl;

      break;
    }

    case 2: //TELEFONOS Y TABLETS
    {
        int codigon,camera,c,mram,minterna,n,i,wififrecuencia;

        float pulgadas;
        string procesador,soperativo,conectivity,name,marca_,z,company,lectordactilar;
        TelefonosyTablets TyT;

      cout<<"\ningrese el nombre del pc(notebook,pc,ultrabook,etc): ";
      cin>>name;
      TyT.setNombre(name);

      cout<<"\nIngrese la marca del objeto: "<<endl;
      cin>>marca_;
      TyT.setMarca(marca_);

      cout<<"\nCodigo del producto: "<<endl;
      cin>>codigon;
      TyT.setCodigo(codigon);

      cout<<"\nProcesador: "<<endl;
      cin>>procesador;
      TyT.setProcesador(procesador);

      cout<<"\nSistema Operativo: "<<endl;
      cin>>soperativo;
      TyT.setSO(soperativo);
      cout<<"Conectividad del articulo: "<<endl;
      cin>>conectivity;
      TyT.setConectividad(conectivity);

      cout<<"Camara del dispositivo: "<<endl;
      cin>>camera;
      TyT.setCamara(camera);

      cout<<"Memoria ram: "<<endl;
      cin>>mram;
      TyT.setMemoriaRam(mram);

      cout<<"Memoria Interna: "<<endl;
      cin>>minterna;
      TyT.setMemoriaInterna(minterna);

      cout<<"Tamanio de pantalla(use sistema decimal, con punto): "<<endl;
      cin>>pulgadas;
      TyT.setPulgadas(pulgadas);

      cout<<"Definamos la compa�ia: "<<endl;
      cin>>company;
      TyT.setCompania(company);

      cout<<"Tipo de Wifi en ghz: "<<endl;
      cin>>wififrecuencia;
      TyT.setWifi(wififrecuencia);

      cout<<"�Tiene lector de huellas?: "<<endl;
      cin>>lectordactilar;
      TyT.setlectorHuella(lectordactilar);


     /*Funcion Mostrar*/
    cout<<"El ingreso fue correcto, Usted volvera al Menu"<<endl;
    break;
    }

    case 3: //CALEFACCION Y VENTILACION
    {
            int codigo,potencia,largo,ancho,profundidad;
            string temperatura,material,superficie,nombre,marca;
            CalefaccionyVentilacion climatizador;

        cout<<"\ningrese el nombre del articulo: ";
        cin>>nombre;
        climatizador.setNombre(nombre);

        cout<<"\nIngrese la marca del objeto: "<<endl;
        cin>>marca;
        climatizador.setMarca(marca);

        cout<<"\nCodigo del producto: "<<endl;
        cin>>codigo;
        climatizador.setCodigo(codigo);

        cout<<"\nMaterial: "<<endl;
        cin>>material;
        climatizador.setMaterial(material);

        cout<<"\nSuperficie: "<<endl;
        cin>>superficie;
        climatizador.setSuperficie(superficie);

        cout<<"Potencia: "<<endl;
        cin>>potencia;
        climatizador.setPotencia(potencia);

        cout<<"Tipo de temperatura: "<<endl;
        cin>>temperatura;
        climatizador.setTipoTemperatura(temperatura);

        cout<<"Largo: "<<endl;
        cin>>largo;
        climatizador.setLargo(largo);

        cout<<"Ancho: "<<endl;
        cin>>ancho;
        climatizador.setAncho(ancho);

        cout<<"Profundidad: "<<endl;
        cin>>profundidad;
        climatizador.setProfundidad(profundidad);


      /*Funcion Mostrar*/
      cout<<"El ingreso fue correcto, Usted volvera al Menu"<<endl;
      break;
    }
    case 4: //LINBA BLANCA
    {
            int codigo,peso,ancho,alto,profundidad;
            string color,clasificacion,nombre,marca;
            LineaBlanca servicioslb;

        cout<<"\ningrese el nombre del articulo: ";
        cin>>nombre;
        servicioslb.setNombre(nombre);

        cout<<"\nIngrese la marca del objeto: "<<endl;
        cin>>marca;
        servicioslb.setMarca(marca);

        cout<<"\nCodigo del producto: "<<endl;
        cin>>codigo;
        servicioslb.setCodigo(codigo);

        cout<<"\nColor: "<<endl;
        cin>>color;
        servicioslb.setColor(color);

        cout<<"\nClasificacion energetica: "<<endl;
        cin>>clasificacion;
        servicioslb.setClasificacionEnergetica(clasificacion);

        cout<<"Peso: "<<endl;
        cin>>peso;
        servicioslb.setPeso(peso);

        cout<<"Ancho: "<<endl;
        cin>>ancho;
        servicioslb.setAncho(ancho);

        cout<<"Alto: "<<endl;
        cin>>alto;
        servicioslb.setAltura(alto);

        cout<<"Profundidad: "<<endl;
        cin>>profundidad;
        servicioslb.setProfundidad(profundidad);


          /*Funcion Mostrar*/
        cout<<"El ingreso fue correcto, Usted volvera al Menu"<<endl;
        break;
        }

    case 5: //MULTIMEDIA
     {
            int codigo,b,potencia,i,n;
            string nombre,marca,smart;
            Audio equiposonido;

        cout<<"\n\nIngrese el nombre del producto multimedia: "<<endl;
        cin>>nombre;
        equiposonido.setNombre(nombre);

        cout<<"Ingrese el codigo del Objeto: "<<endl;
        cin>>codigo;
        equiposonido.setCodigo(codigo);

        cout<<"Ingrese marca del producto: "<<endl;
        cin>>marca;
        equiposonido.setMarca(marca);

        cout<<"�Es smart?(Si / no)"<<endl;
        cin>>smart;
        equiposonido.setSmart(smart);

        cout<<"Potencia de Sonido(W): ";
        cin>>potencia;
        equiposonido.setPotencia(potencia);

     }
    case 6: //TV
    {

        int codigo; float pulgada;
        string nombre,marca,smart;
        TV televisor;

        cout<<"\n\nIngrese el nombre del producto multimedia: "<<endl;
        cin>>nombre;
        televisor.setNombre(nombre);


        cout<<"Ingrese el codigo del Objeto: "<<endl;
        cin>>codigo;
        televisor.setCodigo(codigo);

        cout<<"Ingrese marca del producto: "<<endl;
        cin>>marca;
        televisor.setMarca(marca);

        cout<<"�Es smart?(S/N)"<<endl;
        cin>>smart;
        televisor.setSmart(smart);

        cout<<"Pulgadas de Pantalla(use el sistema decimal con punto): ";
        cin>>pulgada;
        televisor.setPulgadas(pulgada);

        cout<<"\nArticulo ingresado satisfactoriamente."<<endl;
    break;



    }
    }
}


void mostrarArticulo(char tipo)
{
    switch (tipo){
    case '1':
    {
        Computacion prueba;
        while(!listaComputador.empty())
        {
                prueba = listaComputador.top();

                cout<<"Nombre: "<<prueba.getNombre()<<endl;
                cout<<"Codigo: "<<prueba.getCodigo()<<endl;
                cout<<"Marca: "<<prueba.getMarca()<<endl;

                cout<<"Sistema Operativo: "<<prueba.getSO()<<endl;
                cout<<"Conectividad: "<<prueba.getConectividad()<<endl;
                cout<<"Camara: "<<prueba.getCamara()<<endl;
                cout<<"Memoria Ram: "<<prueba.getMemoriaRam()<<endl;
                cout<<"Memoria Interna: "<<prueba.getMemoriaInterna()<<endl;
                cout<<"Pulgadas"<<prueba.getPulgadas()<<endl;
                cout<<"-------- "<<endl;

            listaComputador.pop();
            }
    }
    break;

    case '2':

    {
        TiposTelefonoT s=listaTelefonos;

        while(!s.empty())
            {
                TelefonosyTablets telefonosMostrar;
                telefonosMostrar=s.top();

                cout<<"Nombre: "<<telefonosMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<telefonosMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<telefonosMostrar.getMarca()<<endl;

                cout<<"Compania: "<<telefonosMostrar.getCompania()<<endl;
                cout<<"Tipo: "<<telefonosMostrar.getTipo()<<endl;
                cout<<"Wifi: "<<telefonosMostrar.getWifi()<<endl;
                cout<<"Lector de Huella: "<<telefonosMostrar.getlectorHuella()<<endl;
                cout<<"-------- "<<endl;

            s.pop();

            }
    }

    break;

    case '3':
    {
        TipoStackCalefaccion s=listaCalefaccion;
        while(!s.empty())
            {
                CalefaccionyVentilacion calventMostrar;
                calventMostrar=s.top();

                cout<<"Nombre: "<<calventMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<calventMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<calventMostrar.getMarca()<<endl;

                cout<<"Superficie: "<<calventMostrar.getSuperficie()<<endl;
                cout<<"Tipo de Temperatura: "<<calventMostrar.getTipoTemperatura()<<endl;
                cout<<"Potencia: "<<calventMostrar.getPotencia()<<endl;
                cout<<"Largo: "<<calventMostrar.getLargo()<<endl;
                cout<<"Ancho: "<<calventMostrar.getAncho()<<endl;
                cout<<"Profundidad: "<<calventMostrar.getProfundidad()<<endl;
                cout<<"-------- "<<endl;

            s.pop();

            }
    }
    break;

    case '4':
    {
        TipoStackLineaBlanca s=listaServiciosLB;
        while(!s.empty())
            {
                LineaBlanca lineablancaMostrar;
                lineablancaMostrar=s.top();

                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<lineablancaMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<lineablancaMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<lineablancaMostrar.getMarca()<<endl;

                cout<<"Color: "<<lineablancaMostrar.getColor()<<endl;
                cout<<"Clasificacion Energetica: "<<lineablancaMostrar.getClasificacionEnergetica()<<endl;
                cout<<"Peso: "<<lineablancaMostrar.getPeso()<<endl;
                cout<<"Ancho: "<<lineablancaMostrar.getAncho()<<endl;
                cout<<"Altura: "<<lineablancaMostrar.getAltura()<<endl;
                cout<<"Profundidad: "<<lineablancaMostrar.getProfundidad()<<endl;
                cout<<"-------- "<<endl;

            s.pop();

            }
    }
    break;

    case '5':
    {
        TipoStackAudio s=listaDispositivosAudio;
       while(!s.empty())
            {
                Audio audioMostrar;
                audioMostrar=s.top();

                cout<<"Nombre: "<<audioMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<audioMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<audioMostrar.getMarca()<<endl;

                cout<<"Calidad: "<<audioMostrar.getCalidad()<<endl;
                cout<<"Potencia: "<<audioMostrar.getPotencia()<<endl;
                cout<<"Smart: "<<audioMostrar.getSmart()<<endl;
                cout<<"-------- "<<endl;

            s.pop();

            }
    }

    break;

    case '6':
    {
        TipoStackTV s=listaTV;
        while(!s.empty())
            {
                TV tvMostrar;
                tvMostrar=s.top();

                cout<<"Nombre: "<<tvMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<tvMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<tvMostrar.getMarca()<<endl;

                cout<<"Smart: "<<tvMostrar.getSmart()<<endl;
                cout<<"Pulgadas: "<<tvMostrar.getPulgadas()<<endl;
                cout<<"-------- "<<endl;

            s.pop();

            }
    break;
}
    }
}

void buscarArticulo(int codigo , char categoria)
{
    switch (categoria)
    {
    case '1':
    {
        TipoComputacion s=listaComputador;

        stack <Computacion>aux;
        while(!s.empty())
            {   Computacion computadorMostrar;
                computadorMostrar=s.top();
                s.pop();
            if(computadorMostrar.getCodigo()==codigo)
                {
                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<computadorMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<computadorMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<computadorMostrar.getMarca()<<endl;

                cout<<"Sistema Operativo: "<<computadorMostrar.getSO()<<endl;
                cout<<"Conectividad: "<<computadorMostrar.getConectividad()<<endl;
                cout<<"Camara: "<<computadorMostrar.getCamara()<<endl;
                cout<<"Memoria Ram: "<<computadorMostrar.getMemoriaRam()<<endl;
                cout<<"Memoria Interna: "<<computadorMostrar.getMemoriaInterna()<<endl;
                cout<<"Pulgadas"<<computadorMostrar.getPulgadas()<<endl;
                cout<<"-------- "<<endl;
                }
            }
        while(!aux.empty())
            {
                s.push(aux.top());
                aux.pop();
            }
    }
    break;

    case '2':
    {
        TiposTelefonoT s=listaTelefonos;
        stack <TelefonosyTablets> aux;
        while(!s.empty())
            {   TelefonosyTablets telefonosMostrar;
                telefonosMostrar=s.top();
                s.pop();
            if(telefonosMostrar.getCodigo()==codigo)
                {
                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<telefonosMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<telefonosMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<telefonosMostrar.getMarca()<<endl;

                cout<<"Compania: "<<telefonosMostrar.getCompania()<<endl;
                cout<<"Tipo: "<<telefonosMostrar.getTipo()<<endl;
                cout<<"Wifi: "<<telefonosMostrar.getWifi()<<endl;
                cout<<"Lector de Huella: "<<telefonosMostrar.getlectorHuella()<<endl;
                cout<<"-------- "<<endl;
                }
            }
        while(!aux.empty())
            {
                s.push(aux.top());
                aux.pop();
            }
    }
    break;

    case '3':
    {
        TipoStackCalefaccion s=listaCalefaccion;

        stack <CalefaccionyVentilacion> aux;

        while(!s.empty())
            {   CalefaccionyVentilacion calventMostrar;
                calventMostrar=s.top();
                s.pop();
            if(calventMostrar.getCodigo()==codigo)
                {
                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<calventMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<calventMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<calventMostrar.getMarca()<<endl;

                cout<<"Superficie: "<<calventMostrar.getSuperficie()<<endl;
                cout<<"Tipo de Temperatura: "<<calventMostrar.getTipoTemperatura()<<endl;
                cout<<"Potencia: "<<calventMostrar.getPotencia()<<endl;
                cout<<"Largo: "<<calventMostrar.getLargo()<<endl;
                cout<<"Ancho: "<<calventMostrar.getAncho()<<endl;
                cout<<"Profundidad: "<<calventMostrar.getProfundidad()<<endl;
                cout<<"-------- "<<endl;
                }
            }
        while(!aux.empty())
            {
                s.push(aux.top());
                aux.pop();
            }
    }
    break;

    case '4':
    {
        TipoStackLineaBlanca s=listaServiciosLB;
        stack <LineaBlanca> aux;
        while(!s.empty())
            {   LineaBlanca lineablancaMostrar;
                lineablancaMostrar=s.top();
                s.pop();
            if(lineablancaMostrar.getCodigo()==codigo)
                {
                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<lineablancaMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<lineablancaMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<lineablancaMostrar.getMarca()<<endl;

                cout<<"Color: "<<lineablancaMostrar.getColor()<<endl;
                cout<<"Clasificacion Energetica: "<<lineablancaMostrar.getClasificacionEnergetica()<<endl;
                cout<<"Peso: "<<lineablancaMostrar.getPeso()<<endl;
                cout<<"Ancho: "<<lineablancaMostrar.getAncho()<<endl;
                cout<<"Altura: "<<lineablancaMostrar.getAltura()<<endl;
                cout<<"Profundidad: "<<lineablancaMostrar.getProfundidad()<<endl;
                cout<<"-------- "<<endl;
                }
            }
        while(!aux.empty())
            {
                s.push(aux.top());
                aux.pop();
            }
    }
    break;

    case '5':
    {
        TipoStackAudio s=listaDispositivosAudio;
        stack <Audio> aux;
        while(!s.empty())
            {   Audio audioMostrar;
                audioMostrar=s.top();
                s.pop();
            if(audioMostrar.getCodigo()==codigo)
                {
                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<audioMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<audioMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<audioMostrar.getMarca()<<endl;

                cout<<"Calidad: "<<audioMostrar.getCalidad()<<endl;
                cout<<"Potencia: "<<audioMostrar.getPotencia()<<endl;
                cout<<"Smart: "<<audioMostrar.getSmart()<<endl;
                cout<<"-------- "<<endl;
                }
            }
        while(!aux.empty())
            {
                s.push(aux.top());
                aux.pop();
            }
    }

    break;

    case '6':
    {
        TipoStackTV s=listaTV;
        stack <TV> aux;
        while(!s.empty())
            {   TV tvMostrar;
                tvMostrar=s.top();
                s.pop();
            if(tvMostrar.getCodigo()==codigo)
                {
                cout<<"El articulo fue encontrado"<<endl;
                cout<<"Nombre: "<<tvMostrar.getNombre()<<endl;
                cout<<"Codigo: "<<tvMostrar.getCodigo()<<endl;
                cout<<"Marca: "<<tvMostrar.getMarca()<<endl;

                cout<<"Smart: "<<tvMostrar.getSmart()<<endl;
                cout<<"Pulgadas: "<<tvMostrar.getPulgadas()<<endl;
                cout<<"-------- "<<endl;
                }
            }
        while(!aux.empty())
            {
                s.push(aux.top());
                aux.pop();
            }
    }
    break;
}

}

int main()
{
    char opcion=' ', tipo=' ',categoria=' ';
    int articulo=0, codigo=0;

    stack<Computacion> aux;

    do{
        opcion=Menu();

        switch(opcion)
        {

        case '1':

            cout<<endl<<"\t\tHaz elegido AGREGAR un Articulo a la Pila"<<endl;
            cout<<"\t\t\t Seleccion el tipo de articulo"<<endl<<endl;

            cout<<"1.-  Notebook o pc de escritorio"<<endl;
            cout<<"2.-  Tablets o smartphone"<<endl;
            cout<<"3.-  Ventilacion o calefaccion"<<endl;
            cout<<"4.-  Linea blanca"<<endl;
            cout<<"5.-  Audio"<<endl;
            cout<<"6.-  TV"<<endl;
            cout<<"7.-  Salir"<<endl;

            cin>>articulo;
            system("cls");
            agregarArticulo(articulo);



            break;

        case '2':
            cout<<endl<<"\t\tHaz elegido MOSTRAR la lista de articulos"<<endl;
            cout<<"\t\t\t Seleccion el tipo de articulo"<<endl<<endl;

            cout<<"1.-  Notebook o pc de escritorio"<<endl;
            cout<<"2.-  Tablets o smartphone"<<endl;
            cout<<"3.-  Ventilacion o calefaccion"<<endl;
            cout<<"4.-  Linea blanca"<<endl;
            cout<<"5.-  Audio"<<endl;
            cout<<"6.-  TV"<<endl;
            cout<<"7.-  Salir"<<endl;
            cin>>tipo;

            mostrarArticulo(tipo);

            break;

               case '3':

            cout<<endl<<"Haz elegido BUSCAR un Articulo"<<endl;

            cout<<endl<<"Ingresa el codigo del articulo"<<endl;

            cin>>codigo;
            cout<<endl<<"Ahora ingresa el tipo de producto (categoria)"<<endl;
            cout<<"1.-  Notebook o pc de escritorio"<<endl;
            cout<<"2.-  Tablets o smartphone"<<endl;
            cout<<"3.-  Ventilacion o calefaccion"<<endl;
            cout<<"4.-  Linea blanca"<<endl;
            cout<<"5.-  Audio"<<endl;
            cout<<"6.-  TV"<<endl;
            cout<<"7.-  Salir"<<endl;
            cin>>categoria;

            buscarArticulo(codigo, categoria);


            break;

case'4':
            cout<<endl<<"Hasta la proxima"<<endl;
            break;


        }
        getch();
        system("cls");
    }while(opcion!='5');
    return 0;

}
