#ifndef AUDIO_H
#define AUDIO_H
#include "Multimedia.h"

class Audio: public Multimedia
{
    public:

        Audio();
        Audio(int,string,string,string,int,int);

        void setCalidad(int);
        int getCalidad();
        void setPotencia(int);
        int getPotencia();

    private:
        int Calidad;
        int Potencia;
};
#endif // AUDIO_H
