#ifndef MULTIMEDIA_H
#define MULTIMEDIA_H
#include "ArticulosElectronicos.h"

class Multimedia: public ArticulosElectronicos
{
    public:
        Multimedia();
        Multimedia(int,string,string,string);

        void setSmart(string);
        string getSmart();

    private:
        string Smart;
};
#endif // MULTIMEDIA_H
