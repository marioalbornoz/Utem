#ifndef ArticulosElectronicos_H
#define ArticulosElectronicos_H

#include<stdlib.h>
#include<iostream>
#include<fstream>
#include<stack>
#include <string>
using namespace std;

class ArticulosElectronicos
{
    public:

        ArticulosElectronicos();
        ArticulosElectronicos(int,string,string);
        virtual ~ArticulosElectronicos();

        void setCodigo(int);
        int getCodigo();
        void setNombre(string);
        string getNombre();

        bool Buscar();

        void setMarca(string);
        string getMarca();



    private:
        int codProducto;
        string nombreProducto;
        string marca;
};


#endif // ArticulosElectronicos_H
