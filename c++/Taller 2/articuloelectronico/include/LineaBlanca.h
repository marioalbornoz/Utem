#ifndef LINEABLANCA_H
#define LINEABLANCA_H
#include "ArticulosElectronicos.h"

class LineaBlanca: public ArticulosElectronicos
//(Lavadoras, Centrifugas,Lavavajillas,secadoras) (Refrigeradores, Freezers, Frio directo, Frigobar, Cavas de vino)
{
    public:

        LineaBlanca();
        LineaBlanca(int,string,string,string, string, int, int, int,int);

        void setColor(string);
        string getColor();

        void setClasificacionEnergetica(string);
        string getClasificacionEnergetica();

        void setPeso(int);
        int getPeso();

        void setAncho(int);
        int getAncho();

        void setAltura(int);
        int getAltura();

        void setProfundidad(int);
        int getProfundidad();

    private:

        string Color;
        string ClasificacionEnergetica;
        int Peso;
        int Ancho;
        int Altura;
        int Profundidad;
};
#endif // LINEABLANCA_H
