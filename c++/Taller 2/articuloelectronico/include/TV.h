#ifndef TV_H
#define TV_H
#include "Multimedia.h"

class TV: public Multimedia{
    public:
        TV();
        TV(int,string,string,string,float);

        void setPulgadas(float);
        float getPulgadas();

    private:
        float Pulgadas;
};
#endif // TV_H
