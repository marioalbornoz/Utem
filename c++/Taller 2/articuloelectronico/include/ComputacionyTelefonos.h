#ifndef Computacion_H
#define Computacion_H
#include "ArticulosElectronicos.h"
#include <string>
using namespace std;

class Computacion: public ArticulosElectronicos {
    public:
        Computacion();
        Computacion(int,string,string,string,string, string,int,int,int,float);

        void setProcesador(string);
        string getProcesador();

        void setConectividad(string);
        string getConectividad();

        void setSO(string);
        string getSO();

        void setCamara(int);
        int getCamara();

        void setMemoriaRam(int);
        int getMemoriaRam();

        void setPulgadas(float);
        float getPulgadas();

        void setMemoriaInterna(int);
        int getMemoriaInterna();

    private:

        string Procesador;
        string SO;
        string Conectividad;
        int Camara;
        int MemoriaRam;
        int MemoriaInterna;
        float Pulgadas;

};

#endif // Computacion_H
