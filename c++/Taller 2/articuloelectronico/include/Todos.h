#ifndef TODOS_H
#define TODOS_H
#include "ArticulosElectronicos.h"


using namespace std;


class CalefaccionyVentilacion: public ArticulosElectronicos
 // Estufas, Calientacamas, purificadores
{
    public:

        CalefaccionyVentilacion();
        CalefaccionyVentilacion(int,string,string,string,string,int,int,int,int,int);

        void setMaterial(string);
        string getMaterial();

        void setSuperficie(string);
        string getSuperficie();

        void setPotencia(int);
        int getPotencia();

        void setTipoTemperatura(string);
        string getTipoTemperatura();

        void setLargo(int);
        int getLargo();

        void setAncho(int);
        int getAncho();

        void setProfundidad(int);
        int getProfundidad();

    private:

        string Material;
        string Superficie; //cama, piso, muralla, pedestal,sobremesa
        string TipoTemperatura; //frio caliente
        int Potencia;
        int Largo;
        int Ancho;
        int Profundidad;
};

/*class LineaBlanca: public ArticulosElectronicos
//(Lavadoras, Centrifugas,Lavavajillas,secadoras) (Refrigeradores, Freezers, Frio directo, Frigobar, Cavas de vino)
{
    public:

        LineaBlanca();
        LineaBlanca(int,string,string,string, string, int, int, int,int);

        void setColor(string);
        string getColor();

        void setClasificacionEnergetica(string);
        string getClasificacionEnergetica();

        void setPeso(int);
        int getPeso();

        void setAncho(int);
        int getAncho();

        void setAltura(int);
        int getAltura();

        void setProfundidad(int);
        int getProfundidad();

    private:

        string Color;
        string ClasificacionEnergetica;
        int Peso;
        int Ancho;
        int Altura;
        int Profundidad;
};
*/

class Multimedia: public ArticulosElectronicos
{
    public:
        Multimedia();
        Multimedia(int,string,string,string);

        void setSmart(string);
        string getSmart();

    private:
        string Smart;
};

class Audio: public Multimedia
{
    public:

        Audio();
        Audio(int,string,string,string,int,int);

        void setCalidad(int);
        int getCalidad();
        void setPotencia(int);
        int getPotencia();

    private:
        int Calidad;
        int Potencia;
};

class TV: public Multimedia{
    public:
        TV();
        TV(int,string,string,string,float);

        void setPulgadas(float);
        float getPulgadas();

    private:
        float Pulgadas;
};

#endif // TODOS_H
