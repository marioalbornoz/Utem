#ifndef TELEFONOSYTABLETS_H
#define TELEFONOSYTABLETS_H
#include "ComputacionyTelefonos.h"

class TelefonosyTablets: public Computacion
{
    public:

        TelefonosyTablets();
        TelefonosyTablets(int,string,string,string,string,string,int,int,int,float,string,string,float,string);

        void setCompania(string);
        string getCompania();

        void setTipo(string);
        string getTipo();

        void setWifi(float);
        float getWifi();

        void setlectorHuella(string);
        string getlectorHuella();



    private:

        string Compania;
        string Tipo; //smartphone, tablet, telefono
        float Wifi; //2.4 5 ghz
        string LectorHuella; //S N
};


#endif // TELEFONOSYTABLETS_H
