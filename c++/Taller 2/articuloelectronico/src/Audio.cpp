#include "Audio.h"

Audio::Audio()
{
    Calidad=0;
    Potencia=0;
}

Audio::Audio(int codigo, string nombre,string marca,string smart,int calidad,int potencia)
:Multimedia(codigo,nombre,marca,smart)
{
    /********************************************/
}

void Audio::setCalidad(int calidad)
{
    Calidad=calidad;
}

int Audio::getCalidad()
{
    return Calidad;
}

void Audio::setPotencia(int potencia)
{
    Potencia=potencia;
}

int Audio::getPotencia()
{
    return Potencia;
}
