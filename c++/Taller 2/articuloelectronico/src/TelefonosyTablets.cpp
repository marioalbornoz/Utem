#include "TelefonosyTablets.h"

TelefonosyTablets::TelefonosyTablets()
{
    Compania="No homologado";
    Tipo="No a elegido el tipo del articulo";
    Wifi=0.0;
    LectorHuella="No tiene";
}

TelefonosyTablets::TelefonosyTablets(int codigo, string nombre,string marca,string procesador,string soperativo,string conectividad,int camara,int ram,int minterna,float pulgadas, string compania,string tipo,float wifi,string lectorhuella)
:Computacion(codigo,nombre,marca,procesador,soperativo,conectividad,camara,ram,minterna,pulgadas)
{
    /*************************************************************************************************************/
}

void TelefonosyTablets::setCompania(string compania)
{
    Compania=compania;
}

string TelefonosyTablets::getCompania()
{
    return Compania;
}

void TelefonosyTablets::setTipo(string tipo)
{
    Tipo=tipo;
}

string TelefonosyTablets::getTipo()
{
    return Tipo;
}


void TelefonosyTablets::setWifi(float wifi)
{
    Wifi=wifi;
}

float TelefonosyTablets::getWifi()
{
    return Wifi;
}

void TelefonosyTablets::setlectorHuella(string lectoruhuella)
{
    LectorHuella=lectoruhuella;
}

string TelefonosyTablets::getlectorHuella()
{
    return LectorHuella;
}
