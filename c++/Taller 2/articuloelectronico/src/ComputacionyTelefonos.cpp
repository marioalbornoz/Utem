#include "ComputacionyTelefonos.h"

Computacion::Computacion()
{
    Procesador="Ningun procesador elegido";
    SO="Ningun Sistema operativo definido";
    Conectividad="Sin parametros definidos";
    Camara=0;
    MemoriaRam=0;
    MemoriaInterna=0;
    Pulgadas=0.0;
}

Computacion::Computacion(int codigo ,string nombre, string marca, string procesador, string soperativo, string conectividad, int camara,int ram, int minterna, float pulgadas)
:ArticulosElectronicos(codigo,nombre,marca)

{
    Procesador=procesador;
    SO=soperativo;
    Conectividad=conectividad;
    Camara=camara;
    MemoriaRam=ram;
    MemoriaInterna=minterna;
    Pulgadas=pulgadas;

    /*****************************************************************************/
}

void Computacion::setProcesador(string procesador)
{
    Procesador=procesador;
}

string Computacion::getProcesador()
{
    return Procesador;
}

void Computacion::setConectividad(string conectividad)
{
    Conectividad=conectividad;
}

string Computacion::getConectividad()
{
    return Conectividad;
}

void Computacion::setSO(string soperativo)
{
    SO=soperativo;
}

string Computacion::getSO()
{
    return SO;
}

void Computacion::setCamara(int camara)
{
    Camara=camara;
}

int Computacion::getCamara()
{
    return Camara;
}

void Computacion::setMemoriaRam(int ram)
{
    MemoriaRam=ram;
}

int Computacion::getMemoriaRam()
{
    return MemoriaRam;
}

void Computacion::setPulgadas(float pulgadas)
{
    Pulgadas=pulgadas;
}

float Computacion::getPulgadas()
{
    return Pulgadas;
}

void Computacion::setMemoriaInterna(int minterna)
{
    MemoriaInterna=minterna;
}

int Computacion::getMemoriaInterna()
{
    return MemoriaInterna;
}
