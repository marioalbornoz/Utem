#include "TV.h"

TV::TV()
{
    Pulgadas=0.0;
}

TV::TV(int codigo, string nombre,string marca,string smart,float pulgadas)
:Multimedia(codigo,nombre,marca,smart)
{

}

void TV::setPulgadas(float pulgadas)
{
    Pulgadas=pulgadas;
}

float TV::getPulgadas()
{
    return Pulgadas;
}
