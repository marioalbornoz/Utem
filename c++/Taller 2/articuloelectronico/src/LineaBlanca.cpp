#include "LineaBlanca.h"

LineaBlanca::LineaBlanca()
{
    Color="Color no definido";
    ClasificacionEnergetica="Z++";
    Peso=0;
    Ancho=0;
    Altura=0;
    Profundidad=0;
}

LineaBlanca::LineaBlanca(int codigo, string nombre,string marca,string color,string clasificacionEnergetica,int peso, int ancho, int altura, int profundidad)
:ArticulosElectronicos(codigo,nombre,marca)
{

}

void LineaBlanca::setColor(string color)
{
    Color=color;
}

string LineaBlanca::getColor()
{
    return Color;
}

void LineaBlanca::setClasificacionEnergetica(string clasificacionenergetica)
{
    ClasificacionEnergetica=clasificacionenergetica;
}

string LineaBlanca::getClasificacionEnergetica()
{
    return ClasificacionEnergetica;
}

void LineaBlanca::setPeso(int peso)
{
    Peso=peso;
}

int LineaBlanca::getPeso()
{
    return Peso;
}

void LineaBlanca::setAncho(int ancho)
{

}

int LineaBlanca::getAncho()
{

}

void LineaBlanca::setAltura(int altura)
{
    Altura=altura;
}

int LineaBlanca::getAltura()
{
    return Altura;
}

void LineaBlanca::setProfundidad(int profudidad)
{
    Profundidad=profudidad;
}

int LineaBlanca::getProfundidad()
{
    return Profundidad;
}
