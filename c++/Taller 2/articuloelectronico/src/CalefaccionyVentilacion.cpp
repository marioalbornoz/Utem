#include "CalefaccionyVentilacion.h"

CalefaccionyVentilacion::CalefaccionyVentilacion()
{
    Material="Material no definido";
    Superficie="Superfecie no definida, debe definir para que tipo de superficie \n";
    TipoTemperatura="Tipo de temperatura no difinido \n";
    Potencia=0;
    Largo=0;
    Ancho=0;
    Profundidad=0;
}

CalefaccionyVentilacion::CalefaccionyVentilacion(int codigo,string nombre,string marca,string material,string superficie,int watt,int temperatura,int largo,int ancho_,int profundidad)
:ArticulosElectronicos(codigo,nombre,marca)
{
    /************************************************************/
}

void CalefaccionyVentilacion::setMaterial(string material)
{
    Material=material;
}

string CalefaccionyVentilacion::getMaterial()
{
    return Material;
}

void CalefaccionyVentilacion::setSuperficie(string superficie)
{
    Superficie=superficie;
}

string CalefaccionyVentilacion::getSuperficie()
{
    return Superficie;
}

void CalefaccionyVentilacion::setTipoTemperatura(string tipotemperatura)
{
    TipoTemperatura=tipotemperatura;
}

string CalefaccionyVentilacion::getTipoTemperatura()
{
    return TipoTemperatura;
}

void CalefaccionyVentilacion::setPotencia(int watt)
{
    Potencia=watt;
}

int CalefaccionyVentilacion::getPotencia()
{
    return Potencia;
}


void CalefaccionyVentilacion::setLargo(int largo)
{
    Largo=largo;
}

int CalefaccionyVentilacion::getLargo()
{
    return Largo;
}

void CalefaccionyVentilacion::setAncho(int ancho)
{
    Ancho=ancho;
}

int CalefaccionyVentilacion::getAncho()
{
    return Ancho;
}

void CalefaccionyVentilacion::setProfundidad(int profundidad)
{
    Profundidad=profundidad;
}

int CalefaccionyVentilacion::getProfundidad()
{
    return Profundidad;
}
