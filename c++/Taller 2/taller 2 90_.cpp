//TALLER N2

#include<iostream>
#include<stack>
using namespace std;


class articulosElectronicos{
public:
  articulosElectronicos();
  articulosElectronicos(int,string,string);

  void setCodigo(int);
  int getCodigo();

  void setNombre(string);
  string getNombre();

  bool Buscar();
  void setMarca(string);
  string getMarca();



private:
  int codProducto;
  string nombreProducto;
  string marca;



};
/*class computacionTecno:public articulosElectronicos{
  public:
    computacionTecno();
    computacionTecno(int,int,float,string);

    void setRam(int);
    int getRam();

    void setPulgadas(float);
    float getPulgadas();

    void setDiscoDuro(int);
    int getDiscoDuro();

    void setSO(string);
    string getSO();

  private:
    int ram;
    int discoDuro;
    float pulgadas;
    string SO;

};*/
class computacionyTelefonos: public articulosElectronicos
{
    public:

        computacionyTelefonos();
        computacionyTelefonos(int,string,string,string,string, string,int,int,int,float);

        void setProcesador(string);
        string getProcesador();

        void setConectividad(string);
        string getConectividad();

        void setSO(string);
        string getSO();

        void setCamara(int);
        int getCamara();

        void setMemoriaRam(int);
        int getMemoriaRam();

        void setPulgadas(float);
        float getPulgadas();

        void setMemoriaInterna(int);
        int getMemoriaInterna();

    private:

        string procesador;
        string SO;
        string conectividad;
        int camara;
        int memoriaRam;
        int memoriaInterna;
        float pulgadas;

};
class telefonosyTablets: public computacionyTelefonos
{
    public:

        telefonosyTablets();
        telefonosyTablets(int,string,string,
          string,string,string,int,int,int,float,string,string,float,string);

        void setCompania(string);
        string getCompania();
        void setWifi(float);
        float getWifi();
        void setlectorHuella(string);
        string getlectorHuella();
        void setCategoria(string);
        string getCategoria();

    private:

        string compania;
        string tipo; //smartphone, tablet, telefono
        float wifi; //2.4 5 ghz
        string lectorHuella; //S N
};

class CalefaccionyVentilacion: public articulosElectronicos
 // Estufas, Calientacamas, purificadores
{
    public:

        CalefaccionyVentilacion();
        CalefaccionyVentilacion(int,string,string,string,string,int,int,int,int,int);

        void setMaterial(string);
        string getMaterial();

        void setSuperficie(string);
        string getSuperficie();

        void setPotencia(int);
        int getPotencia();

        void setTipoTemperatura(int);
        int getTipoTemperatura();

        void setLargo(int);
        int getLargo();

        void setAncho(int);
        int getAncho();

        void setProfundidad(int);
        int getProfundidad();

    private:

        string material;
        string superficie; //cama, piso, muralla, pedestal,sobremesa
        int potencia;
        int tipoTemperatura; //frio caliente
        int largo;
        int ancho;
        int profundidad;
};

class lineaBlanca: public articulosElectronicos
//(Lavadoras, Centrifugas,Lavavajillas,secadoras) (Refrigeradores, Freezers, Frio directo, Frigobar, Cavas de vino)
{
    public:

        lineaBlanca();
        lineaBlanca(int,string,string,string, string, int, int, int,int);

        void setColor(string);
        string getColor();
        void setClasificacionEnergetica(string);
        string getClasificacionEnergetica();
        void setPeso(int);
        int getPeso();
        void setAncho(int);
        int getAncho();
        void setAlto(int);
        int getAlto();
        void setProfundidad(int);
        int getProfundidad();

    private:

        string color;
        string clasificacionEnergetica;
        int peso;
        int ancho;
        int alto;
        int profundidad;
};


class multimedia: public articulosElectronicos{
public:
  multimedia();
  multimedia(int,string,string,string);
  void setSmart(string);
  string getSmart();
private:
  string smart;
};

class audio: public multimedia{
public:
  audio();
  audio(int,string,string,string,int,int);
  void setAudio(int);
  int getAudio();
  void setPotencia(int);
  int getPotencia();
private:
  int calidad;
  int potencia;
};

class TV: public multimedia{
public:
  TV();
  TV(int,string,string,string,float);
  void setPulgadas(float);
  float getPulgadas();
private:
  float pulgadas;
};

/*class camaraYotros: public multimedia{
public:
  camaraYotros();
  camaraYotros(string);
  void setConexion(string);
  string getConexion();
private:
  string tipodeConexion;
};
*/

articulosElectronicos::articulosElectronicos(){
  codProducto=00000;
  nombreProducto="No hay nombre definido";
  marca="Marca no definido";

}
articulosElectronicos::articulosElectronicos(int c, string n,string m):codProducto(c),nombreProducto(n),marca(m){}
void articulosElectronicos::setCodigo(int c){
  codProducto=c;
}
int articulosElectronicos::getCodigo(){
  return codProducto;
}
void articulosElectronicos::setNombre(string n){
  nombreProducto=n;
}
string articulosElectronicos::getNombre(){
  return nombreProducto;
}
void articulosElectronicos::setMarca(string m){
  marca=m;
}
string articulosElectronicos::getMarca(){
  return marca;
}



computacionyTelefonos::computacionyTelefonos(){
  procesador="Ningun procesador elegido";
  SO="Ningun Sistema operativo definido";
  conectividad="Sin parametros definidos";
  camara=0;
  memoriaRam=0;
  memoriaInterna=0;
  pulgadas=0.0;

}
computacionyTelefonos::computacionyTelefonos(int c,string n,string m,string pro,string so,string con,int mpx,int r,int mi,float p):articulosElectronicos(c,n,m){}

void computacionyTelefonos::setProcesador(string pro){
  procesador=pro;
}
string computacionyTelefonos::getProcesador(){
  return procesador;
}
void computacionyTelefonos::setSO(string so){
  SO=so;
}
string computacionyTelefonos::getSO(){
  return SO;
}

void computacionyTelefonos::setConectividad(string con){
  conectividad=con;
}
string computacionyTelefonos::getConectividad(){
  return conectividad;
}

void computacionyTelefonos::setMemoriaRam(int r){
  memoriaRam=r;
}
int computacionyTelefonos::getMemoriaRam(){
  return memoriaRam;
}

void computacionyTelefonos::setCamara(int mpx){
    camara=mpx;
}
int computacionyTelefonos::getCamara(){
  return camara;
}

void computacionyTelefonos::setMemoriaInterna(int mi){
  memoriaInterna=mi;
}
int computacionyTelefonos::getMemoriaInterna(){
  return memoriaInterna;
}
void computacionyTelefonos::setPulgadas(float p){
  pulgadas=p;
}
float computacionyTelefonos::getPulgadas(){
  return pulgadas;
}


telefonosyTablets::telefonosyTablets(){
  compania="No homologado";
  tipo="No a elegido el tipo del articulo";
  wifi=0.0;
  lectorHuella="No tiene";
}//ṕroblema con asignacion de herencia
telefonosyTablets::telefonosyTablets(int c, string n,string m,string pro,string con,string so, int mpx,int r,int mi,float p,string com,string type,float w,string lh):computacionyTelefonos(c,n,m,pro,so,con,mpx,r,mi,p)
{}

void telefonosyTablets::setCompania(string com){
  compania=com;
}
string telefonosyTablets::getCompania(){
  return compania;
}
void telefonosyTablets::setlectorHuella(string lh){
  lectorHuella=lh;
}
string telefonosyTablets::getlectorHuella(){
  return lectorHuella;
}

void telefonosyTablets::setWifi(float w){
  wifi=w;
}
float telefonosyTablets::getWifi(){
  return wifi;
}

void telefonosyTablets::setCategoria(string type){
  tipo=type;
}
string telefonosyTablets::getCategoria(){
  return tipo;
}



CalefaccionyVentilacion::CalefaccionyVentilacion(){
  material="Material no definido";
  superficie="area no definida, debe definir para que area\n";
  potencia=0;
  tipoTemperatura=0;
  largo=0;
  ancho=0;
  profundidad=0;
}

CalefaccionyVentilacion::CalefaccionyVentilacion(int c,string n,string m,string mat,
  string categ,int watt,int temp,int large,int ancho_,int m2):articulosElectronicos(c,n,m)
{}

void CalefaccionyVentilacion::setMaterial(string mat){
  material=mat;
}
string CalefaccionyVentilacion::getMaterial(){
  return material;
}
void CalefaccionyVentilacion::setSuperficie(string categ){
  superficie=categ;
}
string CalefaccionyVentilacion::getSuperficie(){
  return superficie;
}
void CalefaccionyVentilacion::setPotencia(int watt){
  potencia=watt;
}
int CalefaccionyVentilacion::getPotencia(){
  return potencia;
}
void CalefaccionyVentilacion::setTipoTemperatura(int temp){
  tipoTemperatura=temp;
}
int CalefaccionyVentilacion::getTipoTemperatura(){
  return tipoTemperatura;
}
void CalefaccionyVentilacion::setLargo(int large){
  largo=large;
}
int CalefaccionyVentilacion::getLargo(){
  return largo;
}

void CalefaccionyVentilacion::setAncho(int ancho_){
  ancho=ancho_;
}
int CalefaccionyVentilacion::getAncho(){
  return ancho;
}
void CalefaccionyVentilacion::setProfundidad(int m2){
  profundidad=m2;
}

lineaBlanca::lineaBlanca(){
  color="Color no definido";
  clasificacionEnergetica="q++";
  peso=0;
  ancho=0;
  alto=0;
  profundidad=0;

}
lineaBlanca::lineaBlanca(int c, string n,
  string m,string color_,string clasif,
   int kg, int h, int a, int profundidad_):articulosElectronicos(c,n,m){}

void lineaBlanca::setColor(string color_){
  color=color_;
}
string lineaBlanca::getColor(){
  return color;
}
void lineaBlanca::setClasificacionEnergetica(string clasif){
  clasif=clasificacionEnergetica;
}
string lineaBlanca::getClasificacionEnergetica(){
  return clasificacionEnergetica;
}
void lineaBlanca::setPeso(int kg){
  peso=kg;
}
int lineaBlanca::getPeso(){
  return peso;
}
void lineaBlanca::setAncho(int a){
  ancho=a;
}
int lineaBlanca::getAncho(){
  return ancho;
}
void lineaBlanca::setAlto(int h){
  alto=h;
}
int lineaBlanca:: getAlto(){
  return alto;
}
void lineaBlanca::setProfundidad(int profundidad_){
  profundidad=profundidad_;
}
int lineaBlanca::getProfundidad(){
  return profundidad;
}

multimedia::multimedia(){
  smart="No ha definido si es smart";
}
multimedia::multimedia(int c, string n,string m,string mm):articulosElectronicos(c,n,m){
}

void multimedia::setSmart(string mm){
  smart=mm;
}
string multimedia::getSmart(){
  return smart;
}

audio::audio(){
  calidad=0;
}
audio::audio(int c, string n,string m,string mm,int calidad_,int potencia_):multimedia(c,n,m,mm){
}
void audio::setAudio(int calidad_){
  calidad=calidad_;
}
int audio::getAudio(){
  return calidad;
}
void audio::setPotencia(int potencia_){
  potencia:potencia_;
}
int audio::getPotencia(){
  return potencia;
}

TV::TV(){
  pulgadas=0.0;
}
TV::TV(int c, string n,string m,string mm,float pulg):multimedia(c,n,m,mm){
}
void TV::setPulgadas(float pulg){
  pulgadas=pulg;
}
float TV::getPulgadas()
{
  return pulgadas;
}





int main(){
 stack <articulosElectronicos>p;
 articulosElectronicos p1(2302,"telefono","nokia");


p.push(p1);
cout << p.top().getCodigo() << '\n';

return 0;

}
