#include<iostream>
#include<stack>
using namespace std;


class Stack : public stack<int>
{
    public:
        int Pop(){
            int a=top();
            pop();
            return a;
        }
        void Push(int valor){
        push(valor);
        }
        bool Empty(){
        return empty();}

};

/***void mostrar(Stack s)
{
  Stack aux;
  int e;
  cout<<"Mostrar Stack sin perder los elementos: \n";
  while(!s.Empty()){
    e= s.pop();
    cout<<"valor: "<< e <<endl;
    //guardo el valor en otro stack
    aux.Push(e);
  }
  //Como quedo en orden inverso lo reordenamos
  while(!aux.Empty()){
    e=aux.pop();
    s.Push(e);
  }
}****/

void mostrar(Stack s)
{
  Stack aux;
  while(!s.Empty()){
    int e = s.Pop();
    cout<<"Valor : "<<e<<endl;
    aux.Push(e);
  }
  while(!aux.Empty()) s.Push(aux.Pop());
}

int contarlelemento(Stack s){
  int cantidad=0;
  Stack aux;
  while(!s.Empty()){
    aux.Push(s.Pop());
    cantidad ++;
  }
  while(!aux.Empty()) s.Push(aux.Pop());
  return cantidad;
}

int sumarStack(Stack s){
  int sum=0,e;
  Stack aux;
  while(!s.Empty()){
    e=s.Pop();
    aux.Push(e);
    sum=sum+e;
  }
  while(!aux.Empty())s.Push(aux.Pop());
  return sum;
}

int main(){
    Stack a;
    a.Push(1);
    mostrar(a);
    int cant=contarlelemento(a);
    cout<<"\nElementos encontrados: "<<cant<<endl;
    int s=sumarStack(a);
    cout<<"La suma de los elementos es: "<<s<<endl;


return 0;

}
