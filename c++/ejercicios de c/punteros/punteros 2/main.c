#include <stdio.h>
#include <stdlib.h>
//Implementar la funci�n rotar(a,b,c) que deje el valor de b en a,el valor de c en b y el valor de a en c.
void rotar(int *x, int *y,int *z)
{
    int aux;
    aux=*y;
    *y=*z;
    *z=*x;
    *x=aux;
}
int main()
{
    int a=1,b=22,c=1000;
    rotar(&a,&b,&c);
    printf("%d, %d, %d",a,b,c);
    return 0;
}
