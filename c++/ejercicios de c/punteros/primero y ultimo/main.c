#include <stdio.h>
#include <stdlib.h>
int invertir(int n)
{
    int m, inv=0;
    while(n!=0)
    {
        m=n%10;
        n=n/10;
        inv=inv*10+m;
    }
    return inv;
}

void prim_ult(int n, int *p,int *u)
{
    *u=n%10;
    n=invertir(n);
    *p=n/10;

}

int main()
{
    int a,b,c;
    printf("ingrese a: ");
    scanf("%d",&a);
    prim_ult(a,&b,&c);
    printf("el numero queda como: %d,%d,%d",a,b,c);
    return 0;
}
