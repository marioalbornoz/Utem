#include <stdio.h>
#include <stdlib.h>
void division(int n, int d, int *q,int *r)
{
    *q=0;
    while(n>=d)
    {
        n=n-d;
        (*q)++;
    }
    *r=n;
}

int main()
{
    int a=12,b=4,c,r;
    division(a,b,&c,&r);
    printf("La division es:%d\n",c);
    return 0;
}
