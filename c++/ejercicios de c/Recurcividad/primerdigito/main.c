#include <stdio.h>
#include <stdlib.h>

int PrimerDigito(int x)
{
      if(x < 10)
          return x;
      else
          return PrimerDigito(x / 10);

}

int main()
{
    int n;
    scanf("%d ",&n);
    printf("%d",PrimerDigito(n));
    return 0;
}
