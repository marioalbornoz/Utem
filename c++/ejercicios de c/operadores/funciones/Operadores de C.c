#include <stdio.h>
#include <stdlib.h>
//funciones.
int contardig(int n)
{
    int cont=0;
    while(n!=0)
    {
        n=n/10;
        cont++;
    }
    return cont;

}

int sumardig(int n)   //sumara los digitos de un numero entero.
{
    int m, sum=0;
    while(n!=0)
    {
        m=n%10;
        sum=sum+m;
        n=n/10;
    }
    return sum;
}

int primerdig(int n) //te dira el primer digito del entero.
{
    while(n!=0)
    {
        n=n/10;
    }
    return n;
}

int invertir(int n) //invierte los dig de un numero
{
    int m, inv=0;
    while(n!=0)
    {
        m=n%10;
        n=n/10;
        inv=inv*10+m;
    }
    return inv;
}

int contardiv(int n)
{
    int c, cent,i;
    c=contardig(n);
    if(c%2!=0)
    {
        for(i=1;i<=c/2;i++)
            n=n/10;
        cent=n%10;
    }
    else
    {
        for(i=1;i<=c/2-1;i++)
            n=n/10;
        cent= n%100;
    }
    return cent;
}

int potencia(int b, int e)
{
    int pot=1,i;
    for(i=1;i<=e;i++)
        pot=pot*b;
    return pot;
}

int intercambiar_extremos(int n)
{
    int u,c,m,p,pot,res;
    u=n%10;
    n=n/10;
    m=invertir(n);
    p=m%10;
    m=invertir(m);
    pot=potencia(10,c-1);
    res=u*pot+m*10+p;
    return res;
}
int sumadiv(int n)
{
    int sum=0,i;
    for(i=1;i<=n;i++)
    {
        if(n%i==0)
            sum=sum+i;
    }
    return sum;
}

int numperfecto(int n)
{
    int s;
    s=sumadiv(n);
    if(s==2*n)
        return 1;
    else
        return 0;
}

int primo(int n)
{
    int c;
    c=contardiv(n);
    if(c==2)
        return 0;
    else
        return 1;
}

int main()
{
    int n,numero,m;
    printf("Programa de Super Marusho\nLea bien Por favor..\n");
    printf("1- Contar digitos de un numero entero.\n2-Sumar los digitos de un numero entero.\n3-Indica el primero digito del numero entero.\n4-Digitos centrales de un numero.\n5-Invertir digitos de un numero entero.\n6-Intercambiar extremos de un numero entero.\n7-Sumar los divisores de un numero.\n8-Indicar si el numero es primo.\n\n");
    printf("A continuacion eliga una de las opciones a operar:....\n");
    scanf("%d",&n);
    switch(n)
    {
    case 1:
        printf("ingrese un numero entero: \n");
        scanf("%d",&numero);
        m=contardig(numero);
        printf("el numero tiene las siguientes cifras de digitos: %d",m);
        break;
    case 2:
        printf("ingrese el numero entero: \n");
        scanf("%d",&numero);
        printf("La suma de los numeros es: %d",m=sumardig(numero));
        break;
    case 3:
        printf("ingrese el numero entero: \n");
        scanf("%d",&numero);
        printf("el primer digito del numero indicado es: %d",m=primerdig(numero));
        break;
    case 4:
        printf("ingrese el numero entero: \n");
        scanf("%d", &numero);
        printf("Los digitos centrales del numero son: %d",m=contardiv(numero));
        break;
    case 5:
        printf("ingrese el numero entero: \n");
        scanf("%d",&numero);
        printf("el numero invertido es: %d",m=invertir(numero));
        break;
    case 6:
        printf("ingrese el numero entero: \n");
        scanf("%d",&numero);
        printf("Con los extremos intercambiados el numero queda como: %d",m=intercambiar_extremos(numero));
        break;
    case 7:
        printf("ingrese el numero entero: \n");
        scanf("%d",&numero);
        printf("la suma de los divisores es: %d",m=sumadiv(numero));
        break;
    case 8:
        printf("ingrese el numero entero: \n");
        scanf("%d",&numero);
        printf("Si es 1 el numero es primo, cero para lo contrario: %d",m=primo(numero));
        break;
    default:
        printf("La opcion ingresada no es valida, por favor reinicie el programa.");


    }

    return 0;
}
