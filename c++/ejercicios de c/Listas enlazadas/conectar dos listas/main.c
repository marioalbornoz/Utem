#include <stdio.h>
#include <stdlib.h>
typedef struct nodo
{
    int dato;
    struct nodo *link;

}nodo;

typedef nodo *lista;

void conectar(lista *L1, lista *L2)
{
    lista p,q;
    q=L1;
    if((*L1)->link==NULL)
        (*L1)=(*L2);
    else
    {
        if((*L1)->link==NULL)
                q=q->link;
        q->link=L2;

    }
}
void mostrar(lista l)
{
    lista p;
    p=l;
    while(p!=NULL)
    {
        printf("%d ",p->dato);
        p=p->link;
    }
}
int main()
{
    lista p,q;
     p=malloc(sizeof(nodo));
    p->dato=9;
    p->link=NULL;
    q=malloc(sizeof(nodo));
    q->dato=2;
    q->link=NULL;
    conectar(&p,&q);
    mostrar(p);
    mostrar(q);
    return 0;
}
