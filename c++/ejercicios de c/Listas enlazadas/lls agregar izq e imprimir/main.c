#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
    int dato;
    struct nodo *link;
}nodo;

typedef nodo *lista;

void agregar(lista *L,int e)
{
    lista p;
    p=malloc(sizeof(nodo));
    p->dato=e;
    p->link=*L;
    *L=p;

}
void mostrar( lista L)
{
    lista p;
    p=L;
    while(p!=NULL)
    {
        printf("%d\n",p->dato);
        p=p->link;
    }
}


int main()
{
    int m;
    lista p;
    p=NULL;
    agregar(&p,1);
    agregar(&p,2);
    agregar(&p,3);
    mostrar(p);

    return 0;
}
