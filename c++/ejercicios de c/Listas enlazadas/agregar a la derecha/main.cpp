#include <iostream>
using namespaces std;

typedef struct nodo
{
    int dato;
    struct nodo *link;
}nodo;
 typedef nodo *lista;

void agregarderecha(lista *l,int e)
{
    lista q,p;
    p=malloc(sizeof(nodo));
    p->dato=e;
    p->link=NULL;
    if(*l==NULL)
        *l=p;
    else
    {
        if((*l)->link==NULL)
            (*l)->link=p;
        else
        {
            q=l;
            while(q->link=NULL)
            {
                q=q->link;
                q->link=p;
            }
        }
    }
}
void mostrar(lista l)
{
    lista p;
    p=l;
    while(p!=NULL)
    {
        printf("%d->",p->dato);
        p=p->link;
    }
}
int main()
{
    lista p;
    p=NULL;
    agregarderecha(&p,3);
    agregarderecha(&p,7);
    agregarderecha(&p,2);
    agregarderecha(&p,5);
    mostrar(p);

    return 0;
}
