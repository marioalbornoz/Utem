#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
    int dato;
    struct nodo *link;
}nodo;
typedef nodo *lista;

int primulti(lista l)
{
    int c=0;
    lista p;
    p=l;
    while(p!=NULL)
    {
        c++;
        p=p->link;
    }
    if(c==0)
        return -1;
    if(c%2==0)
        return l->dato;
    else
    {
        p=l;
        while(p->link!=NULL)
        {
            p=p->link;
        }
        return p->dato;
    }
}

int main()
{
    int m;
    lista p,l,q;
    p=malloc(sizeof(nodo));
    l=malloc(sizeof(nodo));
    q=malloc(sizeof(nodo));
    p->dato=12;
    p->link=l;
    l->dato=2;
    l->link=NULL;
    q->dato=3;
    q->link=NULL;
    m=primulti(p);
    printf("%d",m);

    return 0;
}
