#include <stdio.h>
#include <stdlib.h>

typedef struct nodo
{
    int dato;
    struct nodo *link;
}nodo;

typedef nodo *lista;

void agregar(lista *p, int e)
{
    lista q;
    q=(lista)malloc(sizeof(nodo));
    q->dato=e;
    q=q->link=*p;
    *p=q;
}

int contar(lista l)
{
   int cont=0;
   lista p;
   p=l;
   while(p!=NULL)
   {
       cont++;
       p=p->link;
   }
   return cont;
}

float promedioimpar(lista l)
{
    float sum=0,prom;
    int c=0;
    lista p;
    p=l;
    while(p!=NULL)
    {
        if(p->dato%2!=0)
        {
            c++;
            sum=sum+p->dato;
        }
        p=p->link;
    }
    if(c==0)
        prom=0.0;
    else
        prom=sum/c;
    return prom;
}

int main()
{   int m,n;
    lista l=(lista)malloc(sizeof(nodo));
    l->dato=3;
    l->link=NULL;
    agregar(&l,2);
    agregar(&l,3);
    agregar(&l,7);
    m=contar(l);
    printf("la cantidad de datos es: %d\n",m);
    n=promedioimpar(l);
    printf("el promedio de los datos impar es: %d\n",n);
    return 0;
}
