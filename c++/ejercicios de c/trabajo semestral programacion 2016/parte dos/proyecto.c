#include <stdio.h>
#include <stdlib.h>
void ingresar_texto(char txt)
{
    char nombre[1000];
    printf("\n\n-Escribe el texto a decodificar(caracteres,letras o numeros): ");
    FILE *p;
    scanf("%d", &p);
    p=fopen("texto_a_encriptar.txt","w");
    if(p==NULL)
        printf("Error al generar el texto");
    else
    {

        fgets(nombre, 1000, stdin);
        fprintf(p,"%s\n",nombre);
        printf("El texto ingresado ha sido guardado.\n");
    }
    fclose(p);

}
void guardar_texto_orig(char txt)
    {
        FILE *p =fopen("texto_a_encriptar.txt","rb");
        if(p==NULL){
           perror("\nError al leer el texto\n");
           return 1;
        }
        fseek(p,0,SEEK_END);
        int num_elem=ftell(p);
        rewind(p);

        char *cadena=(char *)calloc(sizeof(char),num_elem);
        if(cadena==NULL){
            perror("Error en la reserva de memoria");
            return 2;
        }
        int num_elem_leidos=fread(cadena,sizeof(char),num_elem, p);
        if(num_elem_leidos!=num_elem){
            perror("Error al leer el archivo");
            return 3;
        }
        FILE *q =fopen("texto_a_encriptado.txt","w");
        if(q==NULL){
            perror("error al crear el texto encriptado");
            return 4;
        }
        fputs(cadena, q);
        fclose(q);
        printf("\n%s\n", cadena);
        free(cadena);
        fclose(p);
        printf("El archivo se ha leido correctamente");
    }


void matriz(int A[5][5])
{
    int i,j,k=97;
    char letra;
    char cadena[1000];
    while(k<123){

    printf("\n\n\n-Ingrese letra con la que quiere codificar: ");
    scanf("%c",&letra);
    printf("\nla matriz interna que codificara sera: \n");
    int a =(int)letra;
    for(i=0;i<5;i++)
    {
        printf("\n");
        for (j=0;j<5;j++)
        {
            if(k!=a)
            {
                char n=(char)k;
                printf("%c ",n);
                k++;
            }
            else
            {
                j--;
                k++;
            }
        }
    }



    }

}

int main()
{
    int mat[5][5];
    char cadena[1000];
    matriz(mat);
    ingresar_texto(cadena);
    guardar_texto_orig(cadena);

    return 0;
}
