#include <stdio.h>
#include <stdlib.h>
typedef struct alumno
{
    int matricula;
    int edad;
    float nota[4];
    char concepto;

}alumno;

void llenar(alumno *a)
{
    int i;
    printf("ingrese numero de matricula: ");
    scanf("%d",&((*a).matricula));
    printf("\ningrese la edad del alumno: ");
    scanf("%d",&((*a).edad));
    for(i=0;i<4;i++)
    {
        printf("ingrese notas del alumno: ");
        scanf("%f",&(*a).nota[i]);
    }
}

void calcular_concepto(alumno *a)
{
    int i;
    float prom,sum=0;
    for(i=0;i<4;i++)
    {
        sum=sum+(*a).nota[i];
    }
    prom=sum/4.0;
    if(prom>4.0)
        (*a).concepto='a';
    else
        (*a).concepto='r';
}

alumno curso[30];
void llenarcurso(alumno a[],int n)
{
    int i;
    for(i=0;i<n;i++)
    {
        llenar(&a[i]);
        calcular_concepto(&a[i]);
    }
}

int aprobados(alumno a[], int n)
{
    int i, cont=0;
    for(i=0;i<n;i++)
    {
        if(a[i].concepto=='a')
            cont++;
    }
    return cont;
}
int main()
{

    return 0;
}
