#include <stdio.h>
#include <stdlib.h>

//funciones.
int contardig(int n)
{
    int cont=0;
    while(n!=0)
    {
        n=n/10;
        cont++;
    }
    return cont;

}

int sumardig(int n)   //sumara los digitos de un numero entero.
{
    int m, sum=0;
    while(n!=0)
    {
        m=n%10;
        sum=sum+m;
        n=n/10;
    }
    return sum;
}

int primerdig(int n) //te dira el primer digito del entero.
{
    while(n!=0)
    {
        n=n/10;
    }
    return n;
}

int invertir(int n) //invierte los dig de un numero
{
    int m, inv=0;
    while(n!=0)
    {
        m=n%10;
        n=n/10;
        inv=inv*10+m;
    }
    return inv;
}

int contardiv(int n)
{
    int c, cent,i;
    c=contardig(n);
    if(c%2!=0)
    {
        for(i=1;i<=c/2;i++)
            n=n/10;
        cent=n%10;
    }
    else
    {
        for(i=1;i<=c/2-1;i++)
            n=n/10;
        cent= n%100;
    }
    return cent;
}

int potencia(int b, int e)
{
    int pot=1,i;
    for(i=1;i<=e;i++)
        pot=pot*b;
    return pot;
}

int intercambiar_extremos(int n)
{
    int u,c,m,p,pot,res;
    u=n%10;
    n=n/10;
    m=invertir(n);
    p=m%10;
    m=invertir(m);
    pot=potencia(10,c-1);
    res=u*pot+m*10+p;
    return res;
}
int sumadiv(int n)
{
    int sum=0,i;
    for(i=1;i<=n;i++)
    {
        if(n%i==0)
            sum=sum+i;
    }
    return sum;
}

int numperfecto(int n)
{
    int s;
    s=sumadiv(n);
    if(s==2*n)
        return 1;
    else
        return 0;
}

int primo(int n)
{
    int c;
    c=contardiv(n);
    if(c==2)
        return 1;
    else
        return 0;
}

