#include <stdio.h>
#include <stdlib.h>

typedef struct nodo{
    char nombre[30];
    int edad;
    float prom;
    struct nodo *link;
    }nodo;

    typedef nodo *lista;

void agregarinicio(lista *l)
{
    lista p;
    p=(lista)malloc(sizeof(nodo));
    fflush(stdin);
    printf("ingrese su nombre: ");
    scanf("%s", &p->nombre);
    fflush(stdin);
    printf("Ingrese edad: ");
    scanf("%d",&p->edad);
    printf("ingrese promedio: ");
    scanf("%f",&p->prom);
    p->link=*l;
    *l=p;

}
void imprimir(lista l)
{
    lista p;
    p=l;
    while(p!=NULL)
    {
        printf("su nombre es: %s\n",p->nombre);
        printf("su edad es: %d\n",p->edad);
        printf("su promedio es: %f",p->prom);
        p=p->link;
        printf("\n");
    }

}
int aprobados(lista l)
{
    lista p=l;
    int cant=0;
    while(p!=NULL)
    {
        if(p->prom>3.94)
            cant++;
        p=p->link;
    }
    return cant;
}

int main()
{
    printf("listas lineales\n");
    lista l=NULL;
    agregarinicio(&l);
    agregarinicio(&l);
    imprimir(l);
    aprobados(l);
    printf("la cantidad de aprobados son: %d",aprobados(l));
    return 0;
}
