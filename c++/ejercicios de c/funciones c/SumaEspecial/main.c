#include <stdio.h>
#include <stdlib.h>
typedef struct algo{
    int x;
    float y;
}algo;
typedef struct cosa{
    int z;
    algo w;
}cosa;
typedef struct cosita{
    int r;
    cosa t;
}cosita;
typedef struct coson{
    int a;
    cosita b;
}coson;
void llenarAlgo(algo *A){
    printf("Ingrese x : ");
    scanf("%d",&((*A).x));
    printf("Ingrese y : ");
    scanf("%f",&((*A).y));
}
void mostrarAlgo(algo A){
    printf("algo es: %d, %f\n", A.x ,A.y);
}
void llenarCosa(cosa *C){
    printf("Ingrese z : ");
    scanf("%d",&((*C).z));
    printf("llenando algo ... \n");
    llenarAlgo(&((*C).w));
}
void mostrarCosa(cosa C){
    printf("Desde Cosa z : %d\n",C.z);
    printf("Mostrar algo ... \n");
    mostrarAlgo(C.w);
}
void llenarCosita(cosita *Q){
    printf("Ingrese r : ");
    scanf("%d",((*Q).r));
    llenarCosa(&((*Q).t));
}
void mostrarCosita(cosita Q){
    printf("Desde cosita r : %d\n",Q.r);
    mostrarCosa(Q.t);
}
void llenarCoson(coson *U){
    printf("Desde coson   a : ");
    scanf("%d",&((*U).a));
    llenarCosita(&((*U).b));
}
void mostrarCoson(coson U){
    printf("Desde coson   a : %d\n",U.a);
    printf("Desde coson   b\n");
    mostrarCosita(U.b);
}
void sumaAlgo(algo M, algo N, algo *L){
    (*L).x = M.x + N.x;
    (*L).y = M.y + N.y;
}
void sumaCosa(cosa H, cosa K, cosa *D){
    (*D).z = H.z + K.z;
    sumaAlgo(H.w,K.w,&((*D).w));
}
void sumaCosita(cosita W, cosita E, cosita *S){
    (*S).r = W.r + E.r;
    sumaCosa(W.t, E.t, &((*S).t));
}
void sumaEspecial(coson U, coson T, coson *R){
    (*R).a = U.a + T.a;
    sumaCosita(U.b,T.b,&((*R).b));
}
int main()
{
/*
    algo A;
    printf("Programa principal\n");
    llenarAlgo(&A);
    mostrarAlgo(A);

    cosa C;
    llenarCosa(&C);
    mostrarCosa(C);

    cosita Q;
    llenarCosita(&Q);
    mostrarCosita(Q);
*/
    coson U, T, R;
    llenarCoson(&U);
    llenarCoson(&T);
    sumaEspecial(U,T,&R);
    mostrarCoson(R);
    return 0;
}
