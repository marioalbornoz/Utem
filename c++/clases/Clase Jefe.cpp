#include<iostream>
#include<string>
using namespace std;

class Jefe{
public:
  Jefe(string,int);
  void setArea(string);
  string getArea();
  void setSueldo(int);
  int getSueldo();
private:
  string area;
  int sueldo;

};

Jefe::Jefe(string a,int s) : area(a),sueldo(s){}

void Jefe::setArea(string a)
{
  area=a;
}
void Jefe::setSueldo(int s){
  sueldo=s;
}
string Jefe::getArea(){
  return area;
}
int Jefe::getSueldo(){
  return sueldo;
}

Jefe m("informarica",10000000);

int main(){
cout<<m.getArea()<<endl;
cout<<m.getSueldo()<<endl;
return 0;

}
