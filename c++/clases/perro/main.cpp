#include<iostream>
#include<vector.h>
#include"perro.h"
#include<string>

using namespace std;

Perro::Perro(){

  raza="No definido";
  nombre="No definido";
  dueno = "No definido";
  sexo="No definido";
  edad=0;
  color="No definido";

}
Perro::~Perro(){}
Perro::Perro(string r, string n, string d, string s, int e, string c): raza(r), nombre(n), dueno(d), sexo(s), edad(e), color(c){}
void Perro::setRaza(string r)
{
  raza=r;
}
string Perro::getRaza()
{
  return raza;
}

void Perro::setNombre(string n){
  nombre=n;
}
string Perro::getNombre(){
  return nombre;
}
void Perro::setDueno(string d)
{
  dueno = d;
}
string Perro::getDueno()
{
  return dueno;
}
void Perro::setSexo(string s){
  sexo=s;
}
string Perro::getSexo(){
  return sexo;
}

void Perro::setEdad(int e){
  edad=e;
}
int Perro::getEdad(){
  return edad;
}

void Perro::setColor(string c)
{
  color=c;
}
string Perro::getColor(){
  return color;
}

void contenedorPerros(Perro p,  vector <Perro> &contenedor)
{
    string names,razade,colour,sex;
    cout<<"\nIngrese nombre de perro :";
    cin>>names;
    p.setNombre(names);

    cout<<"Raza del perro: ";
    cin>>razade;
    p.setRaza(razade);

    cout<<"Sexo: ";
    cin>>sex;
    p.setSexo(sex);

    cout<<"Color: ";
    cin>>colour;
    p.setColor(colour);

    cout<<"\nDatos Guardados!\n"<<endl;
    contenedor.push_back(p);

}

void mostrarContenedor(vector<Perro> contenedor)
{
    for(int i=0;i<contenedor.size();i++)
    {
      cout<<i+1<<" - El "<<contenedor[i].getRaza()<<" se llama "<<contenedor[i].getNombre()<<endl;
    }


}

void eliminarPerro(vector <Perro> &contenedor)
{
  cout<<"\t\t\tQuitar de la lista: \n";
  cout<<"Nombre del perro: ";
  string eliminar;
  cin>>eliminar;
  for(int i=0;i<contenedor.size();i++)
  {
    if(contenedor[i].getNombre()==eliminar){
      cout<<"Perro encontrado: "<<contenedor[i].getNombre();
      int cont=i;
      contenedor.erase(contenedor.begin()+cont);
    }
  }
}
void editarPerro(vector <Perro> &contenedor)
{
  string editar,nombre1,raza, sexo, color ;
  cout<<"Canino a editar: ";
  cin>>editar;
  cout << "Editar" << endl;
  for(int i=0; i<contenedor.size();i++)
  {
    fflush(stdin);
    if(contenedor[i].getNombre()==editar){
      cout<< "Nombre:"<<contenedor[i].getNombre()<<"\nRaza:"<<contenedor[i].getRaza()<<"\nSexo:"<<contenedor[i].getSexo()<<"\nColor:"<<contenedor[i].getColor()<<endl;

      cout<<"\nModificar detalles de: "<<endl;
      cout<<"Nombre : ";
      cin>>nombre1;
      contenedor[i].setNombre(nombre1);
      cout<<"Raza :  ";
      cin>>raza;
      contenedor[i].setRaza(raza);
      cout<<"Sexo : ";
      cin>>sexo;
      contenedor[i].setSexo(sexo);
      cout<<"Color  : ";
      cin>>color;
      contenedor[i].setColor(color);

      cout<<"\nDatos guardados!"<<endl;
      cout<< "Nombre:"<<contenedor[i].getNombre()<<"\nRaza:"<<contenedor[i].getRaza()<<"\nSexo:"<<contenedor[i].getSexo()<<"\nColor:"<<contenedor[i].getColor()<<endl;

    }
  }
}


int main()
{
  cout<<"\t\t\tBienvenido al registro nacional de perros\n";
  vector<Perro> canino;
  int opcion;
  do
  {
    Perro ingresar;
    cout<<"\t\tMenu principal"<<endl;
    cout<<"Ingrese una opcion"<<endl;
    cout<<"1 - Ingresar perro\n2 - Mostrar lista\n3 - Quitar un perro\n4 - Editar\n5 - Salir"<<endl;
    cout<<": ";
    cin>>opcion;

    if(opcion == 1) contenedorPerros(ingresar,canino);
    if(opcion == 2) mostrarContenedor(canino);
    if(opcion == 3) eliminarPerro(canino);
    if(opcion == 4) editarPerro(canino);
    if(opcion == 5) {
      cout<<"Gracias por usar mi app.";
      break;

    }

  }while(opcion!=5);


  return 0;
}
