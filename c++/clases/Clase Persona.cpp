#include<iostream>
#include<string>

using namespace std;

class Persona{
public:
   Persona();
   Persona(string,int);
   void setNombre(string);
   void setEdad(int);
   int getEdad();
   string getNombre();


private:
  string nombre;
  int edad;

};

Persona::Persona(string n,int e) : nombre(n),edad(e){}

void Persona::setNombre(string n){
  nombre=n;
}

void Persona::setEdad(int e){
  edad=e;
}
string Persona::getNombre(){
  return nombre;
}
int Persona::getEdad()
{
  return edad;
}
Persona m("Mario",22);

int main()
{
  cout<<"El nombre es: "<<m.getNombre()<<endl;
  cout<<"Edad: "<<m.getEdad();
  return 0;
}
