#ifndef __CONSTANTES__H
#define __CONSTANTES__H


//Parametros del juego
#define GAME_LEVEL_MAX 10
#define GAME_TIME_START 300 //segundos
#define GAME_TIME_DECREMENT 15 //segundos
#define GAME_GHOST_SPEED_START 0.05
#define GAME_GHOST_SPEED_MAX 0.15
#define GAME_GHOST_SPEED_MIN 0.01
#define GAME_GHOST_BODY_RADIO 0.5
#define GAME_PACMAN_LIFE_START 3
#define GAME_PACMAN_SPEED_START 0.07
#define GAME_PACMAN_SPEED_MAX 0.15
#define GAME_PACMAN_SPEED_MIN 0.01
#define GAME_PACMAN_BODY_RADIO 0.5
#define GAME_BOARD_HIGH 22
#define GAME_BOARD_WIDTH 27
#define GAME_CELL_PACMAN_START_X 13
#define GAME_CELL_PACMAN_START_Y 14
#define GAME_CELL_GHOST_START_X 12
#define GAME_CELL_GHOST_START_Y 9
#define GAME_RANDOM_PLAY 0.00  //probabilidad de moverse al azar


//Contenido que puede tener una celda
#define CONT_VACIO 1             //no hay nada en la celda
#define CONT_WALL 2              //pared 
#define CONT_AWARD 4             //hay un premio
#define CONT_PACMAN 8            //personaje pacman
#define CONT_BLINKY 16           //personaje fantasma rojo
#define CONT_PINKY 32            //personaje fantasma rosado
#define CONT_INKY 64             //personaje fantasma celeste
#define CONT_CLYDE 128           //personaje fantasma anaranjado


//Tipo de personajes
#define CHAR_PACMAN 1              //personaje pacman
#define CHAR_BLINKY 2              //personaje fantasma rojo
#define CHAR_PINKY 3               //personaje fantasma rosado
#define CHAR_INKY 4                //personaje fantasma celeste
#define CHAR_CLYDE 5               //personaje fantasma anaranjado

//Tipo de premios
#define AWARD_FRUTATIPICA 1        //+100*NIVEL de puntaje
#define AWARD_FRUTILLA 2           //+10 de puntaje
#define AWARD_SANDIA 3             //+20 de puntaje
#define AWARD_PIEDRA 4             //-10 de puntaje
#define AWARD_HAMBURGUESA 5        //-0.03 de velocidad a Pacman
#define AWARD_PASTILLA 6           //+0.05 de velocidad a Pacman, puede comer fantasmas
#define AWARD_ENERGIZANTE 7        //+0.02 de velocidad a Pacman
#define AWARD_CHICHARRA 8          //Speed=0 para los fantasmas


//Parametros de puntaje
#define SCORE_GHOST 300
#define SCORE_PEBBLE 1
#define SCORE_FRUTATIPICA 100 //100*nivel
#define SCORE_FRUTILLA 10  //+10
#define SCORE_SANDIA 20    //+20
#define SCORE_PIEDRA 10    //-10

//Parametros de velocidad
#define SPEED_HAMBURGUESA 0.03   //-0.03 de velocidad a Pacman
#define SPEED_PASTILLA 0.05      //+0.05 de velocidad a Pacman, puede comer fantasmas
#define SPEED_ENERGIZANTE 0.02   //+0.02 de velocidad a Pacman
#define SPEED_CHICHARRA 0        //Speed=0 para los fantasmas


//movimientos que se hacen en el tablero
#define MOVE_UP 1
#define MOVE_DOWN 2
#define MOVE_LEFT 3
#define MOVE_RIGHT 4

//Constantes de disenho
#define DISTANCIA  -9.8f
#define TAM_PERSONAJE  0.35f

#endif
