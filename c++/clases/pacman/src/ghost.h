#ifndef __GHOST__H
#define __GHOST__H

#include "constantes.h"
#include "character.h"
#include "util.h"

class Ghost : public Character{
      public:
      Ghost(int, char *);
      void setGoHome(bool);
      bool getGoHome();
      void setIntelligence(int);
      int getIntelligence();
      bool eated;
      
      protected:
      bool goHome;
      int intelligence;
};

#endif
