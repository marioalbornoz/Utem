#ifndef __COMPONENT__H
#define __COMPONENT__H

#include "log.h"
#include "component.h"
          
class Component{
      public:
      Component(char *);
      Component(char *, int);
      ~Component();
      int whoami(); //retorna que es el componente (who am I)
      void setName(char *);
      char *getName();
      virtual char *getImagePath();
      protected:
      int iam;  //identifica quien es (I am)
      char *name;
};

#endif
