#ifndef __CHICHARRA__
#define __CHICHARRA__

#include "chicharra.h"
#include "game.h"
extern Game game;

Chicharra::Chicharra() : Award(AWARD_CHICHARRA, "Chicharra"){
     strcpy(filePath,"images/chicharra.png");
}

Chicharra::~Chicharra(){
}

Character * Chicharra::doEffect(Character *p){
     game.blinky->setSpeed(SPEED_CHICHARRA);
     game.pinky->setSpeed(SPEED_CHICHARRA);
     game.inky->setSpeed(SPEED_CHICHARRA);
     game.clyde->setSpeed(SPEED_CHICHARRA);
     
     setEffectTime(5000);
     
     return p;
}

void Chicharra::setEffectTime(unsigned int tiempo){
     glutTimerFunc(tiempo, loseAwardEffect, AWARD_CHICHARRA);
}

char * Chicharra::getImagePath(){
     return filePath;
}

#endif
