#ifndef __CHARACTER__H
#define __CHARACTER__H

#include <string>
#include "util.h"
#include "component.h"

class Character : public Component{
      public:
      Character(int, char *);
      ~Character();
      void setPosition(double,double);
      void setPosition(Point *);
      void getPosition(Point *);
      void setSpeed(double);
      double getSpeed();
      void speedUp(double);
      void speedDown(double);
      protected:
      
      
      private:
      double x, y;  //posicion donde se encuentra
      double speed; //velocidad a la que se mueve el personaje
      
};

#endif
