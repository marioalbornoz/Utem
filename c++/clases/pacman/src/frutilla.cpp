#ifndef __FRUTILLA__
#define __FRUTILLA__

#include "frutilla.h"

Frutilla::Frutilla() : Award(AWARD_FRUTILLA, "Frutilla"){
     strcpy(filePath,"images/frutilla.png");
}

Frutilla::~Frutilla(){
}

Character * Frutilla::doEffect(Character *p){
     game.increaseScore(SCORE_FRUTILLA);
     return p;          
}

char * Frutilla::getImagePath(){
     return filePath;
}

#endif
