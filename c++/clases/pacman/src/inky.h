#ifndef __INKY__H
#define __INKY__H

#include "ghost.h"
#include "constantes.h"

class Inky : public Ghost{
      public:
      Inky(char *);
      int getImage();
      char *getImagePath(int);
      protected:
      char filePath[3][30];
};

#endif
