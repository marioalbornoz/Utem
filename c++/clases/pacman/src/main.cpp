#include <cstdlib>
#include <windows.h>
#include <cstdio>
#include <math.h>
#include <stdio.h>
#include <gl\gl.h>
#include <gl\glut.h>
#include <gl\glaux.h>
#include "game.h"
#include "cstring"

#include "putAward.h"
#include "display.h"
//#include "wmp3.h"

Game game;
Log debugLog("log/debug.log");
GLUquadricObj	*q;
GLuint		texture[3];
int score;

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };




AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File=NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File=fopen(Filename,"r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		//return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}

int LoadGLTextures()                                    // Load Bitmaps And Convert To Textures
{
    int Status=FALSE;									// Status Indicator
    AUX_RGBImageRec *TextureImage[3];					// Create Storage Space For The Textures
    memset(TextureImage,0,sizeof(void *)*3);			// Set The Pointer To NULL
    if ((TextureImage[0]=LoadBMP("images/EnvWall.bmp")) &&// Load The Floor Texture
        (TextureImage[1]=LoadBMP("images/metal01.bmp")) &&	// Load the Light Texture
        (TextureImage[2]=LoadBMP("images/EnvRoll.bmp")))	// Load the Wall Texture
	{
		Status=TRUE;									// Set The Status To TRUE
		glGenTextures(3, &texture[0]);					// Create The Texture
		for (int loop=0; loop<3; loop++)				// Loop Through 5 Textures
		{
			glBindTexture(GL_TEXTURE_2D, texture[loop]);
			glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage[loop]->sizeX, TextureImage[loop]->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[loop]->data);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		}
		for (int loop=0; loop<3; loop++)					// Loop Through 5 Textures
		{
			if (TextureImage[loop])						// If Texture Exists
			{
				if (TextureImage[loop]->data)			// If Texture Image Exists
				{
					free(TextureImage[loop]->data);		// Free The Texture Image Memory
				}
				free(TextureImage[loop]);				// Free The Image Structure
			}
		}
	}
	return Status;										// Return The Status
}



void DrawObject()										// Draw Our Ball
{
	glBindTexture(GL_TEXTURE_2D, texture[1]);			// Select Texture 2 (1)
	gluSphere(q, TAM_PERSONAJE, 30, 30);						// Draw First Sphere

	glBindTexture(GL_TEXTURE_2D, texture[2]);			// Select Texture 3 (2)
	glColor4f(1.0f, 1.0f, 1.0f, 0.4f);					// Set Color To White With 40% Alpha
	glEnable(GL_BLEND);									// Enable Blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);					// Set Blending Mode To Mix Based On SRC Alpha
	glEnable(GL_TEXTURE_GEN_S);							// Enable Sphere Mapping
	glEnable(GL_TEXTURE_GEN_T);							// Enable Sphere Mapping

	gluSphere(q, TAM_PERSONAJE, 30, 30);						// Draw Another Sphere Using New Texture
														// Textures Will Mix Creating A MultiTexture Effect (Reflection)
	glDisable(GL_TEXTURE_GEN_S);						// Disable Sphere Mapping
	glDisable(GL_TEXTURE_GEN_T);						// Disable Sphere Mapping
	glDisable(GL_BLEND);								// Disable Blending
}






static void
idle(void)
{
    glutPostRedisplay();
}

void TimerFunction(int value){
 	// Get keyboard input
	//move right
	if(GetAsyncKeyState(VK_RIGHT)&& !GetAsyncKeyState(VK_LEFT)){
        game.board->moveCharacter(game.pacman,MOVE_RIGHT);
   	}
    else
	//move left
	if(GetAsyncKeyState(VK_LEFT) && !GetAsyncKeyState(VK_RIGHT)){
        game.board->moveCharacter(game.pacman,MOVE_LEFT);
   	}
	//move up
	if(GetAsyncKeyState(VK_UP)&& !GetAsyncKeyState(VK_DOWN)){
        game.board->moveCharacter(game.pacman,MOVE_UP);
	}else
	//move down
	if(GetAsyncKeyState(VK_DOWN)&& !GetAsyncKeyState(VK_UP)){
        game.board->moveCharacter(game.pacman,MOVE_DOWN);
	}
    //Exit Game
	if(GetAsyncKeyState(VK_ESCAPE))
	{
		exit(0);
	}

	glutPostRedisplay();

    game.board->moveCharacter(game.blinky, game.planningMatrix->plan(game.blinky, game.blinky->getIntelligence()));
    game.board->moveCharacter(game.pinky, game.planningMatrix->plan(game.pinky, game.pinky->getIntelligence()));
    game.board->moveCharacter(game.inky, game.planningMatrix->plan(game.inky, game.inky->getIntelligence()));
    game.board->moveCharacter(game.clyde, game.planningMatrix->plan(game.clyde, game.clyde->getIntelligence()));
    score+='0'+game.getScore();

	glutTimerFunc(15, TimerFunction, 1);
}


int main(int argc, char *argv[]){

    glutInit(&argc, argv);
    srand(time(NULL));
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutGameModeString( "1280x800:32" );
	glutEnterGameMode();
    game.newGame();
    game.nextLevel();


    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);


    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

	glEnable(GL_LIGHT0);								// Enable Light 0
	glEnable(GL_LIGHTING);								// Enable Lighting


    //make mouse disappear

	glutSetCursor(GLUT_CURSOR_NONE);
    glutReshapeFunc(resize);
    glutDisplayFunc(display);

    glutIdleFunc(idle);
   	glutTimerFunc(15, TimerFunction, 1);

    glClearColor(0, 0, 0, 0);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);




    glutMainLoop();

	return 0;
}

