#ifndef __AWARD__
#define __AWARD__

#include "award.h"

Award::Award(int tipoPersonaje, char *str) : Character(tipoPersonaje, str){

}

Award::~Award(){}

Character * Award::doEffect(Character *p){
     //debe ser implementado por los hijos de la clase
     return p;
}

#endif
