#ifndef __PUTAWARD__CPP
#define __PUTAWARD__CPP

#include "putAward.h"

void loseAwardEffect(int premio){
     switch(premio){
       case AWARD_PASTILLA:
            if(game.pacman->getSpeed() - SPEED_PASTILLA >= GAME_PACMAN_SPEED_MIN)
                game.pacman->speedDown(SPEED_PASTILLA);
            game.pacman->setEatGhost(false);
            game.blinky->setIntelligence(10);
            game.pinky->setIntelligence(7);
            game.inky->setIntelligence(5);
            game.clyde->setIntelligence(2);
            break;
       case AWARD_CHICHARRA:
            game.blinky->setSpeed(GAME_GHOST_SPEED_START);
            game.pinky->setSpeed(GAME_GHOST_SPEED_START);
            game.inky->setSpeed(GAME_GHOST_SPEED_START);
            game.clyde->setSpeed(GAME_GHOST_SPEED_START);
            break;
       case AWARD_ENERGIZANTE:
            if(game.pacman->getSpeed() - SPEED_ENERGIZANTE >= GAME_PACMAN_SPEED_MIN)
                game.pacman->speedDown(SPEED_ENERGIZANTE);
            break;
       case AWARD_HAMBURGUESA:
            if(game.pacman->getSpeed() + SPEED_HAMBURGUESA <= GAME_PACMAN_SPEED_MAX)
                game.pacman->speedUp(SPEED_HAMBURGUESA);
            break;
     }
}

void awardMachine(int param){
     //Hay un 40% de prob de entrar
     if(rand()%100 < 40){
          //50% de prob de que ponga una fruta tipica, que tiene que 
          //aparecer hasta 2 veces por nivel
          if(rand()%100 < 50){
              if(game.getCantFrutaTipica() < 2){
                   game.addCantFrutaTipica();
                   game.board->celda[12][13].addAward(new FrutaTipica());
              }
          }
          else{
               int i=0, j=0;
               do{
                   i=rand()%GAME_BOARD_HIGH;
                   j=rand()%GAME_BOARD_WIDTH;
               }while(game.board->celda[i][j].getWall());

               int frutaAPoner;
               switch(rand()%6){
                    case 0: game.board->celda[i][j].addAward(new Chicharra());
                            break;
                    case 1: game.board->celda[i][j].addAward(new Energizante());
                            break;
                    case 2: game.board->celda[i][j].addAward(new Frutilla());
                            break;
                    case 3: game.board->celda[i][j].addAward(new Hamburguesa());
                            break;
                    case 4: game.board->celda[i][j].addAward(new Piedra());
                            break;
                    case 5: game.board->celda[i][j].addAward(new Sandia());
                            break;
               }
          }
     }

	glutTimerFunc(15000, awardMachine, 1);
}

void countDownControl(int entero){
     game.alterTimeLeft(-1);
     if(game.getTimeLeft() <= 0)
          game.pacmanDied();  
     else
          glutTimerFunc(1000, countDownControl, 1);
}

#endif
