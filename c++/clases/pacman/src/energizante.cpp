#ifndef __ENERGIZANTE__
#define __ENERGIZANTE__

#include "Energizante.h"

Energizante::Energizante() : Award(AWARD_ENERGIZANTE, "Energizante"){
     strcpy(filePath,"images/energizante.png");
}

Energizante::~Energizante(){
}

Character * Energizante::doEffect(Character *p){
     if(p->getSpeed() + SPEED_ENERGIZANTE <= GAME_PACMAN_SPEED_MAX)
          p->speedUp(SPEED_ENERGIZANTE);

     setEffectTime(14000);

     return p;          
}

void Energizante::setEffectTime(unsigned int tiempo){
     glutTimerFunc(tiempo, loseAwardEffect, AWARD_ENERGIZANTE);
}

char * Energizante::getImagePath(){
     return filePath;
}

#endif
