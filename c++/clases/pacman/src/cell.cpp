#ifndef __CELL__
#define __CELL__

#include "cell.h"

Cell::Cell() : Component("celda"){
     contenido=CONT_VACIO;
     premio = NULL;
     personaje[0] = NULL;
     personaje[CHAR_PACMAN] = NULL;
     personaje[CHAR_BLINKY] = NULL;
     personaje[CHAR_PINKY] = NULL;
     personaje[CHAR_INKY] = NULL;
     personaje[CHAR_CLYDE] = NULL;
}

Cell::~Cell(){}

int Cell::addContent(int value){
     contenido = contenido  | value;
     if(contenido & (~CONT_VACIO)) contenido = contenido & ~(CONT_VACIO);

     return contenido;
}

int Cell::delContent(int value){
     contenido = contenido & (~value);
     if((contenido & (~CONT_VACIO))==0) contenido = CONT_VACIO;

     return contenido;
}

int Cell::getContent(){
     return contenido;            
}

int Cell::cleanContent(){
     contenido = CONT_VACIO;
     return contenido;            
}

Character * Cell::addCharacter(Character *character){
     int iam = character->whoami();
     personaje[iam] = character;
     
     switch(iam){
         case CHAR_PACMAN: addContent(CONT_PACMAN); break;
         case CHAR_BLINKY: addContent(CONT_BLINKY); break;
         case CHAR_PINKY: addContent(CONT_PINKY); break;
         case CHAR_INKY: addContent(CONT_INKY); break;
         case CHAR_CLYDE: addContent(CONT_CLYDE); break;
     }
     return character;
}

Character * Cell::delCharacter(int tipoPersonaje){
     personaje[tipoPersonaje] = NULL;
     switch(tipoPersonaje){
         case CHAR_PACMAN: delContent(CONT_PACMAN); break;
         case CHAR_BLINKY: delContent(CONT_BLINKY); break;
         case CHAR_PINKY: delContent(CONT_PINKY); break;
         case CHAR_INKY: delContent(CONT_INKY); break;
         case CHAR_CLYDE: delContent(CONT_CLYDE); break;
     }
     return personaje[tipoPersonaje];
}

Character * Cell::getCharacter(int tipoPersonaje){
     return personaje[tipoPersonaje];          
}

Award * Cell::addAward(Award *award){
     premio = award;
     addContent(CONT_AWARD);
     return premio;
}

Award * Cell::delAward(){
     delete premio;
     premio = NULL;
     delContent(CONT_AWARD);
     return premio;    
}

Award * Cell::getAward(){
      return premio;     
}

void Cell::addWall(int tipoPared){
     addContent(tipoPared);
}

void Cell::delWall(int tipoPared){
     delContent(tipoPared);
}

int Cell::getWall(){
     int contRestante = 0;
     contRestante = getContent() &  CONT_WALL;
      return contRestante;
}

void Cell::addPebble(){
     pebble=true;
}

void Cell::delPebble(){
     pebble=false;
}

bool Cell::hasPebble(){
     return pebble;
}
#endif
