#ifndef __BLINKY__
#define __BLINKY__

#include "blinky.h"
#include "game.h"
extern Game game;

Blinky::Blinky(char *name) : Ghost(CHAR_BLINKY, name){
     strcpy(filePath[0],"images/blinky.png");
     strcpy(filePath[1],"images/blueghost.png");
     strcpy(filePath[2],"images/eatenghost.png");
}

int Blinky::getImage(){
     if(getGoHome()) return 2; //fue comido
     if(game.pacman->getEatGhost()) return 1; //puede ser comido
     return 0; //color normal
}

char * Blinky::getImagePath(int id){
     return filePath[id];
}
#endif
