#ifndef __AWARD__H
#define __AWARD__H

#include <cstdlib>
#include <cstring>
#include "component.h"
#include "character.h"
#include "log.h"
#include "util.h"

class Award : public Character{
      public:
      Award(int, char *);
      ~Award();
      Character * doEffect(Character *); //ejecutar las propiedades que tiene un premio sobre el personaje
                                      //Esta funcion es implementada por cada heredero
};

#endif
