#ifndef __PIEDRA__H
#define __PIEDRA__H

/* Efectos del premio
//
//Decrementa 10 puntos al jugador
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"

extern Game game;

class Piedra : public Award{
      public:
      Piedra();
      ~Piedra();

      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
