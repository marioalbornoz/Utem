#ifndef __ENERGIZANTE__H
#define __ENERGIZANTE__H

/* Efectos del premio
//
//Aumenta en 1 la velocidad de pacman
//si no esta en su velocidad maxima
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"
#include <gl\glut.h>

extern Game game;
void loseAwardEffect(int);

class Energizante : public Award{
      public:
      Energizante();
      ~Energizante();

  	  void setEffectTime(unsigned int);
      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
