#ifndef __PASTILLA__H
#define __PASTILLA__H

/* Efectos del premio
//
//Aumenta en 2 la velocidad de pacman
//Da la posibilidad de comer a los fantansmas
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"
#include <gl\glut.h>

extern Game game;
void loseAwardEffect(int);

class Pastilla : public Award{
      public:
      Pastilla();
      ~Pastilla();

	  void setEffectTime(unsigned int);
      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
