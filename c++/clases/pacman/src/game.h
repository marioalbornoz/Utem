#ifndef __GAME__H
#define __GAME__H

#include "constantes.h"
#include "log.h"
#include "board.h"
#include "pacman.h"
#include "blinky.h"
#include "pinky.h"
#include "inky.h"
#include "clyde.h"
#include "planningMatrix.h"
#include <gl\glut.h>

class Game : Component{
      public:
      Game();
      ~Game();
      Board *board;
      Pacman *pacman;
      Blinky *blinky;
      Pinky *pinky;
      Inky *inky;
      Clyde *clyde;
      PlanningMatrix *planningMatrix;
      
      int getCantFrutaTipica();
      void addCantFrutaTipica();
      void newGame();
      void delGame();
      void pacmanDied();
      long int increaseScore(long int);
      long int decreaseScore(long int);
      long int getScore();
      void increaseLevel();
      void resetLevel();
      void resetCharacterPosition();
      int getLevel();
      void nextLevel();
      void setTimeLeft(int);
      int getTimeLeft();
      void alterTimeLeft(int);
      
      protected:
      bool gameStarted;
      long int score;
      int level;
      int cantFrutaTipica;
      int timeLeft;
         
      long int setScore(long int);
};

#endif
