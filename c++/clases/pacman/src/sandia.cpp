#ifndef __SANDIA__
#define __SANDIA__

#include "sandia.h"
#include "cstring"

Sandia::Sandia() : Award(AWARD_SANDIA, "Sandia"){
     strcpy(filePath,"images/sandia.png");
}

Sandia::~Sandia(){
}

Character * Sandia::doEffect(Character *p){
     game.increaseScore(SCORE_SANDIA);
     return p;
}

char * Sandia::getImagePath(){
     return filePath;
}

#endif
