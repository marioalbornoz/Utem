#ifndef __HAMBURGUESA__
#define __HAMBURGUESA__

#include "hamburguesa.h"

Hamburguesa::Hamburguesa() : Award(AWARD_HAMBURGUESA, "Hamburguesa"){
     strcpy(filePath,"images/hamburguesa.png");
}

Hamburguesa::~Hamburguesa(){
}

Character * Hamburguesa::doEffect(Character *p){
     if(p->getSpeed()-SPEED_HAMBURGUESA >= GAME_PACMAN_SPEED_MIN)
          p->speedDown(SPEED_HAMBURGUESA);
     
     setEffectTime(8000);
     
     return p;          
}

void Hamburguesa::setEffectTime(unsigned int tiempo){
     glutTimerFunc(tiempo, loseAwardEffect, AWARD_HAMBURGUESA);
}

char * Hamburguesa::getImagePath(){
     return filePath;
}

#endif
