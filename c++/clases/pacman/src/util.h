#ifndef __UTIL__H
#define __UTIL__H

class Point{
      public:
      double x, y;
      Point(){Point(0,0);}
      Point(double i, double j){x=i; y=j;}
      bool operator ==(Point p){
           if(this->x==p.x && this->y==p.y) return true; 
           else return false;
      }
      Point * operator =(Point p){
           this->x = p.x;
           this->y = p.y;
           return this;
      }
      Point * operator +=(Point p){
           this->x += p.x;
           this->y += p.y;
           return this;
      }
      Point * operator -=(Point p){
           this->x -= p.x;
           this->y -= p.y;
           return this;
      }
};

#endif
