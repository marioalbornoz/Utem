#ifndef __GAME__
#define __GAME__

#include "game.h"


void awardMachine(int);
void countDownControl(int);

Game::Game() : Component("Juego"){
     gameStarted = false;
     setScore(0);
     resetLevel();
     board=NULL;
     pacman=NULL;
     blinky=NULL;
     pinky=NULL;
     inky=NULL;
     clyde=NULL;
     
     timeLeft=0;
     
     board = new Board(GAME_BOARD_HIGH,GAME_BOARD_WIDTH);
     planningMatrix = new PlanningMatrix();
}

Game::~Game(){
     if(gameStarted){
          gameStarted = false;
          delGame();
     }

     delete board;
     delete planningMatrix;
}

void Game::newGame(){
     if(gameStarted){
          gameStarted = false;
          delGame();
     }

     cantFrutaTipica = 0;
     gameStarted = true;
     setScore(0);
     setTimeLeft(0);
     resetLevel();

     pacman = new Pacman("Pacman");
     blinky = new Blinky("Blinky anger");
     blinky->setIntelligence(10);
     pinky = new Pinky("Pinky the bitch");
     pinky->setIntelligence(7);
     inky = new Inky("Sticky Inky");
     inky->setIntelligence(5);
     clyde = new Clyde("Clyde the vagabond");
     clyde->setIntelligence(2);
     
     glutTimerFunc(15000, awardMachine, 1);
}

void Game::delGame(){
     board->clearBoardOfAwards();
     delete pacman;
     delete blinky;
     delete pinky;
     delete inky;
     delete clyde;
}

void Game::nextLevel(){
     board->clearBoardOfAwards();
     increaseLevel();
     setTimeLeft(315 - 15*getLevel()); //5min menos 15 segundos por nivel avanzado.  comienza en 315 para que el nivel 1 coincida con 5min
     board->addPebbles();
     board->addPastillas();
     resetCharacterPosition();
     board->delCharacters();
     board->addCharacters();
     PlaySound("sound/pacman.wav",NULL,SND_FILENAME|SND_ASYNC|SND_NOSTOP);
     cantFrutaTipica=0;
     glutTimerFunc(1000, countDownControl, 1); //Comienza el tiempo de juego
}

void Game::setTimeLeft(int seconds){
     timeLeft = seconds;
}

int Game::getTimeLeft(){
    return timeLeft;    
}

void Game::alterTimeLeft(int delta){
     timeLeft+=delta;
}

long int Game::increaseScore(long int delta){
     return (score += delta);
}

long int Game::decreaseScore(long int delta){
     return (score -= delta);
}

long int Game::getScore(){
     return score;
}

long int Game::setScore(long int delta){
     return (score = delta);
}

void Game::increaseLevel(){
     if(level < GAME_LEVEL_MAX)
          level++;
}

int Game::getLevel(){
    return level;    
}

void Game::resetLevel(){
     level = 0;
}

void Game::pacmanDied(){
     pacman->decreaseLife(1);
     if(pacman->getLifes() < 1){
         delGame();
     }
     else{
         board->delCharacters();
         resetCharacterPosition();
         board->addCharacters();
         setTimeLeft(315 - 15*getLevel()); //5min menos 15 segundos por nivel avanzado.  comienza en 315 para que el nivel 1 coincida con 5min
         glutTimerFunc(1000, countDownControl, 1); //Comienza el tiempo de juego
     }
}

void Game::resetCharacterPosition(){
     pacman->setPosition(GAME_CELL_PACMAN_START_X, GAME_CELL_PACMAN_START_Y);
     blinky->setPosition(GAME_CELL_GHOST_START_X, GAME_CELL_GHOST_START_Y);
     pinky->setPosition(GAME_CELL_GHOST_START_X+2, GAME_CELL_GHOST_START_Y);
     inky->setPosition(GAME_CELL_GHOST_START_X, GAME_CELL_GHOST_START_Y+1);
     clyde->setPosition(GAME_CELL_GHOST_START_X+2, GAME_CELL_GHOST_START_Y+1);
}

int Game::getCantFrutaTipica(){
    return cantFrutaTipica;
}

void Game::addCantFrutaTipica(){
     cantFrutaTipica++;     
}

#endif
