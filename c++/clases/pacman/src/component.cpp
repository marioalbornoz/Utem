#ifndef __COMPONENT__
#define __COMPONENT__

#include "component.h"
#include "cstring"

Component::Component(char *str){
    name = NULL;
    setName(str);
    iam = 0;
}

Component::Component(char *str, int who_am_I){
    name = NULL;
    setName(str);
    iam = who_am_I;
}

Component::~Component(){
    if(name!=NULL) delete []name;
}

int Component::whoami(){
    return iam;
}


void Component::setName(char *str){
     if(name==NULL){
          name = new char[strlen(str)+1];
          strcpy(name, str);
     }
     else{
          delete []name;
          name = NULL;
          setName(str);
     }
}

char *Component::getName(){
     return name;
}

char *Component::getImagePath(){
     return NULL;
}

#endif
