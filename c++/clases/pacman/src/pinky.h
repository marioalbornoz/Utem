#ifndef __PINKY__H
#define __PINKY__H

#include "ghost.h"
#include "constantes.h"

class Pinky : public Ghost{
      public:
      Pinky(char *);
      int getImage();
      char *getImagePath(int);      
      protected:
      char filePath[3][30];
};

#endif
