#ifndef __CLYDE__H
#define __CLYDE__H

#include "ghost.h"
#include "constantes.h"

class Clyde : public Ghost{
      public:
      Clyde(char *);
      int getImage();
      char *getImagePath(int);
      protected:
      char filePath[3][30];
};

#endif
