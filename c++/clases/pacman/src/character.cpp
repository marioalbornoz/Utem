#ifndef __CHARACTER__
#define __CHARACTER__

#include "character.h"

Character::Character(int tipoPersonaje, char *name) : Component(name, tipoPersonaje){
     setName(name);
     setSpeed(0);
     setPosition(0,0);
}

Character::~Character(){
}

void Character::setPosition(double x,double y){
     this->x = x;
     this->y = y;
}

void Character::setPosition(Point *point){
     setPosition(point->x, point->y);
}

void Character::getPosition(Point *p){
     p->x = this->x;
     p->y = this->y;
}

void Character::setSpeed(double newSpeed){
     speed = newSpeed;
}

double Character::getSpeed(){
     return speed;
}

void Character::speedUp(double delta){
     setSpeed(getSpeed()+delta);     
}

void Character::speedDown(double delta){
     setSpeed(getSpeed()-delta);
}

#endif
