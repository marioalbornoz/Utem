#ifndef __DISPLAY__CPP
#define __DISPLAY__CPP

#include "display.h"

//Funcion de salida de texto
void output(float x, float y, char *string)
{
  int len, i;
  glRasterPos2f(x, y);
  len = (int) strlen(string);
  
  for (i = 0; i < len; i++) {
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, string[i]);
  }
}


void resize(int width, int height){
    const float ar = (float) width / (float) height;
    
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

//Dibuja la pantalla
 void display(void){
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const double a = t*90.0;
    Point paux;
    int i;
    float cont=1;
    char puntaje[6], bolita[5], timeleft[4];
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    //Salidas de Texto
    //Titulo
    for(GLfloat i=-2;(i<=-1.95);i+=0.01)
        for(GLfloat j=3.5;(j>=3.45);j-=0.01){
            glColor3f(1,1,1);
            glPushMatrix();
            output(i,j,".: P A C M A N  3 D :.");
            glPopMatrix();
    }
    
    //Vidas
    glColor3f(1,1,1);
    glPushMatrix();
    output(-11.25,2.7,"Vidas");
    glPopMatrix();
        //Dibuja vidas
    for(i=0;i<game.pacman->getLifes();i++){
        glColor3f(1,1,0);
        glPushMatrix();
        glTranslatef(-18.25f,-i+8.4f, DISTANCIA);
        glTranslatef(0.5,0.5,0.5);
        glutSolidSphere(TAM_PERSONAJE,30,30);
    //  DrawObject();
        glPopMatrix();
    }    
    
    //Puntaje
    glColor3f(1,1,1);
    glPushMatrix();
    output(8.25,2.7,"Puntaje: ");
    glPopMatrix();

    glColor3f(1,1,1);    
    itoa(game.getScore(), puntaje, 10);  
    glPushMatrix();
    output(10.25,2.7,puntaje);
    glPopMatrix();

    //Pebbles remaining
    glColor3f(1,1,1);
    glPushMatrix();
    output(8.25,1.7,"Pebbles: ");
    glPopMatrix();

    glColor3f(1,1,1);    
    itoa(game.board->countPebbles(), bolita, 10);  
    glPushMatrix();
    output(10.25,1.7,bolita);
    glPopMatrix();
    
    //Time Left
    glColor3f(1,1,1);
    glPushMatrix();
    output(8.25,0.7,"Time Left: ");
    glPopMatrix();
    
    glColor3f(1,1,1);    
    itoa(game.getTimeLeft(),timeleft, 10);  
    glPushMatrix();
    output(10.25,0.7,timeleft);
    glPopMatrix();

    //Frutas
    
    //Descripcion de Fruta
	for (int i = 0; i < GAME_BOARD_HIGH; i++){
		for (int j = 0; j < GAME_BOARD_WIDTH; j++){
           //award cell
            if(game.board->celda[i][j].getContent() & CONT_AWARD){
    			if(game.board->celda[i][j].getAward()->whoami() == AWARD_PASTILLA){
    			    glColor3f(1,1,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.22f,10,10);
    				glPopMatrix(); 
                }            
      			if(game.board->celda[i][j].getAward()->whoami() == AWARD_FRUTATIPICA){
                    cont--;
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,-0.7,"Pastilla Presente!!");
                    glPopMatrix();                                                                        
    			    glColor3f(1,0,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix(); 
                    
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Frutita Tipica!!");
                    glPopMatrix();
                        
                }
                if(game.board->celda[i][j].getAward()->whoami() == AWARD_FRUTILLA){
                    cont--;
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,-0.7,"Pastilla Presente!!!");
                    glPopMatrix();                                                                        
        			    glColor3f(0.95,0.2,0.1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix();      

                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Frutilla... 10pts Extras!");
                    glPopMatrix();

                }
                if(game.board->celda[i][j].getAward()->whoami() == AWARD_SANDIA){
                cont--;
                glColor3f(1,1,1);
                glPushMatrix();
                output(8.25,-0.7,"Pastilla Presente!!!");
                glPopMatrix();                                                                        
    			    glColor3f(1,0,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix();      
    				
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Sandia... 20pts Extras!");
                    glPopMatrix();    				
                }
                if(game.board->celda[i][j].getAward()->whoami() == AWARD_PIEDRA){
                cont--;
                glColor3f(1,1,1);
                glPushMatrix();
                output(8.25,-0.7,"Pastilla Presente!!!");
                glPopMatrix();                                                                        
    			    glColor3f(1,0,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix();      
    				
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Fruta sorpresa :)");
                    glPopMatrix();    				
                }
                if(game.board->celda[i][j].getAward()->whoami() == AWARD_HAMBURGUESA){
                cont--;
                glColor3f(1,1,1);
                glPushMatrix();
                output(8.25,-0.7,"Pastilla Presente!!!");
                glPopMatrix();                                                                        
    			    glColor3f(1,0,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix(); 
                        
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Fruta sorpresa :)");
                    glPopMatrix();                       
                }
                if(game.board->celda[i][j].getAward()->whoami() == AWARD_ENERGIZANTE){
                cont--;
                glColor3f(1,1,1);
                glPushMatrix();
                output(8.25,-0.7,"Pastilla Presente!!!");
                glPopMatrix();                                                                        
    			    glColor3f(1,0,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix(); 
                    
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Fruta sorpresa :)");                  
                    glPopMatrix();                          
                }
                if(game.board->celda[i][j].getAward()->whoami() == AWARD_CHICHARRA){
                    cont--;
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,-0.7,"Pastilla Presente!!!");
                    glPopMatrix();                                                                        
        			    glColor3f(1,0,1);
    				glPushMatrix();
                	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
    				glTranslatef(0.5,0.5,0.5);
      	            glRotated(65,1,0,0);
                    glRotated(a,0,0,1);
    				glutSolidSphere(0.15f,10,10);
    				glPopMatrix();   
                    
                    glColor3f(1,1,1);
                    glPushMatrix();
                    output(8.25,cont-1.7,"Fruta sorpresa :)");
                    glPopMatrix();                         
                }
            }

            //wall cell
			if( game.board->celda[i][j].getWall()){
				glColor3f(0.27,0.02,0.91);
				glPushMatrix();
            	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
				glTranslatef(0.5,0.5,0.5);
				glutSolidCube(1.0f);
				glPopMatrix();
            }   
            //pebble cell
			if( game.board->celda[i][j].hasPebble()){
				glColor3f(1,1,1);
				glPushMatrix();
            	glTranslatef(j-(float) GAME_BOARD_HIGH / 1.4f,-i+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
				glTranslatef(0.5,0.5,0.5);
  	            glRotated(65,1,0,0);
                glRotated(a,0,0,1);
				glutWireSphere(0.15f,5,5);
				glPopMatrix();
            }
       }
   }
   
   //Pacman cell
   game.pacman->getPosition(&paux);
   glColor3f(1,1,0);
   glPushMatrix();
   glTranslatef(paux.x-(float) GAME_BOARD_HIGH / 1.4f,-paux.y+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
   glTranslatef(0.5,0.5,0.5);
   glutSolidSphere(TAM_PERSONAJE,30,30);
//  DrawObject();
   glPopMatrix();
   
   glColor3f(0,0.2,0.5);
   //Blinky cell
   game.blinky->getPosition(&paux);
   if(!game.pacman->getEatGhost() && !game.blinky->getGoHome()) glColor3f(1,0,0);
   if(game.blinky->getGoHome()) glColor3f(0,0,0);
   glPushMatrix();
   glTranslatef(paux.x-(float) GAME_BOARD_HIGH / 1.4f,-paux.y+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
   glTranslatef(0.5,0.5,0.5);
   glutSolidSphere(TAM_PERSONAJE,30,30);
   glPopMatrix();

   glColor3f(0,0.2,0.5);   
   //Pinky cell
   game.pinky->getPosition(&paux);
   if(!game.pacman->getEatGhost()) glColor3f(0.9,0.3,0.5);
   if(game.pinky->getGoHome()) glColor3f(0,0,0);
   glPushMatrix();
   glTranslatef(paux.x-(float) GAME_BOARD_HIGH / 1.4f,-paux.y+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
   glTranslatef(0.5,0.5,0.5);
   glutSolidSphere(TAM_PERSONAJE,30,30);
   glPopMatrix();

   glColor3f(0,0.2,0.5);   
   //Inky cell
   game.inky->getPosition(&paux);
   if(!game.pacman->getEatGhost()) glColor3f(0,0,1);
   if(game.inky->getGoHome()) glColor3f(0,0,0);
   glPushMatrix();
   glTranslatef(paux.x-(float) GAME_BOARD_HIGH / 1.4f,-paux.y+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
   glTranslatef(0.5,0.5,0.5);
   glutSolidSphere(TAM_PERSONAJE,30,30);
   glPopMatrix();

   glColor3f(0,0.2,0.5);   
   //Clyde cell
   game.clyde->getPosition(&paux);
   if(!game.pacman->getEatGhost()) glColor3f(0.9,0.5,0);
   if(game.clyde->getGoHome()) glColor3f(0,0,0);
   glPushMatrix();
   glTranslatef(paux.x-(float) GAME_BOARD_HIGH / 1.4f,-paux.y+GAME_BOARD_WIDTH/2.9f, DISTANCIA);
   glTranslatef(0.5,0.5,0.5);
   glutSolidSphere(TAM_PERSONAJE,30,30);
   glPopMatrix();

   //Perspectiva Tablero
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   
   gluLookAt(0,-7.5f,13,0,0,-15,0,1,0);

   glutSwapBuffers();
}
#endif
