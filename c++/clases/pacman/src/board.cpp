#ifndef __BOARD__
#define __BOARD__

#include "board.h"
#include "game.h"
#include "constantes.h"
#include "frutaTipica.h"
#include "frutilla.h"
#include "sandia.h"
#include "piedra.h"
#include "hamburguesa.h"
#include "pastilla.h"
#include "energizante.h"
#include "chicharra.h"

extern Game game;


Board::Board(int alto, int largo) : Component("Tablero"){
      high = alto;
      width = largo;
      pebbles = 0;

      celda = new Cell*[high];
      for(int i=0; i<high; i++){
           celda[i] = new Cell[width];
      }
                  
      addWalls();
}

Board::~Board(){
     for(int i=0; i<high; i++)
             delete []celda[i];
     delete []celda;
}

void Board::addPastillas(){
     celda[1][1].addAward(new Pastilla());
     celda[GAME_BOARD_HIGH-2][1].addAward(new Pastilla());
     celda[1][GAME_BOARD_WIDTH-2].addAward(new Pastilla());
     celda[GAME_BOARD_HIGH-2][GAME_BOARD_WIDTH-2].addAward(new Pastilla());
}

void Board::addPebbles(){
     for(int i=1; i<high-1; i++){
          for(int j=1; j<width-1; j++){
               if((i==9 || i==10) && (j>9 && j<17)) continue;
               if(i==8 && j==13) continue;
               if(celda[i][j].getWall()) continue;
               celda[i][j].addPebble();
               pebbles++;
          }
     }
     
     //se quita la pastilla de donde comienza pacman
     celda[GAME_CELL_PACMAN_START_Y][GAME_CELL_PACMAN_START_X].delPebble(); pebbles--;

     
     //se quitan los pebbles de donde deben estar las pastillas que permiten
     //a pacman comer fantasmas
     celda[1][1].delPebble(); pebbles--;
     celda[GAME_BOARD_HIGH-2][1].delPebble(); pebbles--;
     celda[1][GAME_BOARD_WIDTH-2].delPebble(); pebbles--;
     celda[GAME_BOARD_HIGH-2][GAME_BOARD_WIDTH-2].delPebble(); pebbles--;
}

void Board::addCharacters(){
     Point paux;
     game.pacman->getPosition(&paux);
     celda[(int)paux.y][(int)paux.x].addCharacter(game.pacman);
     
     game.blinky->getPosition(&paux);
     celda[(int)paux.y][(int)paux.x].addCharacter(game.blinky);
     game.blinky->eated = false;
     
     game.pinky->getPosition(&paux);
     celda[(int)paux.y][(int)paux.x].addCharacter(game.pinky);
     game.pinky->eated = false;
     
     game.inky->getPosition(&paux);
     celda[(int)paux.y][(int)paux.x].addCharacter(game.inky);
     game.inky->eated = false;
     
     game.clyde->getPosition(&paux);
     celda[(int)paux.y][(int)paux.x].addCharacter(game.clyde);
     game.clyde->eated = false;
}

void Board::delCharacters(){
     Point paux;
     game.pacman->getPosition(&paux);
     celda[(int)floor(paux.y)][(int)floor(paux.x)].delCharacter(CHAR_PACMAN);
     game.blinky->getPosition(&paux);
     celda[(int)floor(paux.y)][(int)floor(paux.x)].delCharacter(CHAR_BLINKY);
     game.pinky->getPosition(&paux);
     celda[(int)floor(paux.y)][(int)floor(paux.x)].delCharacter(CHAR_PINKY);
     game.inky->getPosition(&paux);
     celda[(int)floor(paux.y)][(int)floor(paux.x)].delCharacter(CHAR_INKY);
     game.clyde->getPosition(&paux);
     celda[(int)floor(paux.y)][(int)floor(paux.x)].delCharacter(CHAR_CLYDE);
}

int Board::countPebbles(){
    return pebbles;
}

void Board::delPebbles(){
      for(int i=0; i<high; i++)
            for(int j=0; j<width; j++)
                 celda[i][j].delPebble();
}

void Board::addWalls(){
     int i, j;
     
     //marco del laberinto
     for(i=0; i<GAME_BOARD_HIGH; i++){
          celda[i][0].addWall(CONT_WALL);
          celda[i][GAME_BOARD_WIDTH-1].addWall(CONT_WALL);
     }
     for(j=1; j<GAME_BOARD_WIDTH-1; j++){
          celda[0][j].addWall(CONT_WALL);
          celda[GAME_BOARD_HIGH-1][j].addWall(CONT_WALL);
     }
     
     //barrido por linea
     celda[1][10].addWall(CONT_WALL);
     celda[1][12].addWall(CONT_WALL);
     celda[1][14].addWall(CONT_WALL);
     celda[1][16].addWall(CONT_WALL);
     celda[2][2].addWall(CONT_WALL);
     celda[2][3].addWall(CONT_WALL);
     celda[2][4].addWall(CONT_WALL);
     celda[2][6].addWall(CONT_WALL);
     celda[2][7].addWall(CONT_WALL);
     celda[2][8].addWall(CONT_WALL);
     celda[2][10].addWall(CONT_WALL);
     celda[2][16].addWall(CONT_WALL);
     celda[2][18].addWall(CONT_WALL);
     celda[2][19].addWall(CONT_WALL);
     celda[2][20].addWall(CONT_WALL);
     celda[2][22].addWall(CONT_WALL);
     celda[2][23].addWall(CONT_WALL);
     celda[2][24].addWall(CONT_WALL);
     celda[3][2].addWall(CONT_WALL);
     celda[3][8].addWall(CONT_WALL);
     celda[3][10].addWall(CONT_WALL);
     celda[3][12].addWall(CONT_WALL);
     celda[3][14].addWall(CONT_WALL);
     celda[3][16].addWall(CONT_WALL);
     celda[3][18].addWall(CONT_WALL);
     celda[3][24].addWall(CONT_WALL);
     celda[4][4].addWall(CONT_WALL);
     celda[4][5].addWall(CONT_WALL);
     celda[4][6].addWall(CONT_WALL);
     celda[4][8].addWall(CONT_WALL);
     celda[4][12].addWall(CONT_WALL);
     celda[4][13].addWall(CONT_WALL);
     celda[4][14].addWall(CONT_WALL);
     celda[4][18].addWall(CONT_WALL);
     celda[4][20].addWall(CONT_WALL);
     celda[4][21].addWall(CONT_WALL);
     celda[4][22].addWall(CONT_WALL);
     celda[5][2].addWall(CONT_WALL);
     celda[5][8].addWall(CONT_WALL);
     celda[5][10].addWall(CONT_WALL);
     celda[5][16].addWall(CONT_WALL);
     celda[5][18].addWall(CONT_WALL);
     celda[5][24].addWall(CONT_WALL);
     celda[6][2].addWall(CONT_WALL);
     celda[6][3].addWall(CONT_WALL);
     celda[6][4].addWall(CONT_WALL);
     celda[6][6].addWall(CONT_WALL);
     celda[6][7].addWall(CONT_WALL);
     celda[6][8].addWall(CONT_WALL);
     celda[6][10].addWall(CONT_WALL);
     celda[6][11].addWall(CONT_WALL);
     celda[6][12].addWall(CONT_WALL);
     celda[6][13].addWall(CONT_WALL);
     celda[6][14].addWall(CONT_WALL);
     celda[6][15].addWall(CONT_WALL);
     celda[6][16].addWall(CONT_WALL);
     celda[6][18].addWall(CONT_WALL);
     celda[6][19].addWall(CONT_WALL);
     celda[6][20].addWall(CONT_WALL);
     celda[6][22].addWall(CONT_WALL);
     celda[6][23].addWall(CONT_WALL);
     celda[6][24].addWall(CONT_WALL);
     celda[8][1].addWall(CONT_WALL);
     celda[8][2].addWall(CONT_WALL);
     celda[8][3].addWall(CONT_WALL);
     celda[8][4].addWall(CONT_WALL);
     celda[8][5].addWall(CONT_WALL);
     celda[8][7].addWall(CONT_WALL);
     celda[8][8].addWall(CONT_WALL);
     celda[8][10].addWall(CONT_WALL);
     celda[8][11].addWall(CONT_WALL);
     celda[8][12].addWall(CONT_WALL);
     celda[8][14].addWall(CONT_WALL);
     celda[8][15].addWall(CONT_WALL);
     celda[8][16].addWall(CONT_WALL);
     celda[8][18].addWall(CONT_WALL);
     celda[8][19].addWall(CONT_WALL);
     celda[8][21].addWall(CONT_WALL);
     celda[8][22].addWall(CONT_WALL);
     celda[8][23].addWall(CONT_WALL);
     celda[8][24].addWall(CONT_WALL);
     celda[8][25].addWall(CONT_WALL);
     celda[9][10].addWall(CONT_WALL);
     celda[9][16].addWall(CONT_WALL);
     celda[10][2].addWall(CONT_WALL);
     celda[10][3].addWall(CONT_WALL);
     celda[10][4].addWall(CONT_WALL);
     celda[10][5].addWall(CONT_WALL);
     celda[10][6].addWall(CONT_WALL);
     celda[10][8].addWall(CONT_WALL);
     celda[10][10].addWall(CONT_WALL);
     celda[10][16].addWall(CONT_WALL);
     celda[10][18].addWall(CONT_WALL);
     celda[10][20].addWall(CONT_WALL);
     celda[10][21].addWall(CONT_WALL);
     celda[10][22].addWall(CONT_WALL);
     celda[10][23].addWall(CONT_WALL);
     celda[10][24].addWall(CONT_WALL);
     celda[11][2].addWall(CONT_WALL);
     celda[11][8].addWall(CONT_WALL);
     celda[11][10].addWall(CONT_WALL);
     celda[11][11].addWall(CONT_WALL);
     celda[11][12].addWall(CONT_WALL);
     celda[11][13].addWall(CONT_WALL);
     celda[11][14].addWall(CONT_WALL);
     celda[11][15].addWall(CONT_WALL);
     celda[11][16].addWall(CONT_WALL);
     celda[11][18].addWall(CONT_WALL);
     celda[11][24].addWall(CONT_WALL);
     celda[12][2].addWall(CONT_WALL);
     celda[12][4].addWall(CONT_WALL);
     celda[12][5].addWall(CONT_WALL);
     celda[12][6].addWall(CONT_WALL);
     celda[12][7].addWall(CONT_WALL);
     celda[12][8].addWall(CONT_WALL);
     celda[12][18].addWall(CONT_WALL);
     celda[12][19].addWall(CONT_WALL);
     celda[12][20].addWall(CONT_WALL);
     celda[12][21].addWall(CONT_WALL);
     celda[12][22].addWall(CONT_WALL);
     celda[12][24].addWall(CONT_WALL);
     celda[13][2].addWall(CONT_WALL);
     celda[13][8].addWall(CONT_WALL);
     celda[13][9].addWall(CONT_WALL);
     celda[13][10].addWall(CONT_WALL);
     celda[13][12].addWall(CONT_WALL);
     celda[13][13].addWall(CONT_WALL);
     celda[13][14].addWall(CONT_WALL);
     celda[13][16].addWall(CONT_WALL);
     celda[13][17].addWall(CONT_WALL);
     celda[13][18].addWall(CONT_WALL);
     celda[13][24].addWall(CONT_WALL);
     celda[14][2].addWall(CONT_WALL);
     celda[14][3].addWall(CONT_WALL);
     celda[14][4].addWall(CONT_WALL);
     celda[14][6].addWall(CONT_WALL);
     celda[14][20].addWall(CONT_WALL);
     celda[14][22].addWall(CONT_WALL);
     celda[14][23].addWall(CONT_WALL);
     celda[14][24].addWall(CONT_WALL);
     celda[15][6].addWall(CONT_WALL);
     celda[15][8].addWall(CONT_WALL);
     celda[15][9].addWall(CONT_WALL);
     celda[15][10].addWall(CONT_WALL);
     celda[15][11].addWall(CONT_WALL);
     celda[15][12].addWall(CONT_WALL);
     celda[15][14].addWall(CONT_WALL);
     celda[15][15].addWall(CONT_WALL);
     celda[15][16].addWall(CONT_WALL);
     celda[15][17].addWall(CONT_WALL);
     celda[15][18].addWall(CONT_WALL);
     celda[15][20].addWall(CONT_WALL);
     celda[16][1].addWall(CONT_WALL);
     celda[16][3].addWall(CONT_WALL);
     celda[16][4].addWall(CONT_WALL);
     celda[16][6].addWall(CONT_WALL);
     celda[16][20].addWall(CONT_WALL);
     celda[16][22].addWall(CONT_WALL);
     celda[16][23].addWall(CONT_WALL);
     celda[16][25].addWall(CONT_WALL);
     celda[17][6].addWall(CONT_WALL);
     celda[17][8].addWall(CONT_WALL);
     celda[17][9].addWall(CONT_WALL);
     celda[17][10].addWall(CONT_WALL);
     celda[17][11].addWall(CONT_WALL);
     celda[17][12].addWall(CONT_WALL);
     celda[17][13].addWall(CONT_WALL);
     celda[17][14].addWall(CONT_WALL);
     celda[17][15].addWall(CONT_WALL);
     celda[17][16].addWall(CONT_WALL);
     celda[17][17].addWall(CONT_WALL);
     celda[17][18].addWall(CONT_WALL);
     celda[17][20].addWall(CONT_WALL);
     celda[18][2].addWall(CONT_WALL);
     celda[18][4].addWall(CONT_WALL);
     celda[18][13].addWall(CONT_WALL);
     celda[18][22].addWall(CONT_WALL);
     celda[18][24].addWall(CONT_WALL);
     celda[19][2].addWall(CONT_WALL);
     celda[19][4].addWall(CONT_WALL);
     celda[19][5].addWall(CONT_WALL);
     celda[19][6].addWall(CONT_WALL);
     celda[19][7].addWall(CONT_WALL);
     celda[19][8].addWall(CONT_WALL);
     celda[19][9].addWall(CONT_WALL);
     celda[19][11].addWall(CONT_WALL);
     celda[19][13].addWall(CONT_WALL);
     celda[19][15].addWall(CONT_WALL);
     celda[19][17].addWall(CONT_WALL);
     celda[19][18].addWall(CONT_WALL);
     celda[19][19].addWall(CONT_WALL);
     celda[19][20].addWall(CONT_WALL);
     celda[19][21].addWall(CONT_WALL);
     celda[19][22].addWall(CONT_WALL);
     celda[19][24].addWall(CONT_WALL);
}

void Board::sendGhostToHome(Ghost *g){
     g->setGoHome(true);
}

void Board::pacmanStepOnCell(Cell *cell){
     //si hay un fantasma
     if(cell->getContent() & (CONT_BLINKY | CONT_PINKY | CONT_INKY | CONT_CLYDE)){
          //si pacman puede comer al fantasma
          if(game.pacman->getEatGhost()){
               //busca los fantasmas que se encuentran en la casilla
               if(cell->getContent() & CONT_BLINKY){
                    if(game.blinky->eated==false){
                        game.blinky->eated=true;
                        sendGhostToHome(game.blinky);
                        game.increaseScore(SCORE_GHOST);
                    }
               }
               if(cell->getContent() & CONT_PINKY){
                    if(game.pinky->eated==false){
                        game.pinky->eated=true;
                        sendGhostToHome(game.pinky);
                        game.increaseScore(SCORE_GHOST);
                    }
               }               
               if(cell->getContent() & CONT_INKY){
                    if(game.inky->eated==false){
                        game.inky->eated=true;
                        sendGhostToHome(game.inky);
                        game.increaseScore(SCORE_GHOST);
                    }
               }
               if(cell->getContent() & CONT_CLYDE){
                    if(game.clyde->eated==false){
                        game.clyde->eated=true;
                        sendGhostToHome(game.clyde);
                        game.increaseScore(SCORE_GHOST);
                    }
               }
          }
          //si pacman no puede comer al fantasma
          else{
               game.pacmanDied();
          }
     }

     //si hay un pebble (cosita que come pacman)
     if(cell->hasPebble()){
         pebbles--;
         cell->delPebble();
         game.increaseScore(SCORE_PEBBLE);
         PlaySound("sound/crunch.wav",NULL,SND_FILENAME|SND_ASYNC|SND_NOSTOP);
         if(countPebbles()<=0){
              game.nextLevel();
              PlaySound("sound/victory.wav",NULL,SND_FILENAME|SND_ASYNC|SND_NOSTOP);
         }
     }
    
     //si hay un premio
     if(cell->getContent() & CONT_AWARD){
          Award *award = cell->getAward();
          switch(award->whoami()){
              case AWARD_FRUTATIPICA: 
                   ((FrutaTipica *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_FRUTILLA: 
                   ((Frutilla *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_SANDIA: 
                   ((Sandia *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_PIEDRA: 
                   ((Piedra *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_HAMBURGUESA: 
                   ((Hamburguesa *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_PASTILLA: 
                   ((Pastilla *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_ENERGIZANTE: 
                   ((Energizante *)award)->doEffect(game.pacman); 
                   break;
              case AWARD_CHICHARRA: 
                   ((Chicharra *)award)->doEffect(game.pacman); 
                   break;
          }
          
          cell->delAward();
     }
}

void Board::ghostStepOnCell(Ghost *g, Cell *cell){
     //si esta pacman en la celda
     if(cell->getContent() & CONT_PACMAN){
          //si pacman puede comer al fantasma
          if(game.pacman->getEatGhost()){
               if(g->eated==false){
                   sendGhostToHome(g);
                   game.increaseScore(SCORE_GHOST);
               }
          }
          //si pacman no puede comer al fantasma
          else{
               game.pacmanDied();
          }
     }
}

void Board::moveCharacter(Character *c, int direccion){
     Point pActual, pNuevo;
     int xInt, yInt;
     
     c->getPosition(&pActual); //posicion actual del personaje
     if(direccion == MOVE_UP){
          pNuevo.x = pActual.x;
          pNuevo.y = pActual.y - c->getSpeed();
     }
     if(direccion == MOVE_DOWN){
          pNuevo.x = pActual.x;
          pNuevo.y = pActual.y + c->getSpeed();
     }
     if(direccion == MOVE_RIGHT){
          pNuevo.x = pActual.x + c->getSpeed();
          pNuevo.y = pActual.y;
     }
     if(direccion == MOVE_LEFT){
          pNuevo.x = pActual.x - c->getSpeed();
          pNuevo.y = pActual.y;
     }
     
     xInt = (int)floor(pNuevo.x);
     yInt = (int)floor(pNuevo.y);
     
     //redondeo para no entrar en pared     
     if(celda[yInt+1][xInt].getContent() & CONT_WALL)
          if(pNuevo.y-yInt>0)pNuevo.y=yInt;
     if(celda[yInt][xInt+1].getContent() & CONT_WALL)
          if(pNuevo.x-xInt>0)pNuevo.x=xInt;
     
     
     
     
     //se controla que en la casilla donde quiere ir no haya una pared
     if(celda[yInt][xInt].getContent() & CONT_WALL)
          return;
     
     //si es pacman se controla que no quiera entrar a la casa de los fantasmas
     if(yInt==8 && xInt==13 && c->whoami() == CHAR_PACMAN)
         return;
     
     //se quita al personaje de la celda actual
     celda[(int)floor(pActual.y)][(int)floor(pActual.x)].delCharacter(c->whoami());

     //se agrega al personaje a la celda nueva
     celda[yInt][xInt].addCharacter(c);
     c->setPosition(&pNuevo);
          
     //se ejecutan las acciones de entrar a la nueva celda
     if(c->whoami() == CHAR_PACMAN){
          pacmanStepOnCell(&celda[yInt][xInt]);
     }
     else{
          ghostStepOnCell((Ghost *)c, &celda[yInt][xInt]);
     }
}

void Board::clearBoardOfAwards(){
     for(int i=1; i<high-1; i++){
          for(int j=1; j<width-1; j++){
               if(celda[i][j].getAward()!= NULL){
                       celda[i][j].delAward();
               }
          }
     }
}

#endif
