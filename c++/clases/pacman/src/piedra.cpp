#ifndef __PIEDRA__
#define __PIEDRA__

#include "piedra.h"

Piedra::Piedra() : Award(AWARD_PIEDRA, "Piedra"){
     strcpy(filePath,"images/piedra.png");
}

Piedra::~Piedra(){
}

Character * Piedra::doEffect(Character *p){
     game.decreaseScore(SCORE_PIEDRA);
     return p;          
}

char * Piedra::getImagePath(){
     return filePath;
}

#endif
