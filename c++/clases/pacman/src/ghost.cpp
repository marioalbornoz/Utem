#ifndef __GHOST__
#define __GHOST__

#include "ghost.h"

Ghost::Ghost(int tipoFantasma, char *name) : Character(tipoFantasma, name){
     goHome=false;
     setSpeed(GAME_GHOST_SPEED_START);
     setIntelligence(1);
     eated = false;
}

void Ghost::setGoHome(bool go){
     goHome = go;    
}

bool Ghost::getGoHome(){
     return goHome;
}

void Ghost::setIntelligence(int grade){
     intelligence = grade;
}

int Ghost::getIntelligence(){
    return intelligence;
}

#endif
