#ifndef __PACMAN__
#define __PACMAN__

#include "pacman.h"
#include "cstring"

extern char **filesPath;

Pacman::Pacman(char *name) : Character(CHAR_PACMAN, name){
     setEatGhost(false);
     setSpeed(GAME_PACMAN_SPEED_START);
     lifes = 3;
     strcpy(filePath,"images/pacman.png");
}

void Pacman::setEatGhost(bool on){
     eatGhost = on;
}

int Pacman::increaseLife(int delta){
     return (lifes += delta);
}

int Pacman::decreaseLife(int delta){
     return (lifes -= delta);
}

int Pacman::getLifes(){
    return lifes;
}

bool Pacman::getEatGhost(){
     return eatGhost;
}

int Pacman::getImage(){
     return 0; //color normal
}

char * Pacman::getImagePath(){
     return filePath;
}

#endif
