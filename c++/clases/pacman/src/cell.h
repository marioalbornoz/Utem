#ifndef __CELL__H
#define __CELL__H

#include "constantes.h"
#include "component.h"
#include "character.h"
#include "award.h"



class Cell : public Component{
      public:
      Cell();
      ~Cell();
      
      //manejar el contenido de toda la celda
      int getContent();
      int cleanContent();

      //manejar el contenido
      Character * addCharacter(Character *);
      Character * delCharacter(int);
      Character * getCharacter(int);
      Award * addAward(Award *);
      Award * delAward();
      Award * getAward();
      void addWall(int);
      void delWall(int);
      int getWall();
      void addPebble();
      bool hasPebble();
      void delPebble();
      
      protected:
      int addContent(int);
      int delContent(int);
      int contenido; //contenido de la celda perteneciente a "enum cellContent"
      bool pebble; //objeto que come pacman
      Award *premio;  //puntero a un premio o NULL. "contenido" define si contiene un premio
      Character *personaje[6];  //puntero a un personaje. [pacman][blinky][pinky][inky][clyde]
};

#endif
