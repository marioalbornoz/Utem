#ifndef __CHICHARRA__H
#define __CHICHARRA__H

/* En honor al chapulin colorado
//Efectos del premio
//
//Pone la velocidad de los fantasmas en 0
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"
#include <gl\glut.h>

extern Game game;
void loseAwardEffect(int);

class Chicharra : public Award{
      public:
      Chicharra();
      ~Chicharra();
      
	  void setEffectTime(unsigned int);
      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
