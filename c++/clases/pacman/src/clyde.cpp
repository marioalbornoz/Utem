#ifndef __CLYDE__
#define __CLYDE__

#include "clyde.h"
#include "game.h"
extern Game game;

Clyde::Clyde(char *name) : Ghost(CHAR_CLYDE, name){
     strcpy(filePath[0],"images/clyde.png");
     strcpy(filePath[1],"images/blueghost.png");
     strcpy(filePath[2],"images/eatenghost.png");            
}

int Clyde::getImage(){
     if(getGoHome()) return 2; //fue comido
     if(game.pacman->getEatGhost()) return 1; //puede ser comido
     return 0; //color normal
}

char * Clyde::getImagePath(int id){
     return filePath[id];
}

#endif
