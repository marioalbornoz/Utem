#ifndef __PACMAN__H
#define __PACMAN__H

#include "character.h"
#include "constantes.h"

class Pacman : public Character{
      public:
      Pacman(char *);

      int getImage();
      char *getImagePath();
      void setEatGhost(bool);
      bool getEatGhost();
      int increaseLife(int);
      int decreaseLife(int);
      int getLifes();
      
      protected:
      char filePath[30];
      bool eatGhost;
      int lifes;
};

#endif
