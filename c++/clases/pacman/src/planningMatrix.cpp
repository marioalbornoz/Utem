#ifndef __PLANNING_MATRIX__
#define __PLANNING_MATRIX__

#include "planningMatrix.h"
#include "game.h"
#include "log.h"
extern Game game;
extern Log debugLog;

PlanningMatrix::PlanningMatrix(){
     celda = new int*[GAME_BOARD_HIGH];
     for(int i=0; i<GAME_BOARD_HIGH; i++){
           celda[i] = new int[GAME_BOARD_WIDTH];
     }
     
     for(int i=0; i<GAME_BOARD_HIGH; i++){
          for(int j=0; j<GAME_BOARD_WIDTH; j++){
               if(game.board->celda[i][j].getWall()) celda[i][j] = PM_WALL;
               else celda[i][j]=PM_VACIO;
          }
     }
     
     srand(time(NULL));
}

void PlanningMatrix::cleanUp(Ghost *ghost){
     Point pAux;
     
     debugLog.activated(false);
     switch(ghost->whoami()){
      case CHAR_BLINKY: debugLog.addLog("blinky: cleanUp para planificar movida"); break;
      case CHAR_PINKY: debugLog.addLog("pinky: cleanUp para planificar movida"); break;
      case CHAR_INKY: debugLog.addLog("inky: cleanUp para planificar movida"); break;
      case CHAR_CLYDE: debugLog.addLog("clyde: cleanUp para planificar movida"); break;
     }
     
     mejorPuntaje=0;
     puntaje=0;
     nivelMax=0;
     noPasarEntradaCasa = false;
     proxDireccion=MOVE_UP;
     
     for(int i=0; i<GAME_BOARD_HIGH; i++){
          for(int j=0; j<GAME_BOARD_WIDTH; j++){
               if(celda[i][j] != PM_WALL) celda[i][j] = PM_VACIO;
          }
     }
     
     //Si el fantasma fue comido
     //toma como objetivo su casa
     if(ghost->getGoHome()){
          ghost->getPosition(&pAux);
          
          //si ya llego a su casa
          if((int)pAux.x==GAME_CELL_GHOST_START_X && (int)pAux.y==GAME_CELL_GHOST_START_Y){
               ghost->setGoHome(false);
               ghost->eated=FALSE;
               //el objetivo es salir de la casa
               pAux.y = 7;
               pAux.x = 13;
               noPasarEntradaCasa = false;
               debugLog.activated(false);
               debugLog.addLog("volvi a casa ahora debo salir");
          }
          else{
              pAux.x=GAME_CELL_GHOST_START_X;
              pAux.y=GAME_CELL_GHOST_START_Y;
          }
     }
     //Si el fantasma no fue comido
     else{
          ghost->getPosition(&pAux);
          
          //Si se esta dentro de la casa el objetivo primero es salir de ella
          if(pAux.y>8 && pAux.y<11 && pAux.x>10 && pAux.x<16){
               pAux.y = 7;
               pAux.x = 13;
          }
          //Si se esta fuera de la casa el objetivo es pacman
          else{
               noPasarEntradaCasa = true;
               game.pacman->getPosition(&pAux);
               //Si pacman puede comer a los fantasmas
               //se mira la posicion de pacman y se va a otro lugar
               if(game.pacman->getEatGhost()){
                    if(pAux.x <= GAME_BOARD_WIDTH/2 && pAux.y <= GAME_BOARD_HIGH/2){
                         pAux.x=GAME_BOARD_WIDTH-2;
                         pAux.y=1;
                    }
                    if(pAux.x > GAME_BOARD_WIDTH/2 && pAux.y <= GAME_BOARD_HIGH/2){
                         pAux.x=GAME_BOARD_WIDTH-2;
                         pAux.y=GAME_BOARD_HIGH-2;
                    }
                    if(pAux.x <= GAME_BOARD_WIDTH/2 && pAux.y > GAME_BOARD_HIGH/2){
                         pAux.x=1;
                         pAux.y=1;
                    }
                    if(pAux.x > GAME_BOARD_WIDTH/2 && pAux.y > GAME_BOARD_HIGH/2){
                         pAux.x=1;
                         pAux.y=GAME_BOARD_HIGH-2;
                    }
               }
          }
     }
          
     yPacman = (int)pAux.y;
     xPacman = (int)pAux.x;
     celda[yPacman][xPacman] = PM_PACMAN;
}

PlanningMatrix::~PlanningMatrix(){
     for(int i=0; i<GAME_BOARD_HIGH; i++) delete []celda[i];
     delete []celda;
}

void PlanningMatrix::search(int x, int y, int nivel){

     //si se esta en la entrada a la casa y no se debe pasar
     if(y==8 && x==13 && noPasarEntradaCasa) 
          return;
     
     //si se llego a pacman
     if(celda[y][x]==PM_PACMAN){
          //el puntaje es una constante por la cantidad de pasos que se dieron
          if(mejorPuntaje < (1000 * (nivelMax-nivel))){
               mejorPuntaje=1000 * (nivelMax-nivel);
          }

          return;
     }
     //si se llego al limite de profundidad o pasos
     if(nivel>=nivelMax){
          //el puntaje es una constante por la cantidad de pasos que se dieron
          //menos la distancia lineal a pacman por una constante
          puntaje = 1000 - 10*(int)sqrt(pow(x-xPacman, 2) + pow(y-yPacman, 2));
          if(mejorPuntaje < puntaje){
               mejorPuntaje=puntaje;
          }

          return;              
     }
     
     celda[y][x]=PM_VISITADO;

     if(celda[y-1][x] != PM_WALL && celda[y-1][x] != PM_VISITADO)
          search(x,y-1,nivel+1);

     if(celda[y+1][x] != PM_WALL && celda[y+1][x] != PM_VISITADO)
          search(x,y+1,nivel+1);
     
     if(celda[y][x-1] != PM_WALL && celda[y][x-1] != PM_VISITADO)
          search(x-1,y,nivel+1);
     
     if(celda[y][x+1] != PM_WALL && celda[y][x+1] != PM_VISITADO)
          search(x+1,y,nivel+1);
     
     celda[y][x]=PM_VACIO;
}

int PlanningMatrix::plan(Ghost *ghost, int limite){
    int puntajeAux=0;
    Point point;

    ghost->getPosition(&point);
    
    //tiene GAME_RANDOM_PLAY de probabilidad de moverse al azar
    if(rand()%100 < GAME_RANDOM_PLAY*100){
         do{
             switch(rand()%4){
                 case 0: if(!(game.board->celda[(int)floor(point.y)-1][(int)floor(point.x)].getContent() & CONT_WALL)){
                              return MOVE_UP; break;
                         }
                 case 1: if(!(game.board->celda[(int)floor(point.y)+1][(int)floor(point.x)].getContent() & CONT_WALL)){
                              return MOVE_DOWN; break;
                         }
                 case 2: if(!(game.board->celda[(int)floor(point.y)][(int)floor(point.x)-1].getContent() & CONT_WALL)){
                              return MOVE_LEFT; break;
                         }
                 case 3: if(!(game.board->celda[(int)floor(point.y)][(int)floor(point.x)+1].getContent() & CONT_WALL)){
                              return MOVE_RIGHT; break;
                         }
             }
         }while(1);
    }
    
    cleanUp(ghost);
    nivelMax=limite;
    
     //Se busca el mejor movimiento empezando en cada una de las 
     //cuatro direcciones posibles, para elegir por donde ir
     if(celda[(int)point.y-1][(int)point.x] != PM_WALL && celda[(int)point.y-1][(int)point.x] != PM_VISITADO){
          search((int)point.x, (int)point.y-1, 1);
          puntajeAux = mejorPuntaje;
          proxDireccion = MOVE_UP;
     }
     if(celda[(int)point.y+1][(int)point.x] != PM_WALL && celda[(int)point.y+1][(int)point.x] != PM_VISITADO){
          search((int)point.x, (int)point.y+1, 1);
          if(puntajeAux < mejorPuntaje){
               puntajeAux = mejorPuntaje;
               proxDireccion = MOVE_DOWN;
          }
     }
     if(celda[(int)point.y][(int)point.x-1] != PM_WALL && celda[(int)point.y][(int)point.x-1] != PM_VISITADO){
          search((int)point.x-1, (int)point.y, 1);
          if(puntajeAux < mejorPuntaje){
               puntajeAux = mejorPuntaje;
               proxDireccion = MOVE_LEFT;
          }
     }         
     if(celda[(int)point.y][(int)point.x+1] != PM_WALL && celda[(int)point.y][(int)point.x+1] != PM_VISITADO){
          search((int)point.x+1, (int)point.y, 1);
          if(puntajeAux < mejorPuntaje){
               puntajeAux = mejorPuntaje;
               proxDireccion = MOVE_RIGHT;
          }
     }        
    
    return proxDireccion;
}

#endif
