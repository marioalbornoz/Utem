#ifndef __FRUTATIPICA__H
#define __FRUTATIPICA__H

/* Efectos del premio
//
//Incrementa 100*NIVEL puntos al jugador
//Cada 2 niveles corresponde otra imagen
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"

extern Game game;

class FrutaTipica : public Award{
      public:
      FrutaTipica();
      ~FrutaTipica();

      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[6][30];
};

#endif
