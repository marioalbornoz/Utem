#ifndef __LOG__H
#define __LOG__H

#include <cstdio>
#include <string>
#include <time.h>

class Log{
      public:
      Log(char *); //como parametro el path salida de los logs
      ~Log();
      void setPath(char *);
      char *getPath();
      bool addLog(char *);
      void activated(bool); //activar o desactivar el log
             
      private:
      char *path;
      FILE *fp;
      bool active; //se puede activar o desactivar
};

#endif
