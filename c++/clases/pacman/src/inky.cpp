#ifndef __INKY__
#define __INKY__

#include "inky.h"
#include "game.h"
extern Game game;

Inky::Inky(char *name) : Ghost(CHAR_INKY, name){
     strcpy(filePath[0],"images/inky.png");
     strcpy(filePath[1],"images/blueghost.png");
     strcpy(filePath[2],"images/eatenghost.png");            
}

int Inky::getImage(){
     if(getGoHome()) return 2; //fue comido
     if(game.pacman->getEatGhost()) return 1; //puede ser comido
     return 0; //color normal
}

char * Inky::getImagePath(int id){
     return filePath[id];
}

#endif
