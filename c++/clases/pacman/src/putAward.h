#ifndef __PUTAWARD__H
#define __PUTAWARD__H

#include <gl\glut.h>
#include <cstdlib>
#include <gl\glaux.h>
#include <math.h>
#include "frutaTipica.h"
#include "chicharra.h"
#include "energizante.h"
#include "frutilla.h"
#include "hamburguesa.h"
#include "piedra.h"
#include "sandia.h"
#include "game.h" 
#include "constantes.h"

extern Game game;

void loseAwardEffect(int premio);
void awardMachine(int param);
void countDownControl(int entero);

#endif
