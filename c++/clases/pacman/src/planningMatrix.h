#ifndef __PLANNING_MATRIX__H
#define __PLANNING_MATRIX__H

#include <math.h>
#include <cstdlib>
#include "constantes.h"
#include "util.h"
#include "ghost.h"

#define PM_VACIO 0
#define PM_VISITADO 1
#define PM_WALL 2
#define PM_PACMAN 3

class PlanningMatrix{
      public:
      PlanningMatrix();
      ~PlanningMatrix();
      int **celda;
      int proxDireccion;
      int mejorPuntaje, puntaje;
      int nivelMax;
      int xPacman, yPacman;
      bool noPasarEntradaCasa;
      
      int plan(Ghost *ghost, int limite); //planear una nueva movida con un arbol de hasta limite niveles.
                            //Luego de llamar a esta funcion el resultado
                            //estara disponible en proxDireccion y sera tambien devuelto por la funcion
                            //mejorPuntaje contendra el puntaje que se le dio a la movida
                   
                   
      void cleanUp(Ghost *ghost); //limpia la matriz auxiliar para un nuevo uso
      
      //se deben pasar como parametros la posicion (x,y)
      //del personaje y la matriz se completa con PM_VISITADO
      //Al termino de la funcion el camino mas eficiente estara marcado con PM_VISITADO
      //se mueve hasta profundidad veces, cuando llega a cero evalua su resultado segun
      //la distancia lineal a pacman.
      //Si un camino tiene mejor puntaje que lo guardado, entonces se elige este nuevo
      //camino pero solo guardando la direccion del paso inicial y el puntaje
      void search(int x,int y, int nivel);
      
      //hace que el fantasme se escape de pacman
      void scape(int x,int y, int nivel);
};

#endif
