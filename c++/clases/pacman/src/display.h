#ifndef __DISPLAY__H
#define __DISPLAY__H

#include <gl\gl.h>
#include <gl\glut.h>
#include <cstdlib>
#include <gl\glaux.h>
#include <math.h>
#include "game.h"
#include "constantes.h"
#include "util.h"

extern Game game;

void output(float x, float y, char *string);
void resize(int width, int height);
void display(void);

#endif
