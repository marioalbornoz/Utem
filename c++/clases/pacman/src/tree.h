#ifndef __TREE__H
#define __TREE__H
       
#include <cstdlib>
#include "node.h"
                
class Tree{     
     public:    
     Tree();
     Node *root;
     Node *betterNode;
     
     void search();
};              
                
#endif          
