#ifndef __FRUTILLA__H
#define __FRUTILLA__H

/* Efectos del premio
//
//Incrementa 10 puntos al jugador
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"

extern Game game;
extern char *filesPath[];

class Frutilla : public Award{
      public:
      Frutilla();
      ~Frutilla();

      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
