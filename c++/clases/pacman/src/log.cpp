#ifndef __LOG__
#define __LOG__

#include "log.h"
#include "cstring"

using namespace std;
Log::Log(char *file){
     if(file!=NULL) setPath(file);
     else setPath(NULL);
     active = false;
}

Log::~Log(){
     delete []path;
}

void Log::setPath(char *str){
     path = new char[strlen(str)+1];
     strcpy(path, str);
}

char * Log::getPath(){
     return path;
}

bool Log::addLog(char *str){
     if(active==false) return false;
     if((fp = fopen(path, "a+"))==NULL) return false;

     time_t t;
     time(&t);
     tm* lt = localtime(&t);
     char text[200];
     strftime(text, 21, "[%x %X] ", lt);
     memcpy(&text[20], str, strlen(str)+1);
     text[20+strlen(str)] = '\n';
     text[21+strlen(str)] = '\0';

     fputs(text,fp);

     fclose(fp);
     return true;
}

void Log::activated(bool on){
     active = on;
}

#endif
