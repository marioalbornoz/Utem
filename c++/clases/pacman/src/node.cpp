#ifndef __NODE__
#define __NODE__

#include "node.h"

Node::Node(){
     value=0;
     child[0] = NULL;
     child[1] = NULL;
     child[2] = NULL;
     child[3] = NULL;
}

void Node::expandChilds(Node *a, Node *b, Node *c, Node *d){
     child[0] = a;
     child[1] = b;
     child[2] = c;
     child[3] = d;
}

#endif

