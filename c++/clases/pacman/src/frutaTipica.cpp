#ifndef __FRUTATIPICA__
#define __FRUTATIPICA__

#include "frutaTipica.h"

FrutaTipica::FrutaTipica() : Award(AWARD_FRUTATIPICA, "Fruta"){
     strcpy(filePath[0],"images/guinda.png");
     strcpy(filePath[1],"images/manzana.png");
     strcpy(filePath[2],"images/ciruela.png");
     strcpy(filePath[3],"images/mango.png");
     strcpy(filePath[4],"images/uva.png");
     strcpy(filePath[5],"images/llave.png");
}

FrutaTipica::~FrutaTipica(){
}

Character * FrutaTipica::doEffect(Character *p){
     game.increaseScore(SCORE_FRUTATIPICA + (int)(544.44*(game.getLevel()-1)));
     return p;          
}

char * FrutaTipica::getImagePath(){
     switch(game.getLevel()){
         case 1: 
         case 2: return filePath[0]; 
                 break;
         case 3:
         case 4: return filePath[1];
                 break;
         case 5:
         case 6: return filePath[2];
                 break;
         case 7:
         case 8: return filePath[3];
                 break;
         case 9: return filePath[4];
                 break;
         case 10:return filePath[5];
                 break;
     }
}

#endif
