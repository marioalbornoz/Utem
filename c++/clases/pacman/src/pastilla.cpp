#ifndef __PASTILLA__
#define __PASTILLA__

#include "pastilla.h"

Pastilla::Pastilla() : Award(AWARD_PASTILLA, "Pastilla"){
     strcpy(filePath,"images/pastilla.png");
}

Pastilla::~Pastilla(){
}

Character * Pastilla::doEffect(Character *p){
     if(p->getSpeed() + SPEED_PASTILLA <= GAME_PACMAN_SPEED_MAX)
          p->speedUp(SPEED_PASTILLA);
     ((Pacman *)p)->setEatGhost(true);
     
     game.blinky->setIntelligence(30);
     game.pinky->setIntelligence(30);
     game.inky->setIntelligence(30);
     game.clyde->setIntelligence(30);
     
     setEffectTime(60000);
     
     return p;
}

void Pastilla::setEffectTime(unsigned int tiempo){
     glutTimerFunc(tiempo, loseAwardEffect, AWARD_PASTILLA);
}


char * Pastilla::getImagePath(){
     return filePath;
}

#endif
