#ifndef __BLINKY__H
#define __BLINKY__H

#include "ghost.h"
#include "constantes.h"

class Blinky : public Ghost{
      public:
      Blinky(char *);
      int getImage();
      char *getImagePath(int);
      protected:
      char filePath[3][30];
};

#endif
