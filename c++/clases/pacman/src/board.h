#ifndef __BOARD__H
#define __BOARD__H

#include <math.h>
#include <windows.h>
#include "constantes.h"
#include "component.h"
#include "character.h"
#include "cell.h"
#include "ghost.h"
#include "util.h"

class Board : public Component{
      public:
      Board(int, int);
      ~Board();
      
      Cell **celda; //celdas que forman el tablero

      void addPastillas(); //pastillas que permiten a pacman comer fantasmas
      void addPebbles();
      void delPebbles();
      void addCharacters();
      void delCharacters();
      int countPebbles();
      void clearBoardOfAwards();
      void addWalls();
      void moveCharacter(Character *, int); //quita de la casilla actual y lo pone en la que corresponda
      void pacmanStepOnCell(Cell *); //ejecuta las acciones segun el contenido de la celda
      void ghostStepOnCell(Ghost *, Cell *); //ejecuta las acciones segun el contenido de la celda
      void sendGhostToHome(Ghost *); //Posiciona al fantasma en su casa, donde se regenera
      
      private:
      int high;  //dimensiones del tablero
      int width; //dimensiones del tablero
      int pebbles; //objetos que come pacman
};

#endif
