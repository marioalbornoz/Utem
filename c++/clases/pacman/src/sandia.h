#ifndef __SANDIA__H
#define __SANDIA__H

/* Efectos del premio
//
//Incrementa 20 puntos al jugador
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"

extern Game game;

class Sandia : public Award{
      public:
      Sandia();
      ~Sandia();

      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
