#ifndef __NODE__H
#define __NODE__H

#include <cstdlib>

class Node{
     public:
     Node();
     Node *child[4];
     int value;
     void expandChilds(Node *, Node *, Node *, Node *);
};

#endif
