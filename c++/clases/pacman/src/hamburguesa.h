#ifndef __HAMBURGUESA__H
#define __HAMBURGUESA__H

/* Efectos del premio
//
//Decrementa en 1 la velocidad de pacman
//si es mayor a 1
///////////////////////////////////*/

#include "award.h"
#include "character.h"
#include "game.h"
#include <gl\glut.h>

extern Game game;
void loseAwardEffect(int);

class Hamburguesa : public Award{
      public:
      Hamburguesa();
      ~Hamburguesa();

	  void setEffectTime(unsigned int);
      Character * doEffect(Character *p);
      char *getImagePath();
      protected:
      char filePath[30];
};

#endif
