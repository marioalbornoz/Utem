#ifndef __PINKY__
#define __PINKY__

#include "pinky.h"
#include "game.h"
extern Game game;

Pinky::Pinky(char *name) : Ghost(CHAR_PINKY, name){
     strcpy(filePath[0],"images/pinky.png");
     strcpy(filePath[1],"images/blueghost.png");
     strcpy(filePath[2],"images/eatenghost.png");
}

int Pinky::getImage(){
     if(getGoHome()) return 2; //fue comido
     if(game.pacman->getEatGhost()) return 1; //puede ser comido
     return 0; //color normal
}

char * Pinky::getImagePath(int id){
     return filePath[id];
}

#endif
