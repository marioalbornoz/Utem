#include<iostream>
#include<string>

using namespace std;

class Persona{
public:

   ~Persona();//Destructor
   //Metodos
   Persona(string,int);
   void setNombre(string);
   void setEdad(int);
   int getEdad();
   string getNombre();



private:
  string nombre;
  int edad;

};
//Constructores
class Jefe: public Persona{
public:
  Jefe(string,int,string,int);
  void setArea(string);
  string getArea();
  void setSueldo(int);
  int getSueldo();
private:
  string area;
  int sueldo;

};
class Alumnos: public Persona{
public:
  Alumnos(string, int, int,float);
  void setMatricula(int);
  void setNotas(float);
  int getMatricula();
  int getNotas();
private:
  int matricula;
  int notas;
};

Persona::Persona(string n,int e) : nombre(n),edad(e){}
void Persona::setNombre(string n){
  nombre=n;
}

void Persona::setEdad(int e){
  edad=e;
}
string Persona::getNombre(){
  return nombre;
}
int Persona::getEdad()
{
  return edad;
}
Persona::~Persona(){}

Jefe::Jefe(string n,int e,string a,int s) : Persona(n,e){
  area=a;
  sueldo=s;
}
void Jefe::setArea(string a)
{
  area=a;
}

void Jefe::setSueldo(int s){
  sueldo=s;
}
string Jefe::getArea(){
  return area;
}
int Jefe::getSueldo(){
  return sueldo;
}

Alumnos::Alumnos(string n,int e,int m,float no): Persona(n,e){
  matricula=m;
  notas=no;

}

Persona m("Daniela",18);
Persona b("BOBO",200);
Jefe o("Mario",22,"Informatica",1000000000);

int main()
{
  cout<<"El nombre de la persona es: "<<m.getNombre()<<endl<<b.getNombre()<<endl;
  cout<<"Edad: "<<m.getEdad()<<endl<<b.getEdad()<<endl;
  cout<<"El nombre del jefaso es: "<<o.getNombre()<<endl<<"El Area del jefe es: "<<o.getArea()<<endl<<"sueldo: "<<o.getSueldo()<<endl;

  return 0;
}
