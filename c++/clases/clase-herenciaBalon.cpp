#include<iostream>

using namespace std;
class balon{
public:
  balon();
  balon(string, string);
  void setTipo(string);
  void setColor(string);
  string getColor();
  string getTipo();

private:
  string tipo;
  string color;

};

class balonfutbol: public balon{
public:
  balonfutbol();
  balonfutbol(string,string,string);
  void setMarca(string);
  string getMarca();

private:
  string marca;

};

class balonBasket: public balon{
public:
  balonBasket(string,string, int);
  void setDiametro(int);
  int getDiametro();

private:
  int diametro;
};

balon::balon(){
  tipo="ningun Balon Definido\n";
  color="ningun color definido\n";
}
balon::balon(string t, string c): tipo(t),color(c){}

void balon::setTipo(string t){
  tipo=t;
}
void balon::setColor(string c){
  color=c;
}
string balon::getTipo(){
  return tipo;
}
string balon::getColor(){
  return color;
}

balonfutbol::balonfutbol(){
  marca="No definido";
}
balonfutbol::balonfutbol(string t, string c, string m): balon(t,c){

}
void balonfutbol::setMarca(string m){
  marca=m;
}

string balonfutbol::getMarca(){
  return marca;
}

balonBasket::balonBasket(string t, string c, int d): balon(t,c){}
void balonBasket::setDiametro(int d){
  diametro=d;
}
int balonBasket::getDiametro(){
  return diametro;
}
balonfutbol g;
balonBasket c("Nike","rojo",23);

int main(){
  cout<<"el tipo de balon es: "<<g.getTipo();
  cout<<"el color del balo es: "<<c.getColor()<<endl;
  cout<<"La marca del balon ingresado: "<<c.getTipo()<<endl<<"";

  cout<<"El diametro en dm es "<<c.getDiametro()<<endl;
  return 0;


}
