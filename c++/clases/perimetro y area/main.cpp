#include<iostream>
using namespace std;

class rectangulo{
public:
  rectangulo();
  rectangulo(int);
  rectangulo(int,int);
  void set_ancho(int);
  void set_largo(int);
  int get_ancho();
  int get_largo();
  int get_perimetro();
  int get_area();
private:
  int ancho;
  int largo;
  int dato;
};

rectangulo::rectangulo()
{
  ancho=0;
  largo=0;
  dato=0;
}
rectangulo::rectangulo(int a, int l) : ancho(a),largo(l) {}
rectangulo::rectangulo(int a){}
void rectangulo::set_largo(int l)
{
  largo=l;
}
void rectangulo::set_ancho(int a)
{
  ancho=a;
}
int rectangulo::get_perimetro()
{
  return ancho*2 + largo*2;
}
int rectangulo::get_area()
{
  return ancho*largo;
}

rectangulo rec;
rectangulo rec2(10,2);


int main() {
  rec.set_largo(10);
  rec.set_ancho(12);
  cout << "El perimetro es: " << rec.get_perimetro() << endl;
  cout << "El areas es : " << rec.get_area() << endl;
  cout << "El perimetro 2 es: "<< rec2.get_perimetro() <<endl;
  cout << "El area 2 es : " << rec2.get_area() << endl;
  return 0;
}
