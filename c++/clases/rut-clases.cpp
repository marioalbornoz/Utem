#include <iostream>
#include <string>
using namespace std;

class Rut
{
public:
  Rut();
  Rut(int,int);
  void setRut(int);
  void setDigVerificador(int);
  int getRut();
  int getDigVerificador();
private:
  int rut;
  int digVerificador;
};
Rut::Rut()
{
  rut=20000000;
  digVerificador=0;
}
Rut::Rut(int r,int d) : rut(r),digVerificador(d) {}

void Rut::setRut(int r){
  rut=r;
}
void Rut::setDigVerificador(int d){
  digVerificador=d;
}
int Rut::getDigVerificador()
{return digVerificador;}

int Rut::getRut(){
  return rut;
}

Rut run;
Rut miRun(19002691,4);

int main(){


  cout<<"el digito verificador es: "<<miRun.getDigVerificador();

  return 0;
}
