#include<iostream>
#include <algorithm>
using namespace std;

class Fraccion{
public:
  Fraccion();
  Fraccion(int,int);
  void setNumerador(int);
  void setDenominador(int);
  int getNumerador();
  int getDenominador();
  void get_muestraFraccion();
  int maxComunDivisor();
  Fraccion Producto(Fraccion);
  Fraccion suma(Fraccion);
  void simplificar();

private:
  int numerador;
  int denominador;

};

Fraccion::Fraccion(){
  numerador=0;
  denominador=1;
}
Fraccion::Fraccion(int n,int d) : numerador(n),denominador(d){}
void Fraccion::setNumerador(int n){
  numerador=n;
}
void Fraccion::setDenominador(int d ) { denominador=d;
  /* cod*/
}

int Fraccion::getNumerador(){
  return numerador;
}

int Fraccion::getDenominador(){
  return denominador;
}

void Fraccion::get_muestraFraccion()
{
  cout<<numerador<<"/"<<denominador<<endl;
}
 Fraccion Fraccion::Producto(Fraccion s)
{
  Fraccion frac(s.getNumerador()*numerador,s.getDenominador()*denominador);
  //frac.simplificar();
  return frac;
}
Fraccion Fraccion::suma(Fraccion s){
  Fraccion otrafracc(numerador*s.getDenominador()+denominador*s.getNumerador(),getDenominador()*denominador);
  otrafracc.simplificar();
  return otrafracc;
}

void Fraccion::simplificar(){
  int mcd;
  mcd = maxComunDivisor();
  numerador=numerador/mcd;
  denominador=denominador/mcd;
}

int Fraccion::maxComunDivisor(){  // Abre gcd
  if(numerador==0){
    return denominador;
  } else {
    Fraccion otraFraccion(denominador%numerador,numerador);
    return otraFraccion.maxComunDivisor();
  }

  }


Fraccion f;
Fraccion a(2,3);
Fraccion s(6,3);
Fraccion w(6,6);

int main(){
  cout<<"la fraccion es: ";
  s.get_muestraFraccion();
  cout << "El producto de las fracciones es: ";
  s.Producto(s).get_muestraFraccion();
  std::cout << "La suma de las fracciones es: " ;
  s.suma(a).get_muestraFraccion();
  std::cout << "La fraccion simplificada es: " ;
  s.simplificar();
  s.get_muestraFraccion();
  w.simplificar();
  w.get_muestraFraccion();

  return 0;
}
