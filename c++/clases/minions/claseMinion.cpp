#include<iostream>

using namespace std;

class minion{
public:
  minion();
  minion(string,int,int,int);
  void setNombre(string);
  void setCantidadOjos(int);
  void setColor(int);
  void setNivelReveldia(int);
  string getNombre();
  int getCantidadOjos();
  int getColor();
  int getNivelReveldia();
  bool estaRevelde(minion);
  void colorMinion(int);
private:
  string name;
  int nOjos;
  int color;
  int rebeldia;
};

minion::minion(){
  name="Nombre no definido";
  nOjos= 0;
  color=0;
  rebeldia=0;
}

minion::minion(string n,int o, int c, int r): name(n),nOjos(o),color(c),rebeldia(r){}

void minion::setNombre(string n){
  name=n;
}
void minion::setCantidadOjos(int o){
  nOjos=o;
  }
void minion::setColor(int c){
  color=c;
}
void minion::setNivelReveldia(int r){
  rebeldia=r;
}
string minion::getNombre(){
  return name;
}
int minion:: getCantidadOjos(){
  return nOjos;
}
int minion:: getColor(){
  return color;
}
int minion:: getNivelReveldia()
{
  return rebeldia;
}

bool minion::estaRevelde(minion b)
{
  if(b.getNivelReveldia()>50) return true;
  else return false;
}


/*void minion::colorMinion(int c)
{
   c=getColor();
   if(c==1) cout<< "amarillo\n";
   if(c==2) cout<<"Color del minion: Morado\n";
   if(c==3) cout<<"Color del Minion: Otro";
   else cout<<"El minion no tiene color disponible\n";
}
*/

minion a("Kevin",2,1,2);
int main(){
  cout<<"******Los minions******\n\n";
  cout<<"El minion elegido es: "<<a.getNombre()<<endl;
  cout<<"El color del minion es: "<<a.getColor()<<".";
  if(a.getColor()==1) cout<< "amarillo\n";
  if(a.getColor()==2) cout<<"Color del minion: Morado\n";
  if(a.getColor()==3) cout<<"Color del Minion: Otro\n";
  cout<<"Cantidad de ojos: "<<a.getCantidadOjos()<<endl;
  cout<<"Es rebelde al: "<<a.getNivelReveldia()<<"%\n"<<endl;
  /*int rebelde=a.estaRevelde();
  if(rebelde==1) cout<<"Es muy rebelde\n"
else cout<<" Es muy poco rebelde\n"<<endl;*/



  return 0;
}
