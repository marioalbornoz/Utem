#include <iostream>
#include<limits.h>
#define MAX_FILAS       10
#define MAX_COLUMNAS    10

using namespace std;

void mostrarMatriz(int);
void sumarMatriz(int, int);
void transponerMatriz(int);
void sumarDiagonalSuperior (int);
void posicionXYMayoresMenor(int);



void mostrarMatriz(int m[10][10])
{
    int i, j;
    for(i=0;i<MAX_FILAS;i++)
    {
        cout<<endl;
        for(j=0;j<MAX_COLUMNAS;j++)
        {
            cout<< m[i][j]<<" ";
        }
    }
}
void sumarMatriz(int m[10][10], int n[10][10])
{
    int i,j,nuevaMatriz=0;;
    for(i=0;i<MAX_FILAS;i++)
    {
        cout<<endl;
        for(j=0;j<MAX_COLUMNAS;j++)
        {
            nuevaMatriz=m[i][j]+n[i][j];
            cout<< nuevaMatriz<<" ";
        }
    }
}
void transponerMatriz(int m[10][10])
{
    int i,j;
    for(i=0;i<MAX_FILAS;i++)
    {
        cout<<endl;
        for(j=0;j<MAX_COLUMNAS;j++)
        {
            cout<<m[j][i]<<" ";
        }
    }
}
int sumarDiagonalSuperior(int m[10][10])
{
    int i,j,suma=0;
    for(i=0;i<MAX_FILAS;i++)
    {
        for(j=0;j<MAX_COLUMNAS;j++)
        {
            if(i>j)
               {
                suma+=m[i][j];
                }
        }
    }
    return suma;
}
void posicionXYMayoresMenor(int m[10][10])
{
    int i,j,xmayor,ymayor,xmenor,ymenor,menor=INT_MAX,mayor=INT_MIN;
    for(i=0;i<MAX_FILAS;i++)
    {
        for(j=0;j<MAX_COLUMNAS;j++)
        {
            if(m[i][j]>mayor)
            {
                xmayor=i;
                ymayor=j;
                mayor=m[i][j];
            }



            if(m[i][j]<menor)
                {menor=m[i][j];
                xmenor=i;
                ymenor=j;}
        }
    }
    cout<<"El numero menor de la matriz es: "<<menor << " en la posicion: " <<xmenor<<","<<ymenor<<endl;

    cout<<endl;

    cout<<"El numero mayor de la matriz es: "<<mayor << " en la posicion: " <<xmayor<<","<<ymayor<<endl;
}
int main()
{   int suma;
    int A[10][10] = {
        { 0 , 1 , 8 , 7 , 4 , 4 , 2 , 9 , 2 , 2 },
        { 7 , 7 , 5 , 8 , 9 , 0 , 7 , 4 , 0 , 1 },
        { 1 , 7 , 8 , 3 , 6 , 6 , 8 , 2 , 0 , 2 },
        { 1 , 9 , 7 , 3 , 0 , 8 , 6 , 3 , 5 , 2 },
        { 2 , 6 , 0 , 0 , 2 , 1 , 1 , 6 , 5 , 0 },
        { 1 , 5 , 7 , 4 , 3 , 2 , 4 , 4 , 3 , 9 },
        { 3 , 7 , 7 , 3 , 8 , 5 , 9 , 2 , 2 , 3 },
        { 6 , 9 , 8 , 8 , 4 , 9 , 3 , 4 , 8 , 4 },
        { 0 , 5 , 6 , 2 , 7 , 9 , 2 , 6 , 2 , 2 },
        { 1 , 0 , 0 , 7 , 0 , 6 , 5 , 9 , 7 , 2 }
    };
    int B[10][10] = {
        { 4 , 9 , 9 , 3 , 4 , 4 , 8 , 8 , 4 , 7 },
        { 5 , 6 , 0 , 8 , 2 , 5 , 5 , 1 , 4 , 8 },
        { 2 , 3 , 8 , 3 , 5 , 2 , 8 , 8 , 5 , 5 },
        { 3 , 0 , 6 , 6 , 1 , 6 , 9 , 8 , 5 , 8 },
        { 2 , 3 , 2 , 5 , 8 , 0 , 8 , 8 , 6 , 2 },
        { 4 , 4 , 4 , 8 , 6 , 3 , 5 , 0 , 0 , 7 },
        { 7 , 1 , 0 , 8 , 1 , 6 , 3 , 0 , 9 , 5 },
        { 2 , 2 , 0 , 5 , 4 , 1 , 9 , 1 , 9 , 2 },
        { 2 , 4 , 1 , 8 , 9 , 5 , 5 , 5 , 1 , 7 },
        { 0 , 5 , 2 , 5 , 0 , 0 , 9 , 0 , 3 , 7 }
    };
    cout<<"primera Matriz: "<<endl;
    mostrarMatriz(A);
    cout<<endl;
    cout<<endl <<"Suma de Matriz:"<<endl;
    sumarMatriz(A,B);
    cout<<endl;
    transponerMatriz(A);
    cout<<endl<<endl;
    suma=sumarDiagonalSuperior(A);
    cout<<"la suma de la siagonal superior es: "<< suma<<endl;
    posicionXYMayoresMenor(A);
    return 0;
}
