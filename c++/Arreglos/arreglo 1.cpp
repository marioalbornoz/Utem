#include <iostream>

using namespace std;

/*Definimos las funciones que utilizaremos*/
void llenar (int A[], int n);
void mostrar(int A[], int n);
void impri1 (int A[], int n);
void impri2 (int A[], int n);
int sumar   (int A[], int n);
void llenarRecursivo(int A[], int n, int posActual);


int main() {

    int A[100]; /*Definimos un arreglo de 100 elementos, recuerda que parte en 0*/
    int n;
    cout << "Recursividad con arreglos" << endl;
    cout << "Ingrese N : ";
    cin >> n;
    cout << "LLenar arreglo " << endl;
    //llenar(A,n);
    llenarRecursivo(A, n, 0);
    cout << endl;
    cout << "Mostrar arreglo " << endl;
    mostrar(A,n);

    cout << endl;
    cout << "Imprimir 1 : arreglo" << endl;
    impri1(A,n);

    cout << endl;
    cout << "Imprimir 2 : arreglo" << endl;
    impri2(A,n);

    cout << endl;
    cout << "Suma elementos : arreglo" << endl;
    cout << "Suma = " << sumar(A,n) << endl;

    return 0;
}


void llenar(int A[], int n){
    int i;
    for(i=0; i<n; i++){
        cout << "Dato["<<i<<"]= ";
        cin >> A[i];
    }
}

/*llamada   llenarRecursivo(A, n, 0);*/
void llenarRecursivo(int A[], int n, int posActual){
    if(n <= posActual)  return;

    cout << "Recursivo Dato["<<posActual<<"]= ";
    cin >> A[posActual];
    llenarRecursivo(A, n, posActual + 1);
}


void mostrar(int A[], int n){
    int i;
    for(i=0; i<n; i++){
        cout << "Arreglo["<<i<<"]= "<<A[i]<<endl;
    }
}

/* llamada impri1(A,n);*/
void impri1(int A[], int n){
    if(n==0){}
    else{
        impri1(A,n-1);
        cout << "Arreglo " << A[n-1]<<endl;
    }
}

void impri2(int A[], int n){
    if(n!=0){
        cout << "Arreglo " << A[n-1]<<endl;
        impri2(A,n-1);
    }
}

int sumar(int A[], int n){
    if(n==0)
        return 0;
    else{
        return sumar(A,n-1) + A[n-1];
    }
}
