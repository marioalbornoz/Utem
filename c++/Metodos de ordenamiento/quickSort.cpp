#include <iostream>
#include <iomanip>
#define TAM 100

using namespace std;
void agregarArreglo(int, int);
void mostarArreglo(int,int);
void ordenarQuicksort(int,int,int);

void agregarArreglo(double a[TAM], int n)
{

  cout<<"ingrese elementos del arreglo: "<<endl;
  for(int i=0;i<n;i++)
  {
    cout<<"a["<<i<<"]= ";
    cin>>a[i];
  }
  cout<<"El arreglo ingresado fue: "<<endl;
  for(int i=0;i<n;i++)
  {
    cout<<a[i]<<endl;
  }

}



void mostarArreglo(double a[TAM],int n)
{
  cout<<endl<<"El arreglo  ordenado: "<<endl;
  for(int i=0;i<n;i++)
  {
    cout<<a[i]<<setw(5);
  }

}

void ordenarQuicksort(double a[TAM], int primero, int ultimo){
  int central,i,j;
  double pivote;
  central=(primero+ultimo)/2; //obtener la posicion central del arrelo
  pivote=a[central];//guarda en neustra variable el valor central
  i=primero;
  j=ultimo;
  do {
    //ahora separamos el vector en dos partes
    while(a[i] < pivote) i++;
    while(a[j] > pivote) j--;
    if(i<=j)
    {
      double temp; //definimos una variable temporal
      //intercamvio de valores
      temp=a[i];
      a[i]=a[j];
      a[j]=temp;
      i++;
      j--;
    }
  } while(i<=j);
  if(primero>j)
      ordenarQuicksort(a,primero,j);
  if(i<ultimo)
      ordenarQuicksort(a,i,ultimo);


}



int main()
{
  double A[TAM];
  int n;
  cout<<"Ingrese cantidad de elementos: "<<endl;
  cin>>n;
  agregarArreglo(A,n);
  ordenarQuicksort(A,0,n-1);
  mostarArreglo(A,n);


  return 0;
}
