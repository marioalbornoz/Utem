#include<iostream>

using namespace std;

//METODO DE ORDENAMIENTO MERGE SORT
void mezclar(int A[], int izq, int der, int centro)
{
  int i=izq;
  int Cont=0;
  int j=centro+1;
  int* aux= new int[der-izq+1];
  int k=0;

  while ((i<=centro) &&(j<=der))
  {
      Cont++;
      if(A[i]<A[j])
      {
          aux[k]=A[i];
          i++;
      }
      else
      {
          aux[k]=A[j];
          j++;
      }
      k++;
  }

  while (i<=centro)
  {
      Cont++;
      aux[k]=A[i];
      i++;
      k++;
  }

  while (j<=der)
  {
      Cont++;
      aux[k]=A[j];
      j++;
      k++;
  }

  for(i=0;i<k;i++)
  {
      Cont++;
      A[i+izq]=aux[i];
  }

}


void ordenarMezcla(int A[], int p, int q)
{

  int centro=0;

  if (p<q)
  {

      centro=(p+q)/2;
      ordenarMezcla(A,p,centro);
      ordenarMezcla(A,centro+1,q);
      mezclar(A,p,q,centro);
  }
}

int main()
{
    int A[20], auxArray[20];
    int n=0, i;
    cout<<endl<<endl;
    cout<<"\t\t\t"<<"////////////////////////////////"<<endl;
    cout<<"\t\t\t"<<"///  Metodo de ordenamiento  ///"<<endl;
    cout<<"\t\t\t"<<"///        MERGE SORT        ///"<<endl;
    cout<<"\t\t\t"<<"////////////////////////////////"<<endl;
    cout<<endl;
	 cout<<"Ingrese la dimension del arreglo:";
	 cin>>n;
    cout<<endl;
	 for(i=0;i<=n-1;i++)
	    {
		  cout<<"Elemento A["<<i<<"]:";
		  cin>>A[i];
	    }
     ordenarMezcla(A,auxArray,n-1);
     cout<<endl;
     cout<<"Ordenando por Merge Sort:";
     cout<<endl<<endl;
     for(i=0;i<=n-1;i++)
		  {
		   cout<<"\t"<<A[i];
		  }
return 0;
}
