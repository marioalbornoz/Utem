#ifndef NULL
    #define NULL 0
#endif

#ifndef STACK_H
#define STACK_H

template <class T>
class Stack {
	public:
		class Nodo {
			public:
				Nodo(const T& e, Nodo *p) : dato(e), link(p) {}
				~Nodo() { }

				T dato;
				Nodo *link;
		};

		Stack();
		~Stack();

		void push(T e);
		T pop();

		bool empty();

	private:
		Nodo *mPrimero;
		Nodo *mUltimo;
};



template <class T>
Stack<T>::Stack() {
    mUltimo = NULL;
	mPrimero	=	NULL;

}

template <class T>
Stack<T>::~Stack() {
}

template <class T>
void Stack<T>::push(T e) {
	Nodo *p = new Nodo(e, NULL);

	if(empty()) {
		mPrimero = p;
		mUltimo	=	mPrimero;
		return;
	}
	mUltimo->link	=	p;
	mUltimo	=	p;
}

template <class T>
T Stack<T>::pop() {
	if(empty()) return T();
	Nodo *p = mPrimero;
	T e	=	mPrimero->dato;
	mPrimero = mPrimero->link;
	delete p;
	return e;
}

template <class T>
bool Stack<T>::empty() {
	return !(mPrimero);
}
/*
template <class T>
T sumar(Stack<T>& S) {
	T aux;
	T e;
	if(S.empty())
		return 0;
	else {
		e = S.pop();
		aux = e + sumar(S);
		S.push(e);
		return aux;
	}
}

*/
#endif // STACK_H
