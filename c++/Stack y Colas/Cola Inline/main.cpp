#include <iostream>
#include "Cola.h"

using namespace std;

int main() {
    cout << "Colas (Queue) Implementación Simple" << endl;

    Cola c;
    c.agregar(1);
    c.agregar(2);
    c.agregar(3);
    c.agregar(4);
    c.agregar(5);

    while (!c.vacia()) {
        cout << c.extraer() <<endl;
    }

    return 0;
}
