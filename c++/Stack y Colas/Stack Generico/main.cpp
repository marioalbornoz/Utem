    #include <iostream>
    #include <stack>
    #include <queue>
    using namespace std;

    class Persona {
    public:
        Persona();
        ~Persona();
        void setNombre(string);
        string getNombre();
    private:
            string nombre;
    };

    Persona::Persona() {
        nombre = "";
    }

    Persona::~Persona() {
    }

    void Persona::setNombre(string valor) {
        nombre = valor;
    }

    string Persona::getNombre(){
        return nombre;
    }

    class Stack : public stack<int> {
    public:
        int Pop() {
            int a = top();
            pop();
            return a;
        }

        void Push(int valor) {
            push(valor);
        }

        bool Empty() {
            return empty();
        }
    };

    int main() {

        Stack f;
        f.Push(1);
        f.Push(15);
        while (!f.Empty()) {
            cout << "\nValor "<<f.Pop();
        }
        cout << endl;

        Persona p1;
        p1.setNombre("David");

        Persona p2;
        p2.setNombre("Marilyn");

        /**Utilizando Queue**/
        queue<Persona> s;
        s.push(p1);
        s.push(p2);

        cout << "Lista de Personas" << endl;
        while(!s.empty()){
            Persona aux;
            aux = s.front();
            s.pop();
            cout <<endl<< aux.getNombre();
        }

        return 0;
    }
