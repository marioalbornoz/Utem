#ifndef STACK_H
#define STACK_H

class Stack {
    typedef int Tipo;
    class Nodo {
        public:
            Nodo() {};
            Nodo(const Tipo &e, Nodo *p) : dato(e), link(p) {};
            virtual ~Nodo() {};

            Tipo dato; /**Esto lo podemos cambiar**/
            Nodo *link;
    };

	public:
		Stack();
		~Stack();

		void push(Tipo e);
		Tipo pop();
		bool empty();

	private:
		Nodo *mL;

};

#endif // STACK_H




