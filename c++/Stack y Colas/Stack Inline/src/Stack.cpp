#include "Stack.h"
#define NULL 0
Stack::Stack() {
    mL = NULL;
}

Stack::~Stack() { }

void Stack::push(Tipo e) {
	Nodo *p = new Nodo(e, NULL);

	if(mL == NULL)
		mL = p;
	else{
		p->link = mL;
		mL = p;
	}
}

Stack::Tipo Stack::pop() {
	Nodo *p = mL;
	Tipo e=0;

	if(p != NULL){
		e = mL->dato;
		mL = mL->link;
		delete p;
		return e;
	}
	return e;
}

bool Stack::empty() {
	return (mL == NULL);
}

