#ifndef ALFABETOINGLES_H
#define ALFABETOINGLES_H


class alfabetoIngles
{
    public:
        alfabetoIngles();
        alfabetoIngles(string,string);
        virtual ~alfabetoIngles();
        void setCadena(string);
        void caracter(string);
        string getCadena();
        string getCaracter();
    // int asignarCharCadena();

    protected:

    private:
        string cadena[26];
        string caracter;
};

#endif // ALFABETOINGLES_H
