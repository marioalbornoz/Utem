#include<iostream>
using namespace std;

template<typename ValueType>
class Arraylist{
public:
    Arraylist();
    explicit Arraylist(int n, ValueType value = ValueType());
    virtual ~Arraylist();
    void Push(ValueType value);
    void Insert(int index, ValueType value);
    void Add(ValueType value);
    int Size() const;
    bool isEmpty() const;
    void Clear();
    void Remove(int index);

    const ValueType & get(int index) const;
    void Set(int index, const ValueType & value);

    ValueType &operator[](int index);
    const ValueType &operator[](int index) const;





private:
    int count;
    int capacity;
    ValueType *elements;
    void expandCapacity();
};
template<typename ValueType>
Arraylist<ValueType>::Arraylist() {
   count = capacity = 0;
   elements = NULL;
}

template <typename ValueType>
Arraylist<ValueType>::Arraylist(int n, ValueType value)
{
    count = capacity = n;
    elements = (n == 0) ? NULL : new ValueType[n];
    for(int i = 0; i < n; i++){
        elements[i] = value;
    }
}
template<typename ValueType>
Arraylist<ValueType>::~Arraylist()
{
    if(elements != NULL) delete[] elements;
}

template <typename ValueType>
void Arraylist<ValueType>::Insert(int index, ValueType value) {
   if (count == capacity) expandCapacity();
   if (index < 0 || index > count) {
      perror("insert: indice fuera de rango");
   }
   for (int i = count; i > index; i--) {
      elements[i] = elements[i - 1];
   }
   elements[index] = value;
   count++;
}


template <typename ValueType>
void Arraylist<ValueType>::Push(ValueType value) {
   Insert(count, value);
}

template <typename ValueType>
void Arraylist<ValueType>::expandCapacity() {
   capacity = max(1, capacity * 2);
   ValueType *array = new ValueType[capacity];
   for (int i = 0; i < count; i++) {
      array[i] = elements[i];
   }
   if (elements != NULL) delete[] elements;
   elements = array;
}


template <typename ValueType>
int Arraylist<ValueType>::Size() const {
   return count;
}

template <typename ValueType>
bool Arraylist<ValueType>::isEmpty() const {
   return count == 0;
}

template <typename ValueType>
void Arraylist<ValueType>::Clear() {
   if (elements != NULL) delete[] elements;
   count = capacity = 0;
   elements = NULL;
}

template <typename ValueType>
void Arraylist<ValueType>::Remove(int index) {
   if (index < 0 || index >= count) perror("remove: indice fuera de rango");
   for (int i = index; i < count - 1; i++) {
      elements[i] = elements[i + 1];
   }
   count--;
}



template <typename ValueType>
const ValueType & Arraylist<ValueType>::get(int index) const {
   if (index < 0 || index >= count) perror("get: indice fuera de rango");
   return elements[index];
}

template <typename ValueType>
void Arraylist<ValueType>::Set(int index, const ValueType & value) {
   if (index < 0 || index >= count) perror("set: indice fuera de rango");
   elements[index] = value;
}

template <typename ValueType>
ValueType & Arraylist<ValueType>::operator[](int index) {
   if (index < 0 || index >= count) perror("SSelecionar: indice fuera de rango");
   return elements[index];
}
template <typename ValueType>
const ValueType & Arraylist<ValueType>::operator[](int index) const {
   if (index < 0 || index >= count) perror("Selecionar: indice fuera de rango");
   return elements[index];
}

template <typename ValueType>
void Arraylist<ValueType>::Add(ValueType value) {
   Insert(count, value);
}
