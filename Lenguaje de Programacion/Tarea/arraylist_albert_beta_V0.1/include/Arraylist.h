#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include<iostream>
using namespace std;

template<typename ValueType>
class Arraylist{
public:
    Arraylist();
    explicit Arraylist(int n, ValueType value = ValueType());
    virtual ~Arraylist();
    void Push(ValueType value);
    void Insert(int index, ValueType value);
    int Size() const;
    bool isEmpty() const;
    void Clear();
    void Remove(int index);

    const ValueType & get(int index) const;
    void Set(int index, const ValueType & value);
    ValueType & operator[](int index);
    const ValueType & operator[](int index) const;





private:
    int count;
    int capacity;
    ValueType *elements;
    void expandCapacity();
};

#endif // ARRAYLIST_H
