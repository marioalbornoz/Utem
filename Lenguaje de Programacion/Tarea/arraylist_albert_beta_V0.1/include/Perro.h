#ifndef PERRO_H
#define PERRO_H


#include<iostream>

using namespace std;

class Perro{
public:
  Perro();
  ~Perro();
  Perro(string,string);
  void setNombre(string);
  string getNombre();
  void setRaza(string);
  string getRaza();
private:
  string nombre;
  string raza;

};


#endif // PERRO_H
