; ********************************************************
; Desarrollo de Microcontroladores y DSPS
; Manejo de un LCD
; Pr�ctica 4
;
; Fecha: 18/06/2012
; Notas: Controlar un LCD LM016
;
; ek
; ************************************************************
LIST P = 18F4550
INCLUDE <P18F4550.INC>
;************************************************************
CONFIG FOSC = HS
CONFIG PWRT = ON
CONFIG BOR = OFF
CONFIG WDT = OFF
CONFIG MCLRE = ON
CONFIG PBADEN = OFF
CONFIG LVP = OFF
CONFIG DEBUG = OFF
CONFIG XINST = OFF
; CODE ******************************************************
CBLOCK 0x0C
ENDC
 
#define LCD_RS PORTD,0
#define LCD_RW PORTD,1
#define LCD_E PORTD,2
 
ORG 0x00
 
    clrf PORTB
    clrf PORTD
 
    clrf TRISB
    clrf TRISD
 
    call LCD_Inicializa
    bcf  LCD_E
 
Inicio
   call Delay ;Esperar un tiempo antes de comenzar a escribir
   movlw  'H'
   call   LCD_Caracter
   movlw  'o'
   call   LCD_Caracter
   movlw  'l'
   call   LCD_Caracter
   movlw  'a'
   call   LCD_Caracter
   movlw  ' '
   call   LCD_Caracter
   movlw  'M'
   call   LCD_Caracter
   movlw  'u'
   call   LCD_Caracter
   movlw  'n'
   call   LCD_Caracter
   movlw  'd'
   call   LCD_Caracter
   movlw  'o'
   call   LCD_Caracter
   movlw  '!'
   call   LCD_Caracter
   call   Delay
   call   Delay
 
   call   LCD_Borrar
   goto   Inicio
 
LCD_Inicializa
   call   Retardo_20ms ;Esperar 20 ms
   movlw  b'00110000' ;Mandar 0x30 -> W
   movwf  PORTB ;Enviar W -> PORTB
 
   call   Retardo_5ms ;Esperar 5ms
   movlw  b'00110000' ;Enviar 0x30 -> W
   movwf  PORTB
 
   call   Retardo_50us ;Acumular 100us
   call   Retardo_50us ;Acumular 100us
   movlw  b'00110000'
   movwf  PORTB
 
   movlw  0x0F
   movwf  PORTB
 
   bsf    LCD_E
   bcf    LCD_E
   return
 
LCD_Caracter
   bsf    LCD_RS ;Modo Caracter RS = 1
   movwf  PORTB ;Lo que se carg� previamente en W -> PORTB
   bsf    LCD_E ;Activar Enable
   call   Retardo_50us ;Esperar 50us para enviar informaci�n
   bcf    LCD_E ;Transici�n del Enable a 0
   call   Delay ;Esperar a poner la siguiente llamada
   return
 
LCD_Borrar
   movlw  b'00000001' ;Comando para Borrar
   call   LCD_Comando ;Enviar un comando
 
LCD_Comando
   bcf    LCD_RS ;Modo Comando RS = 0
   movwf  PORTB ;Envia W -> PORTB
   bsf    LCD_E ;Activa Enable
   call   Retardo_50us ;Espera que se envie la informaci�n
   bcf    LCD_E ;Transici�n del Enable
   return
 
  INCLUDE <LCD_Retardo.inc>
 
END ;Fin de Programa