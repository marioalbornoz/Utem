﻿; ----------------------------------------------
; Nombre: Sensor Ultrasonico - Assembly
; Autores: Mario Albornoz
; Fecha: Semestre 1 2018
; ----------------------------------------------
; ************************************************************
    LIST P = 18F4550
    INCLUDE <P18F4550.INC>
;************************************************************
    CONFIG FOSC = XT_XT
    CONFIG PWRT = ON
    CONFIG BOR = OFF
    CONFIG WDT = OFF
    CONFIG MCLRE = ON
    CONFIG PBADEN = OFF
    CONFIG LVP = OFF
    CONFIG DEBUG = OFF
    CONFIG XINST = OFF
; CODE ******************************************************
    CBLOCK 0x0C
    C1
    ENDC

; DEFINE NOMBRES PARA PINES

#define LCD_RS PORTD,0
#define LCD_RW PORTD,1
#define LCD_E PORTD,2
#define TRIGGER PORTD,3
#define ECHO PORTD,4
s
 
    ORG 0x00
;REINICIA PUERTOS A CERO
   clrf PORTB
   clrf PORTD

;SET TRISB COMO SALIDA (PANTALLA)
   clrf TRISB
   
;SET TRISB,4 COMO ENTRADA (ECHO)
   movlw b'00010000' ;
   movwf TRISD ; 

;INICIALIZA PANTALLA
   call LCD_Inicializa

Inicio
   call trigger	  ;ENVIA TRIGGER
   call wait4echo ;ESPERA POR RETORNO DE ECHO
   call count	  ;CUENTA PERIODO DE ECHO CADA 58uS

   movf C1,0,0		;MUEVE EL CONTADOR A W
   call BIN_BCD		;CONVIERTE W A BCD
   movlw 0x30		;SE SUMA 0X30 PARA OBTENER CODIGO ASCII
   addwf BCD_TRES
   movlw 0x30
   addwf BCD_DOS
   movlw 0x30
   addwf BCD_UNO
   
   movf BCD_TRES,0,0
   call LCD_Caracter
   movf BCD_DOS,0,0
   call LCD_Caracter
   movf BCD_UNO,0,0
   call LCD_Caracter
   movlw ' '
   call LCD_Caracter
   movlw 'c'
   call LCD_Caracter
   movlw 'm'
   call LCD_Caracter

continuar   
   ;call delay_200ms
   ;call LCD_Borrar
   
   call LCD_Return_Start
   goto   Inicio
   
;TRIGGER: ENVIA UN TRIGGER DE 25uS
trigger
   bsf TRIGGER
   call delay_25us
   bcf TRIGGER
   call delay_25us
   Return
   
;COUNT: CUENTA EL PERIODO EN QUE ECHO
;ESTÁ EN HIGH CADA 58 uS   
count
   clrf C1
cnt
   infsnz C1
   return
   call delay_58us
   btfsc ECHO
   goto cnt
   return
      
;SENDNEWTRIGGER: ENVIA UN NUEVO TRIGGER
;CUANDO NO SE RECIBE NADA.   
sendNewTrigger
   call trigger
   goto wait4echo

;WAIT4ECHO: LOOP HASTA QUE LLEGUE ECHO = HIGH.   
wait4echo
   btfss ECHO
   goto sendNewTrigger
   return
   
;LCD_INICIALIZA: INICIALIZA EL LCD
LCD_Inicializa
   call   delay_200ms ;Esperar 200ms
   movlw  b'00111000' ;Enviar 0x30 -> W
   movwf  PORTB	      ;Inicializa modo 8bits 
		      ;usando las dos filas del display
   call   delay_50us ;Acumular 100us
   call   delay_50us ;Acumular 100us
;   movlw  b'00111000'
;   movwf  PORTB
 
   movlw  b'1100' ;Enciende la pantalla
   movwf  PORTB
 
   bsf    LCD_E
   bcf    LCD_E
   return

;LCD_CARACTER: ESCRIBE UN CARACTER EN LCD   
LCD_Caracter
   bsf    LCD_RS ;Modo Caracter RS = 1
   movwf  PORTB ;Lo que se cargo previamente en W -> PORTB
   bsf    LCD_E ;Activar Enable
   call   delay_50us ;Esperar 50us para enviar informacion
   bcf    LCD_E ;Transicion del Enable a 0
   call   delay_50us
   return

;LCD_BORRAR: BORRA EL CONTENIDO DE LA PANTALLA   
LCD_Borrar
   movlw  b'00000001' ;Comando para Borrar
   call   LCD_Comando ;Enviar un comando

LCD_Return_Start
   movlw  b'00000010'
   call LCD_Comando
   
 
;LCD_COMANDO: ENVIA UN COMANDO A LA PANTALLA
LCD_Comando
   bcf    LCD_RS ;Modo Comando RS = 0
   movwf  PORTB ;Envia W -> PORTB
   bsf    LCD_E ;Activa Enable
   call   delay_50us ;Espera que se envie la informacion
   bcf    LCD_E ;Transicion del Enable
   call   delay_2ms
   return
   


;INCLUYEN LIBRERIAS AUXILIARES   
  INCLUDE <delays.inc>
  INCLUDE <numToBCD.inc>

  END
