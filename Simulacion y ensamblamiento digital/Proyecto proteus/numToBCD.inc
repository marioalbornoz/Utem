CBLOCK 
BCD_TRES
BCD_DOS
BCD_UNO
ENDC

;Iniciando numero, pasar de numero binario a BCD
BIN_BCD
	clrf BCD_TRES
	clrf BCD_DOS
	movwf BCD_UNO ;cargando el numero binario 

BCD_resta
	movlw .10 ; a la cifra uno se le resta 10
	subwf BCD_UNO,W	; resultado es guardado en W
	btfss STATUS,C; C == 1?
	goto BIN_BCD_FIN; Caso W negativo
BCD_Incrementa_DOS ; Caso W positivo
	movwf BCD_UNO; W -> BCD_UNO
	incf BCD_DOS,F ;incrementa las decenas
	movlw .10; 10 -> W
	subwf BCD_DOS,W; BCD_DOS - 10
	btfss STATUS,C; C == 1?
	goto BCD_resta; W es negativo
BCD_Incrementa_TRES; W es positivo
	clrf BCD_DOS; Borra BCD_DOS
	incf BCD_TRES,F; Incremente centena
	goto BCD_resta; Vuelve al inicio
BIN_BCD_FIN
	return