    CBLOCK
   Conta1
   Conta2
   Conta3
    ENDC

;delay_50us *********************
delay_50us
   movlw  .15
   movwf  Conta1
Re_50us
   decfsz Conta1, F ;Salta cuando Conta1 llega a 0
   bra    Re_50us   ;Salta a Repeat para Decrementar Conta1
   Return      
   
;delay_2ms *********************
delay_2ms
   movlw  .151
   movwf  Conta1
   movlw  .3
   movwf  Conta2
Re_2ms
   decfsz  Conta1, F ;Salta cuando Conta1 llega a 0
   bra     Re_2ms    ;Salta a Repeat para Decrementar Conta1
   decfsz  Conta2, F ;Salta cuando Conta2 llega a 0
   bra     Re_2ms    ;Salta a Repeat
   Return   
 
;delay_200ms   
delay_200ms
   clrf	Conta1
   clrf Conta2
del_200ms
   decfsz Conta1, F
   goto del_200ms
   decfsz Conta2, F
   goto del_200ms
   return
    
delay_10us   
   nop
   RETURN
   
